#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on April 10th 07:43:08 2024

@author: nchevaug
"""
from __future__ import annotations
from warnings import warn
import unittest
import numpy as np
import numpy.typing as npt
import scipy as sp
import scipy.optimize as spopt
import gmsh
import function_spaces as fs
import quadrature as quad
import oneLevelSimplexMesh as sm
import gmsh_post_pro_interface as gp
import gmsh_mesh_generator as mg
import hyperelasticity as hy
import sparse_tools as st
import boundary_conditions as bc

FNDArray = npt.NDArray[np.float64]
INDArray = npt.NDArray[np.int64]

# def computeNeumann(space, physName2Neumann):
#     ''' return the vector F such as F.T.dot(U) = int_neumann ( t.u_h) dGamma_h '''
#     F = np.zeros((space.scalarSize(), space.vecDim))
#     edgequad = quad.Edge_gauss(0)
#     for physName, dir2Value in physName2Neumann.items():
#         eids, ___ = space.m.getEdgeGroupe(physName)
#         N = space.evalOpOnEdges(eids, edgequad.s)
#         xy = space.m.getEdgesCog(eids)
#         l = space.m.getEdgesLength(eids)
#         t = np.zeros((eids.size, 2))
#         for directionName, value in dir2Value.items():
#             direction = dirName2dirVal[directionName]
#             if  direction is None:
#                 funName = computeNeumann.__name__
#                 raise ValueError('directionName '+directionName+' unknown in '+funName)
#             t[:, direction] = l*setValues(xy, value)
#         F += (edgequad.w*N.T.dot(t.flatten())).reshape((-1, 2))
#     return F.flatten()

# def computeVolumeLoad(space, source):
#     """ compute the vector F such as F.U = int_Omega f.u_h dOmega_h """
#     assert isinstance(source, np.ndarray)
#     quadSource = quad.T3_gauss(0)
#     N = space.operator_dof2val_tri(quadSource.uv)
#     J = space.get_tris_mapping().J
#     wSource = quadSource.w[0]*source[np.newaxis, :]*J[:, np.newaxis]
#     Fint = N.T.dot(wSource.flatten())
#     return Fint

class HyperElasticitySolver():
    """ A class to set up and compute a 2d linear Hyper Elastic problem with FEM method """
    def __init__(self, mesh:sm.sMesh, matlaw:hy.HyperElasticMaterial,
                 physname2dirichlet:dict|None= None,
                 physname2neumann:dict|None= None,
                 source=np.array([0., 0.]),
                 space_constructor=fs.FEMSpaceP1):
        self.mesh = mesh
        self.law = matlaw
        self.quad_bilinform = quad.T3_gauss(0)
        self.quad_corners = quad.T3_nodes()
        self.space = space_constructor("DisplacementSpace", self.mesh, vecdim=2)
        self.B:sp.sparse.spmatrix = self.space.operator_dof2grad_tri( # pylint: disable=C0103
                                    self.quad_bilinform.uv)
        self.Bb = self.space.operator_dof2grad_tri( # pylint: disable=C0103
                                    self.quad_bilinform.uv, raw_data = True).squeeze()
        self.BbT = self.Bb.swapaxes(-1,-2) # pylint: disable=C0103
        self.NatVertices = self.space.operator_dof2val_tri( # pylint: disable=C0103
                                     self.quad_corners.uv)
        self.physname2dirichlet = {} if physname2dirichlet is None else physname2dirichlet
        self.physname2neumann = {} if physname2neumann is None else physname2neumann
        self.source = source
        gname2dofid, gname2dofxy = bc.get_dofs_and_interpolation_points(
            mesh, self.physname2dirichlet.keys(), self.space)
        self.free_dofs, self.fixed_dofs, self.fixed_values = bc.compute_dirichlet(
            self.space, self.physname2dirichlet, gname2dofid, gname2dofxy)
        # Next is a tentative to imporve the speed of assembly, see
        # _compute_internal_stiffness_blocs
        def Ksparse_indexes(): # pylint: disable=C0103
            scal_t2dofs = self.space.a2dofs
            vecdim = self.space.vecdim
            vec_t2dofs = np.column_stack([vecdim*scal_t2dofs + i for i in range(vecdim)])
            #print (vec_t2dofs.shape)
            row = np.repeat(vec_t2dofs, vec_t2dofs.shape[1], axis = 1)
            col = np.repeat(vec_t2dofs, vec_t2dofs.shape[1], axis = 0)
            row.shape = row.size
            col.shape = col.size
            return (row, col)
        self.Kindexes = Ksparse_indexes() # pylint: disable=C0103
        # this need to be implemented latter
        # self.Fneumann = computeNeumann(self.space, self.physname2neumann)
        # self.Fint = computeVolumeLoad(self.space, source)

    def compute_energy(self, U:FNDArray)-> float: # pylint: disable=C0103
        """ return the stored energy associated to nodal displacement e"""
        # pylint: disable=C0103
        F = self.compute_F(U)
        J0 = self.space.get_tris_mapping().J
        e = self.law.potentialF(F)
        return np.sum(J0*self.quad_bilinform.w[0]*e)

    def compute_internal_forces(self, U:FNDArray)->FNDArray: # pylint: disable=C0103
        """ compute the internal force vecor F_int, such as F_int.delta_u = delta w_int """
        # pylint: disable=C0103
        F = self.compute_F(U)
        J0 = self.space.get_tris_mapping().J
        w = J0*self.quad_bilinform.w[0]
        wPKI = w[..., np.newaxis, np.newaxis]*self.law.PKI(F)
        internal_forces = self.B.T@wPKI.flatten()
        return internal_forces

    def compute_internal_stiffness(self, U:FNDArray)->sp.sparse.csr_array:
        """ compute the internal tangent stifness matrix K_int, such as 
            K_int.delta_u = sum_omega dfintdu delta_u """
        # pylint: disable=C0103
        F = self.compute_F(U)
        J0 = self.space.get_tris_mapping().J
        w = J0*self.quad_bilinform.w[0]
        wdPKIdF = w[..., np.newaxis, np.newaxis]*(self.law.dPKIdF(F).reshape(-1, 4, 4))
        wdPKIdF = st.regular_block_diag_to_csr(wdPKIdF)
        return self.B.T@wdPKIdF@self.B

    def compute_internal_stiffness_blocs(self, U:FNDArray)->sp.sparse.csr_array:
        """ compute the internal tangent stifness matrix K_int, such as 
            K_int.delta_u = sum_omega dfintdu delta_u :  A tentative to speed up the assembly 
            Not successful as of now 
        """
        # pylint: disable=C0103
        F = self.compute_F(U)
        J0 = self.space.get_tris_mapping().J
        w = J0*self.quad_bilinform.w[0]
        wdPKIdF = w[..., np.newaxis, np.newaxis]*(self.law.dPKIdF(F).reshape(-1, 4, 4))
        Bb  = self.Bb
        BbT = self.BbT
        K_blocs = BbT@wdPKIdF@Bb
        K_blocs.shape = K_blocs.size
        K_csr = sp.sparse.csr_array((K_blocs, self.Kindexes))
        return K_csr

    def _compute_Hsparse(self, U:npt.NDArray)->npt.NDArray: # pylint: disable=C0103
        """ compute H, the gradient of the displacment field at each gauss point """
        # pylint: disable=C0103
        Ushape = U.shape
        nelem = self.mesh.getNbTris()
        ndofs = self.space.size()
        U.shape = ndofs
        vecdim = self.space.vecdim
        H = self.B@U
        H.shape = (nelem, vecdim, vecdim)
        U.shape = Ushape
        return H

    def _compute_Hblocs(self, U:npt.NDArray)->npt.NDArray: # pylint: disable=C0103
        """ compute H, the gradient of the displacment field at each gauss point 
            This is a tentative version to speed up the computation compared to compute Hsparse
            Not effective as of now
        """
        # pylint: disable=C0103
        Ushape = U.shape
        nelem = self.mesh.getNbTris()
        ndofs = self.space.size()
        vecdim = self.space.vecdim
        U.shape = ndofs
        Bb = self.Bb
        Ushape = U.shape
        nelem = Bb.shape[0]
        U.shape = (ndofs//vecdim, vecdim)
        Utris = U[self.mesh.getTris2Vertices()]
        Utris.shape = (nelem, 3*vecdim, 1)
        H = Bb@Utris
        H.shape = (nelem, 2, 2)
        U.shape = Ushape
        return H

    def compute_F(self, U:npt.NDArray)->npt.NDArray: # pylint: disable=C0103
        """ compute F, the gradient of the position field at each gauss point  """
        return  self._compute_Hsparse(U) +  np.eye(2)[np.newaxis]

    def solve(self, loadfac:float = 1.,
              Uguess:FNDArray|None=None,  # pylint: disable=C0103
              verbose = True,
              itmax =10)-> FNDArray:
        """ solve the non linear system """
        if Uguess is None :
            Uguess = np.zeros(self.B.shape[1])
        U = Uguess.flatten() # pylint: disable=C0103
        U_fixed= loadfac*self.fixed_values # pylint: disable=C0103
        U[self.fixed_dofs] = U_fixed
        it = 0
        R = self.compute_internal_forces(U) # pylint: disable=C0103
        R_free = R[self.free_dofs]          # pylint: disable=C0103
        res_norm = np.linalg.norm(R_free)
        if verbose:
            print(f"iter {it:d}, res = {res_norm:.3e}")
        while (res_norm > 1.e-5 or not np.isfinite(res_norm)) and it < itmax:
            K = self.compute_internal_stiffness(U) # pylint: disable=C0103
            K_free = K[:,self.free_dofs][self.free_dofs, :] # pylint: disable=C0103

            line = sp.sparse.linalg.spsolve(K_free, R_free)
            alpha =1.
            res_norm1 = res_norm
            while (res_norm1 >=res_norm  or not np.isfinite(res_norm)) and alpha > 1.e-12:
                if alpha != 1.:
                    print("alpha", alpha)
                U1 = U.copy() # pylint: disable=C0103
                U1[self.free_dofs] -= alpha*line
                R_free = self.compute_internal_forces(U1)[self.free_dofs] # pylint: disable=C0103
                res_norm1 = np.linalg.norm(R_free)
                alpha = 0.5*alpha
            U = U1 # pylint: disable=C0103
            res_norm = res_norm1
            it += 1
            if verbose:
                print(f"iter {it:d}, res = {res_norm:.3e}")
        if it == itmax:
            warn("Newton did not converge after {it:d} iteration")
        else:
            if verbose:
                print(f"converged at iter {it:d}, res = {res_norm:.3e}")
        return U

class TestHyperelasticitySolver(unittest.TestCase):
    """ Testing Hyperelasticity Solver """
    def setUp(self) -> None:
        law = hy.CompressibleNeoHookeen(E=1., nu=0.3, hyp=hy.MaterialHypothesis.PLANESTRAIN)
        meshgenerator = mg.l_shape
        element_size = 1.
        gmsh.initialize()
        gmsh.option.setNumber('General.Terminal', 0)
        model_name, physical_dict = meshgenerator(element_size) # pylint: disable=W0612
        phys2dirichlet = {'dOmega_left': {'x':0., 'y':0.}}
        phys2neumann = {'dOmega_right':{'x': 0.01}}
        edgegroupe_names = list(phys2dirichlet.keys())+ list(phys2neumann.keys())
        mesh = sm.gmshModel2sMesh(model_name, edgegroupe_names)
        self.pb = HyperElasticitySolver(mesh=mesh, matlaw = law)
        gmsh.finalize()
        return super().setUp()

    def test_patch(self):
        """ Patch test: verifying energy, fint, and kint computation """
        verbose = True
        if verbose:
            print("#")
        pb = self.pb
        s0 = 0.5*np.sum(pb.space.get_tris_mapping().J)
        F = np.array([2.,1.,3.,4.]).reshape((2,2)) # pylint: disable=C0103
        def ufun(coord):
            h= F-np.eye(2)
            return (h[np.newaxis]@coord[..., np.newaxis])
        u = pb.space.interpolate(ufun)
        energy_fem = pb.compute_energy(u)
        energy_exact = s0*pb.law.potentialF(F)
        energy_err = np.fabs(energy_exact-energy_fem)
        if verbose:
            print(f"energy error between exact and Fem: {energy_err:.3e}")
        ok = energy_err < 1.e-9
        self.assertTrue(ok)
        if not ok:
            warn("energy comparaison between exact and Fem failed")
            if verbose:
                print("Energy for imposed displacement (Homogeneous F)")
                print(f"energy exa: {energy_exact:.3e}")
                print(f"energy fem: {energy_fem:.3e}")
        error_fint = sp.optimize.check_grad(pb.compute_energy, pb.compute_internal_forces, u)
        if verbose:
            print(f"internal force error between exact and FD approx : {error_fint:.3e}")
        ok = energy_err < 1.e-7
        self.assertTrue(ok)
        if not ok:
            warn("internal forces comparaison between exact and FD approx failed")
            if verbose:
                print(f"error internal forces between exact : {error_fint:.3e}")
                f_int_approx = spopt.approx_fprime(u, pb.compute_energy)
                f_int_fem = pb.compute_internal_forces(u)
                print('f_int_approx')
                print(f_int_approx)
                print('f_int_fem')
                print(f_int_fem)
        error_kint = spopt.check_grad(pb.compute_internal_forces, pb.compute_internal_stiffness, u)
        if verbose:
            print(f"internal stiffness error between exact and FD approx: {error_kint:.3e}")
        ok = error_kint < 1.e-6
        self.assertTrue(ok)
        if not ok:
            warn("internal stiffness comparaison between exact and FD approx")
            if verbose:
                print(f"error stiffness forces between and FD approx: {error_kint:.3e}")
                k_int_approx = spopt.approx_fprime(u, pb.compute_internal_forces)
                k_int_fem = pb.compute_internal_stiffness(u)
                print('k_int_approx')
                print(k_int_approx)
                print('k_int_fem')
                print(k_int_fem)

def solve(mesh:sm.sMesh, dirichlet_bc, loadstep, savedir:None|str = None, popup_gmsh = False):
    """ example main program """
    law = hy.CompressibleNeoHookeen(E=1., nu=0.3, hyp=hy.PLANESTRESS)
    pb = HyperElasticitySolver(mesh= mesh, matlaw = law, physname2dirichlet=dirichlet_bc)
    U= None # pylint: disable=C0103
    inc = 0
    gmsh.initialize()
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)
    if savedir is not None:
        gmsh.write(savedir+"post_options.opt")
    X= mesh.getTris2VerticesCoord() # pylint: disable=C0103
    for loadfac in loadstep:
        print(f"# increment {inc:5d}, loading factor {loadfac:.3e}")
        U = pb.solve(loadfac=loadfac, Uguess = U) # pylint: disable=C0103
        u = U.reshape(-1,2)[mesh.getTris2Vertices()]
        x = X+u
        F = pb.compute_F(U).reshape(-1, 2, 2) #  pylint: disable=C0103
        P = law.PKI(F) #  pylint: disable=C0103
        if law.hyp == hy.PLANESTRESS:
            lambda3 = law.l3_planestress(np.linalg.det(F))
            vL3 = gp.listPlotFieldTri(x, lambda3, #pylint: disable=C0103
                    f"lambda3_{inc:04d}", P0 = True)
        vF = gp.listPlotFieldTri(x, F, f"F_{inc:04d}", P0 = True) # pylint: disable=C0103
        vP = gp.listPlotFieldTri(x, P, f"P_{inc:04d}", P0 = True) # pylint: disable=C0103
        if savedir is not None :
            gmsh.view.write(vF, savedir+f"F_{inc:04d}"+".pos")
            gmsh.view.write(vP, savedir+f"P_{inc:04d}"+".pos")
            if law.hyp == hy.PLANESTRESS:
                gmsh.view.write(vL3, savedir+f"L3_{inc:04d}"+".pos")

        inc += 1
    if popup_gmsh:
        gmsh.fltk.run()
    gmsh.finalize()
