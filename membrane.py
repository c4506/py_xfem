""" Module to test idea for hyper elastic membrane. For now splitting F in F1@iF0 """

from __future__ import  annotations
import sys
import unittest
import functools

from typing import Literal
import numpy as np
#from scipy.sparse import coo_array, dok_array
import scipy as sp
import gmsh
#from mapping import Mapping3DTri
#import gmsh_mesh_model_interface as mi
import oneLevelSimplexMesh as sm
#from execTools import Profile
#from function_spaces import op2sparse

import hyperelasticity as hyp
import gmsh_mesh_generator as mg
from smallsize_tensor import FNDArray, metric, dmetric, d2metric, inv, det, df_num, check_relerr
from smallsize_tensor import vecprodmat

import sparse_tools as spt
from sparse_tools import regular_block_diag_to_csr_v2
import function_spaces as fs
import gmsh_post_pro_interface as gp

class DofManager():
    """ Class to compute the degree of freedom associated to the mesh and the dirichlet BC """
    def __init__(self, m):
        self.m = m
        self.vid = self.m.getUsedVertices()  # np.unique(tris.flatten())
        if len(self.vid) != len(self.m.getVerticesCoord()):
            self.allnodes = False
            self.vid2dofid = dict(zip(self.vid, np.arange(len(self.vid))))
            self.a2dofs = np.array([[self.vid2dofid[vid] for vid in t]
                                for t in self.m.getTris2Vertices()])
        else:
            self.allnodes = True
            self.vid2dofid = np.arange(len(self.vid))
            self.a2dofs = self.vid2dofid[self.m.getTris2Vertices()]
        self.nelem    = len(self.a2dofs)
        self.nscaldof = len(self.vid)

class Membrane():
    """ Class to represent membrane problem in 3d """
    # pylint: disable=C0103
    def __init__(self, law:hyp.HyperElasticMaterial, mesh:sm.sMesh):
        assert law.hyp == hyp.MaterialHypothesis.PLANESTRESS
        self._B = None
        self._Natv = None
        self._N = None
        self.law = law
        self.mesh = mesh
        self.dofm = DofManager(mesh)
        self.initial_zstretch = 2.
        XYZ = mesh.getVerticesCoord()
        self.X = XYZ[self.dofm.vid]
        if self.X.shape[-1] == 2 :
            print("Assuming a plannar initial membrane")
            self.X = np.column_stack([self.X, np.zeros(self.X.shape[:-1])])
        elif self.X.shape[-1] == 3 :
            print("The initial membrane is assumed to be a curved surface")
        else :
            raise ValueError("The mesh vertices must have 2d or 3d coordinates")
        # x = self.X.copy()
        # x[...,2] = initial_zstretch*x[...,2]
        # self.x = x
        self.F0 = self.getGrad(self.X)
        self.iF0 = inv(self.F0)
        self.M  = metric(self.F0)
        self.J0 = np.sqrt(det(self.M))
        self.iM = inv(self.M)


    def getGrad(self, A:FNDArray, algo:Literal[0,1] = 0)-> FNDArray:
        """ return the gradient of the nodal vector field stored in A
            input A[i,j] coordinate j of field A at node j
            return  G(nelem, 3, 2) matrix, such as :
            G[ie, j, 0] is the derivative of A in elem ie with regard to reference coordinate u
            G[ie, j, 1] is the derivative of A in elem ie with regard to reference coordinate v
        """
        if algo == 0:
            return (self.getB()@A.flatten()).reshape((self.dofm.nelem, 3, 2))
        if algo == 1:
            t2vf = self.dofm.a2dofs
            Atris = A[t2vf]
            BelemsT = np.array([[[-1., 1. ,0.],[-1.,0.,1.]]]).swapaxes(-1,-2)
            return Atris.swapaxes(-1,-2)@BelemsT

    def getB(self)-> sp.sparse.spmatrix:
        """ return the sparse matrix B such as B@x return the values of the (FE) local gradient 
            of the vector field x xith regard to the parameters uv of the point in the space
            of the referent element, for each element
        """
        if self._B is None:
            nelem = self.dofm.nelem
            dN = np.array([[[-1., 1. ,0.],[-1.,0.,1.]]])
            dN = np.broadcast_to(dN, (nelem,) + dN.shape)
            self._B = fs.op2sparse(dN, self.dofm.nscaldof, self.dofm.a2dofs, 3, 3, 1, 2 )
        return self._B

    def getNatVertices(self)-> sp.sparse.spmatrix:
        """ return the sparse matrix B such as B@x return the values of the (FE) local gradient 
            of the vector field x xith regard to the parameters uv of the point in the space
            of the referent element, for each element
        """
        if self._Natv is None:
            nelem = self.dofm.nelem
            N = np.array([[[1., 0. ,0.]], [[0.,1.,0.]], [[0.,0.,1.]] ])
            N = np.broadcast_to(N, (nelem,) + N.shape)
            self._Natv = fs.op2sparse(N, self.dofm.nscaldof, self.dofm.a2dofs, 3, 3, 3, 1 )
        return self._Natv

    def getN(self)-> sp.sparse.spmatrix:
        """ return the sparse matrix B such as B@x return the values of the (FE) local gradient 
            of the vector field x xith regard to the parameters uv of the point in the space
            of the referent element, for each element
        """
        if self._N is None:
            nelem = self.dofm.nelem
            N = np.array([[[1./3., 1./3. , 1./3.]]])
            N = np.broadcast_to(N, (nelem,) + N.shape)
            self._N = fs.op2sparse(N, self.dofm.nscaldof, self.dofm.a2dofs, 3, 3, 1, 1 )
        return self._N

    def getF(self, x:FNDArray, algo:Literal[0,1] = 0)-> FNDArray:
        """
            return an array of size nelem, 3, 3
            such as F[k].dX = dx where DX is is a tangent vector included
            in element k in the reference configuration 
        """
        if algo == 0 :
            return self.getGrad(x, algo = 0)@self.iF0
        if algo == 1 :
            return self.getGrad(x, algo = 1)@self.iF0

    def surf0(self):
        """ return the initila surface of the membrane """
        return 0.5*np.sum(self.J0)

    def surf(self, x:FNDArray)-> np.float64:
        """ return the deformed surface of the membrane """
        J1 = det(self.getGrad(x))
        return 0.5*np.sum(J1)

    def getC(self, F1:FNDArray)-> FNDArray:
        """ compute the C=FT@F component (2x2) for each element 
            Where F1 contain the gradient with regard to local coordinates of the deformed position
            dx= F1@du
        """
        m = metric(F1)
        return self.iM@m

    def getdCdF1(self, F1:FNDArray)-> FNDArray:
        """ return the derivative of C with regard to F1"""
        dm = dmetric(F1)
        #dm = 0.5*(dm+np.swapaxes(dm, -3,-4))
        return np.einsum("...ak, ...kbic -> ...abic", self.iM, dm)

    def getd2Cd2F1(self, F1:FNDArray)-> FNDArray:
        """ return the second derivative of C with regard to F1"""
        d2m = d2metric(F1)
        #dm = 0.5*(dm+np.swapaxes(dm, -3,-4))
        return np.einsum("...ak, ...kbicjd -> ...abicjd", self.iM, d2m)

    def strain_energy(self, x:FNDArray)-> np.float64:
        """ return the integrates strain energy """
        F1 = self.getGrad(x, algo = 0)
        C = self.getC(F1)
        J0 = self.J0
        return 0.5*np.sum(J0*self.law.potentialC(C))

    def dpotentialdF1(self, F1: FNDArray) -> FNDArray:
        """ return the first derivative of the strain enegy with regard to F1 fo each element 
            (a stress measure)
        """
        C = self.getC(F1)
        dCdF1 = self.getdCdF1(F1)
        dphidC =  0.5*self.law.PKII(C)
        return np.einsum("...ef, ...efia -> ...ia", dphidC, dCdF1)

    def d2potentiald2F1(self, F1: FNDArray) -> FNDArray:
        """ return the second derivative of the strain enegy with regard to F1 fo each element 
            (a tangent stifness) 
        """
        C = self.getC(F1)
        dCdF1    = self.getdCdF1(F1)
        d2CdF1_2 = self.getd2Cd2F1(F1)
        dphidC= 0.5*self.law.PKII(C)
        d2phid2C= 0.5*self.law.dPKIIdC(C)
        dCdF1_d2phid2C_dCdF1 = np.einsum(" ...efia, ...efgh, ...ghjb   -> ...iajb",
                                         dCdF1, d2phid2C ,dCdF1)
        dphidC_d2Cd2F1 = np.einsum("...ef, ...efiajb -> ...iajb", dphidC, d2CdF1_2)
        d2phid2CF1 = dCdF1_d2phid2C_dCdF1 + dphidC_d2Cd2F1
        return d2phid2CF1

    def internalForce(self, *, x:FNDArray|None =None, F1:FNDArray|None=None)-> FNDArray:
        """ return the finite element internal force vector """
        if (x is None) and (F1 is None) :
            raise ValueError("input either x or F1")
        if F1 is None and x is not None:
            F1 = self.getGrad(x, algo = 0)
        if F1 is not None:
            J0 = self.J0[..., np.newaxis, np.newaxis]
            dphidF1 = self.dpotentialdF1(F1)
            return (0.5*J0*dphidF1).flatten()@self.getB()
        raise ValueError("input either x or F1")

    def internalStiffness(self, *, x:FNDArray|None =None, F1:FNDArray|None=None) \
        -> sp.sparse.csr_array:
        """ return the finite element internal stifness as a asparse matrix  """
        if (x is None) and (F1 is None) :
            raise ValueError("input either x or F1")
        if F1 is None and x is not None:
            F1 = self.getGrad(x, algo = 0)
        if F1 is not None:
            C = self.getC(F1)
            w = 0.5*self.J0.reshape(self.J0.shape + (1,)*4)
            d2phid2F1 = (w*self.d2potentiald2F1(F1)).reshape(C.shape[:-2]+(6,6))
            d2phid2F1 = spt.regular_block_diag_to_csr(d2phid2F1)
        Ke =   self.getB().T@d2phid2F1@self.getB()
        return Ke

    def normalPerElem(self, x:FNDArray|None =None, F1:FNDArray|None=None)-> FNDArray:
        """ return the normal to each triangle of the deformed membrane """
        if (x is None) and (F1 is None) :
            raise ValueError("input either x or F1")
        if F1 is None and x is not None:
            F1 = self.getGrad(x, algo = 0)
        if F1 is not None :
            return np.cross(F1[...,0], F1[...,1])
        raise ValueError("input either x or F1")

    def dnormalPerElem(self, x:FNDArray|None =None, F1:FNDArray|None=None):
        """ return the derivatives of the normal per element as a function of x,
        the nodals positions"""
        if (x is None) and (F1 is None) :
            raise ValueError("input either x or F1")
        if F1 is None and x is not None:
            F1 = self.getGrad(x, algo = 0)
        if F1 is not None:
            Q0 = vecprodmat(F1[...,0])
            Q1 = vecprodmat(F1[...,1])
            dn_elem = np.zeros(F1.shape[:-2] + (3,6))
            dn_elem[...,[0, 2, 4]] = -Q1
            dn_elem[...,[1, 3, 5]] =  Q0
            dn_elem_sparse = regular_block_diag_to_csr_v2(dn_elem)
            opdnormal = dn_elem_sparse@self.getB()
            opdnormal.sort_indices()
            return opdnormal
        else:
            raise ValueError("input either x or F1")

    def pressureForcePerElem(self, *, p:float, x:FNDArray|None =None, F1:FNDArray|None=None)\
        -> FNDArray:
        """ return the pressure force per element, as a function of p and x (or p and F1)  """
        return  0.5*p* self.normalPerElem(x = x, F1 = F1)

    def pressureForce(self, *, p:float, x:FNDArray|None =None, F1:FNDArray|None=None)-> FNDArray:
        """ return the "nodal" pressure force per node, as a function of p and x (or p and F1)  """
        Fe = self.pressureForcePerElem(p=p, x=x, F1=F1)
        N = self.getN()
        return(N.T@Fe.flatten()).reshape(( -1, 3))

    def pressureStiffnessPerElem(self, *, p:float, x:FNDArray|None =None, F1:FNDArray|None=None)\
        -> sp.sparse.csr_array:
        """ return the derivative of the pressure force per element with reagardto x
            (the deformed nodal position), as a function of   p, x (or p F1)"""
        return 0.5*p*self.dnormalPerElem(x=x, F1 =F1)

    def pressureStiffness(self, *, p:float, x:FNDArray|None =None, F1:FNDArray|None=None)\
        -> sp.sparse.csr_array:
        """ return the finite element internal stiffness as a sparse matrix  """
        N = self.getN()
        return 0.5*p*N.T@self.dnormalPerElem(x=x, F1 =F1)

    def postProDofVector(self, U:FNDArray, pos:FNDArray|None = None):
        """ return X and U in A 3d array : U[i,j,k] is the displacement in direction k of node j of
            element i. Usefull for gmsh post-pro"""
        if pos is None :
            assert U.shape == (self.dofm.nscaldof,3)
            elemvertexVals = U[self.dofm.a2dofs]
            return self.mesh.getTris2VerticesCoord(), elemvertexVals
        else:
            raise NotImplementedError()

class TestMembrane(unittest.TestCase):
    """ Unit test class for the Membrane class """
    def test(self):
        # pylint: disable=C0103
        """ unit test, calling test_membrane """
        verbose = 1
        abstol = 1.e-5
        reltol = 1.e-3
        E = 1.
        nu = 0.4
        law1 = hyp.CompressibleNeoHookeen(E = E, nu = nu, hyp = hyp.PLANESTRESS)
        argv = sys.argv
        gmsh.initialize(argv)
        gmsh.option.setNumber('General.Terminal', 0)
        radius = 1.
        height = 2.
        hscale = 1.
        modelname, physdic = mg.cylinder(radius, height, hscale)
        mesh1 = sm.gmshModel2sMesh(modelname, physdic.keys(), dim=3)
        mem = Membrane(law1, mesh1)
        randomscale = 0.
        def phix(X):
            """ A simple transformation X -> x"""
            # pylint: disable=C0103
            x = X.copy()
            x[:,0] = x[:,0]*0.5
            x[:,1] = x[:,1]*0.5
            shape = x.shape
            x += randomscale*np.random.rand(int(np.prod(shape))).reshape(shape)
            return x
        X = mem.X
        x = phix(X)
        res = dict()
        F0 = mem.getGrad(X, algo = 0)
        F1 = mem.getGrad(x, algo = 0)
        # F0 = np.array([[[1,0], [0,1],[0,0]]])
        # F1 = np.array([[[1,0], [0,1],[0,0]]])
        # F0 = np.random.rand(nelem*6).reshape((nelem,3,2))
        # F1 = np.random.rand(nelem*6).reshape((nelem,3,2))
        # F1 = np.random.rand(6).reshape(3,2)
        res.update(test_law_PKII_convbasis(mem.law, F0, F1,
                                        abstol = abstol, reltol = reltol, verbose=verbose))
        res.update(test_membrane(mem, x, abstol = abstol, reltol = reltol, verbose = verbose))
        for (test_name, test_ok) in res.items():
            if verbose >=2 :
                print(f"{test_name} success : {test_ok}")
            elif verbose >=1 and not test_ok :
                print(f"{test_name} Failed !")
        for (test_name, test_ok) in res.items():
            self.assertTrue(test_ok, msg=f"test {test_name} Failed !")
        gmsh.finalize()

def test_membrane(mem:Membrane, x:FNDArray,
                  reltol:float= 1.e-3, abstol:float = 1.e-5,
                  verbose:Literal[0,1,2] = 0) -> dict:
    """ check correctness of membrane operator (This can be slow : use a small mesh)
        x : position of the nodes in the deformed configuration for which one want to test
            the internalForces and internalStiffness
    """
    # pylint: disable=C0103
    # surf0 = mem.surf0()
    # surf1 = mem.surf(x)
    # if verbose:
    #     print(f"surface 0, discrete : {surf0:10.4f}")
    #     print(f"surface 1, discrete : {surf1:10.4f}")
    X = mem.X
    res = dict()
    F0 = mem.getGrad(X, algo = 0)
    F1 = mem.getGrad(x, algo = 0)
    if verbose > 0 :
        print("## test dpotentialdF1")
    dphidF1 = mem.dpotentialdF1(F1)
    dphidF1_num  = np.zeros(dphidF1.shape)
    for k in np.ndindex(F0.shape[:-2]):
        F0k = F0[k]
        F1k = F1[k]
        def phiF0F1(F0k, F1k):
            M = metric(F0k)
            iM = inv(M)
            m = metric(F1k)
            C = iM@m
            return mem.law.potentialC(C)
        phiF1 = functools.partial(phiF0F1,  F0k)
        dphidF1_num[k] = df_num(phiF1, F1k, (3,2), ())
    dphidF1_ok   = check_relerr(dphidF1, dphidF1_num, \
        reltol=reltol, abstol=abstol, verbose = verbose)
    dphidF1_name = "check_dphidF1"
    res.update({dphidF1_name: dphidF1_ok})
    if verbose > 0:
        print("## test d2potentiald2F1")
    d2phid2F1 = mem.d2potentiald2F1(F1)
    d2phid2F1_num  = np.zeros(d2phid2F1.shape)
    for k in np.ndindex(F0.shape[:-2]):
        F0k = F0[k]
        F1k = F1[k]
        def dphidF1_fun(F1k):
            """ local function of F1 of element k, needed for the numerical derivative """
            M = metric(F0k)
            iM = inv(M)
            m = metric(F1k)
            dm = dmetric(F1k)
            C = iM@m
            dCdF1 = np.einsum("...ak, ...kbic -> ...abic", iM, dm)
            dphidC =  0.5*mem.law.PKII(C)
            return np.einsum("...ef, ...efia -> ...ia", dphidC, dCdF1)
        d2phid2F1_num[k] = df_num(dphidF1_fun, F1k, (3,2), (3,2))
    d2phid2F1_ok   = check_relerr(d2phid2F1, d2phid2F1_num,
                                  reltol=reltol, abstol=abstol, verbose = verbose)
    d2phid2F1_name = "check_d2phid2F1"
    res.update({d2phid2F1_name: d2phid2F1_ok})
    if verbose > 0:
        print("## test internal Forces")
    Fi = mem.internalForce(x=x)
    Fi_num = sp.optimize.approx_fprime(x.flatten(), lambda x : mem.strain_energy(x.reshape(-1,3)))
    Fi_ok  = check_relerr(Fi, Fi_num, reltol=reltol, abstol=abstol, verbose = verbose)
    Fi_name = "check_internal_forces"
    res.update({Fi_name: Fi_ok})
    if verbose:
        print("## test internal Stiffness")
    Ki = mem.internalStiffness(x=x)
    Ki_num = sp.optimize.approx_fprime(x.flatten(), \
        lambda x : mem.internalForce(x = x.reshape((-1,3))))
    Ki_ok  = check_relerr(Ki, Ki_num, reltol=reltol, abstol=abstol, verbose= verbose)
    Ki_name = "check_internal_stiffness"
    res.update({Ki_name: Ki_ok})
    if verbose:
        print("## test dndx (variation of the normals as a function of value of x at nodes)")
    dndx = mem.dnormalPerElem(x=x)
    dndx_num = sp.optimize.approx_fprime(x.flatten(), \
        lambda x : mem.normalPerElem(x = x.reshape((-1,3)) ).flatten())
    dndx_num = sp.sparse.csr_array(dndx_num)
    dndx_num.sort_indices()
    dndx_ok  = check_relerr(dndx.todense(), dndx_num.todense(), \
        reltol=reltol, abstol=abstol, verbose= verbose)
    dndx_name = "check_dndx"
    res.update({dndx_name: dndx_ok})
    if verbose:
        print("## test Ke(p,x) (variation of the force vector associated to pressure forces \
                            as a function of value of x at nodes")
    p = 1.
    Ke = mem.pressureStiffness(p = p, x=x)
    Ke_num = sp.optimize.approx_fprime(x.flatten(), \
        lambda x : mem.pressureForce(p= p, x = x.reshape((-1,3)) ).flatten())
    Ke_num = sp.sparse.csr_array(Ke_num)
    Ke_num.sort_indices()
    Ke_ok  = check_relerr(Ke.todense(), Ke_num.todense(),\
        reltol=reltol, abstol=abstol, verbose= verbose)
    Ke_name = "check_Ke"
    res.update({Ke_name: Ke_ok})
    return res

def test_law_PKII_convbasis(law:hyp.HyperElasticMaterial, F0:FNDArray, F1:FNDArray,
                            abstol:float = 1.e-5,
                            reltol:float = 1.e-3,
                            verbose:Literal[0,1,2] = 0) -> dict:
    """ 
        let  G_i = dX/du_i and g_i = dx/du_i
        are the "Natural basis"  or Convectives bais for the initial and deformed configuration.
        let m_ij = g_i.g_j and  M_ij = G_i.G_j  be metric of theses basis. 
        m^ij, M^ij the coeficients of the inverse metrics
        let G^i and g^i be the dual basis, such as G_i.G^j = delta_ij and  g_i.g^j = delta_ij 
        we have g^i = m^ij_gj and G^i = m^ij G_i
        let dX = F0du and dx = F1du
        F0_Ia = (G_a)I, F1_ia = (g_a)i
        F = F1 F0^{-1}
        F = g_a x G^a
        C = F^T F = F0^{-T} F1^T F1 F0^{-1}
        C = g_a . g_b  G^a x G^b
        C =  m_ab G^a x G^b
        C = M^ac m_cb G_a x G^b
        the invariant of C are then equal to the invariant of M^ac m_cb 
        Note that if C is of course a symetric tensor, M^ac m_cb  is not a symetric matrix.
        if every thing was tested in materiallaw for C represented in a basis in wich it's composant
        are a symetric matrix, we need to check if the code is still correct when
        it is not the case.
        This is the purpose of this function. The input are the coefficients of F0 and F1, 
        and we check using numerical differentiation that we get the correct answers 
        for PKII and dPKIIdC
    """
    # pylint: disable=C0103
    if verbose > 0:
        print("## test conv Basis")
    res_check = {}
    d2m = d2metric(F1)
    d2m_num = df_num(dmetric, F1, (3,2), (2,2,3,2) )
    res_check["check metric hessian"] = check_relerr(d2m, d2m_num,
            checkname= "metric hessian", abstol = abstol, reltol = reltol, verbose=verbose)
    def compC(F1):
        # pylint: disable=C0103
        M = metric(F0)
        iM = inv(M)
        m = metric(F1)
        C = iM@m
        return C
    C = compC(F1)
    PKII     = law.PKII(C)
    PKII_num = 2*df_num(law.potentialC, C, (2,2), ())
    res_check["check PKII asym C"] = check_relerr(PKII, PKII_num,
                checkname= "PKII asym C", abstol = abstol, reltol = reltol, verbose=verbose)

    dPKIIdC     = law.dPKIIdC(C)
    dPKIIdC_num = df_num(law.PKII, C, (2,2), (2,2))
    res_check["check dPKIIdC asym C"] = check_relerr(dPKIIdC, dPKIIdC_num,
                checkname= "dPKIIdC asym C",  abstol = abstol, reltol = reltol, verbose=verbose)
    return res_check

def test():
    """ Small test to quick check some functionnalities """
    # pylint: disable=C0103
    E = 1.
    nu = 0.4
    law1 = hyp.CompressibleNeoHookeen(E = E, nu = nu, hyp = hyp.PLANESTRESS)
    argv = sys.argv
    gmsh.initialize(argv)
    radius = 1.
    height = 2.
    hscale = 1.
    modelname, physdic = mg.cylinder(radius, height, hscale)
    mesh1 = sm.gmshModel2sMesh(modelname, physdic.keys(), dim=3)
    nv, nt = (mesh1.getNbVertices(), mesh1.getNbTris())
    print(f"nv: {nv:d}, nt {nt:d}")
    #phys2Dirichlet = {'bottom': {'x':0., 'y':0., 'z':0.}, 'top': {'x':0., 'y':0., 'z':0.}}
    #xy             = np.array([[0.,0.,0.], [1.,0.,0.], [0.,1.,0.], [1,1,0], [2,0,0]])
    #tris           = np.array([ [1,3,2] , [1,4,3]])
    #mesh           = sm.sMesh(xy, tris)
    mem = Membrane(law1, mesh1)
    x = mem.X.copy()
    x[:,2] = 1.5*x[:,2]
    disp = x - mem.X
    trisXYZ, trisDISP = mem.postProDofVector(disp)
    gp.listPlotFieldTri(trisXYZ, trisDISP, P0=False,
                            viewname='x', VectorType="Arrow",
                            DisplacementFactor=1.)
    Fint = mem.internalForce(x = x).reshape((-1, 3))
    trisXYZ, trisFint = mem.postProDofVector(Fint)
    gp.listPlotFieldTri(trisXYZ, trisFint, P0=False,
                            viewname='Fint', VectorType="Arrow",
                            DisplacementFactor=1.)
    Fext_e =  mem.pressureForcePerElem(p = 1., x=x)
    gp.listPlotFieldTri(trisXYZ, Fext_e, P0=True,
                        viewname='pressure resultant per elem', VectorType="Arrow")
    Fext_v = mem.pressureForce(p = 1., x =x)
    trisXYZ, trisFext = mem.postProDofVector(Fext_v)
    gp.listPlotFieldTri(trisXYZ, trisFext, P0=False,
                        viewname="pressure resultant per nodes", VectorType="Arrow")
    Kext = mem.pressureStiffness(p = 1., x =x)
    print(Kext.shape)
    print(nv*3, nv*3)

    gmsh.fltk.run()
if __name__ == "__main__":
    test()
