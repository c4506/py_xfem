#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 15 13:22:41 2023

@author: nchevaug
"""
# Note : This file was cut and past from another project of mine. there are lot of stuff here not usefull for PY_XFEM library. 
# Some clean up is necessary.

import time
import numpy as np
import scipy.sparse
import sksparse.cholmod
#import mesh as meshplotter
import gmsh
import itertools


# gmsh.option.set_number("Mesh.Lines",1)
# gmsh.option.set_number("Mesh.SurfaceEdges",0)
# gmsh.option.set_number("Mesh.LineWidth",3.)
# gmsh.option.set_number("Mesh.ColorCarousel",0)
# gmsh.option.set_number("PostProcessing.AnimationCycle",1)

def gmshListPlotScalarNodalField(xy, tri2node, nodedata, viewname = "T", IntervalsType = 3, NbIso=10):
        gv = gmsh.view.add(viewname)
        nnode = len(xy)
        ntri =  len(tri2node)
        ldata = np.moveaxis(np.hstack((xy, np.zeros((nnode,1)), nodedata[:, None]))[tri2node],1, -1).flatten()
        gmsh.view.addListData(gv, "ST", ntri, ldata)
        gmsh.view.option.setNumber(gv,"IntervalsType",IntervalsType)
        gmsh.view.option.setNumber(gv,"NbIso",NbIso)
        gmsh.view.option.setNumber(gv,"ShowElement",1)
        return gv

def gmshListPlotScalarElementField(xy, tri2node, elementdata, viewname = "T", IntervalsType = 3, NbIso=10):
        gv = gmsh.view.add(viewname)
        nnode = len(xy)
        ntri =  len(tri2node)
        txyz   = np.hstack((xy, np.zeros((nnode,1))))[tri2node]
        datat  = np.repeat(elementdata,3).reshape((ntri, 3,1))
        ldata = np.moveaxis((np.concatenate((txyz,datat), axis = 2)), 1,-1).flatten()
        gmsh.view.addListData(gv, "ST", ntri, ldata)
        gmsh.view.option.setNumber(gv,"IntervalsType",IntervalsType)
        gmsh.view.option.setNumber(gv,"NbIso",NbIso)
        gmsh.view.option.setNumber(gv,"ShowElement",1)
        return gv   


#def nodes2triangles(triangles2vid):
#    nv = np.amax(triangles2vid)+1
#    v2tri = [None]*int(nv)
#    for (tid, t) in enumerate(triangles2vid) :
#            for i in [0,1,2] :
#                a = v2tri[t[i]]
#                if  a is None :
#                    v2tri[t[i]] = set([tid])
#                else :
#                    a.add(tid)
#                    v2tri[t[i]] = a.copy()
#    return v2tri

def nodes2triangles(triangles2vid):
    nv = np.amax(triangles2vid)+1
    v2tri = [None]*int(nv)
    for (tid, t) in enumerate(triangles2vid) :
        for i in [0,1,2] :
            a = v2tri[t[i]]
            v2tri[t[i]] = {tid} if a is None else  a.union({tid})
    return v2tri

def createEdgesFromTri(triangles, pattern =1):
    """ edge is array of size nedge, vid0 = edge[i,0] and vid1 edge[i, 1] contain the node id, vid1 < vid0 
        tri2edge : for each line the edge id of the tri
        tri2edgedir : tri2edgedir[i,j] orientation used for edge j of triangle i in triangle i
        edge 2tri : each edge can have up to 2 tri. 
            column 0 : triangle at the left  of edge i or -1
            column 1 : triangle at the right of edge i or -1
        pattern 1 :    triangle edges : = [(0,1), (1,2), (2,0)]
                2
               / \
              2   1           
             /     \
            0___0___1
        pattern 2 :    triangle edges: = [(1,2), (2,0), (0,1)]
                2
               / \
              1   0           
             /     \
            0___2___1
        
      
        
    """
    #n=3
    #nsub = 2
    #comb = list(itertools.combinations(range(3), 2))  #[(0,1), (0,2), (1,2)] 
    if pattern == 1 :  comb = [(0,1), (1,2), (2,0)]
    if pattern == 2 :  comb = [(1,2), (2,0), (0,1)]
    
    sube = triangles[:,comb].reshape(-1,2)
    asort = sube.argsort(axis=1)
    sube,emap = np.unique(np.take_along_axis(sube,asort,1),axis=0,return_inverse=True)
    emap = emap.reshape(triangles.shape[0],-1)
    edges = sube
    tri2edge = emap
    #tri2edgedir = np.where(triangles[:,[0,2,1]] == edges[tri2edge,0],1, -1) #using [(0,1), (0,2), (1,2)] canonical ordering 
    if pattern ==1 : tri2edgedir = np.where(triangles == edges[tri2edge,0],1, -1) #using [(0,1), (1,2), (2,0)] canonical ordering 
    if pattern ==2 : tri2edgedir = np.where(triangles[:,[1,2,0]] == edges[tri2edge,0],1, -1) #using [(0,1), (1,2), (2,0)] canonical ordering 
    
    #Following only make sense with ordering [(0,1), (1,2), (2,0)] 
    edge2tri = np.full((len(edges),2),-1)
    for t, (te, ted)  in enumerate( zip(tri2edge, tri2edgedir)) :
        for e, ed in zip(te, ted):
            if ed == 1 :  edge2tri[e, 0] = t
            elif ed == -1 : edge2tri[e, 1] = t
    return edges, tri2edge, tri2edgedir, edge2tri



def initialize_front(topo, x, sol_init, T0=0, tol = 1.e-8):
    sol = np.ones(x.shape[0])+T0
    front = np.full(x.shape[0], False)
    #i = 0
    while True:
        solold = sol.copy()
        sol = sol_init(x)
        xold = x.copy()
        x, front = move_front(topo, front, x, solold-T0, sol-T0)
        if np.linalg.norm(x-xold) < tol:
            return x, front, sol


def move_front(topo, front, x, f_old, f):
    x_next = x.copy()
    front_next = front | ((f_old > 0) != (f > 0))

    for i in np.where(front_next)[0]:
        neigh =  topo.nodeNeighbour(i) #topo.neigh[topo.neigh_start[i]:topo.neigh_start[i+1]]
        opposite_neigh = neigh[(~front[neigh]) & ((f[neigh]>0) != (f[i]>0))]
        if opposite_neigh.size == 0:
            front_next[i] = False
            continue
        dist = np.linalg.norm(x[opposite_neigh]- x[i], axis=1)
        if f[i] < 0 : dist = -dist
        target = opposite_neigh[np.argmin(f[opposite_neigh]/dist)]
        alpha = np.clip((f[i])/(f[i]-f[target]), 0, 1)
        x_next[i] = alpha*x[target]+(1-alpha)*x[i]

#    if mesh.clamp_boundaries is not None:
#        mesh.clamp_boundaries(x_next)

    return x_next, front_next

class SimplexTopo():
    def __init__(self, t):
        self.tri2node = t.astype('int')
        self.node2tri, self.node2tri_start = SimplexTopo._upwardAdjacencies(self.tri2node)
        self.ntri    = len(self.tri2node) 
        self.nnode   = len(self.node2tri_start-1)
        self.neigh, self.neigh_start   = SimplexTopo._gen_neighbours(t)
            
    def nodes2triangles(self,ids):
        tris = np.hstack( list( self.node2tri[self.node2tri_start[i]:self.node2tri_start[i+1]]  for i in ids)   )
        return np.unique(tris)
    
    def nodeNeighbour(self, i):
        return self.neigh[self.neigh_start[i]:self.neigh_start[i+1]]
     
    @staticmethod
    def _create_sub_simplices(elements, subdim):
        n = elements.shape[1]
        nsub = subdim+1
        comb = list(itertools.combinations(range(n), nsub))
        sube = elements[:,comb].reshape(-1,nsub)
        asort = sube.argsort(axis=1)
        sube,emap = np.unique(np.take_along_axis(sube,asort,1),axis=0,return_inverse=True)
        emap = emap.reshape(elements.shape[0],-1)
        return sube, emap
    
    @staticmethod
    def _gen_neighbours(t):
        ''' from the array of triangle 2 vertices, computes the neighbors of each vertices
            neighbors of vertices i are neigh[neigh_start[i]:neigh_start[i+1]]
        '''
        hedges = np.hstack([
            [t[:,0],t[:,1]],[t[:,1],t[:,0]],
            [t[:,1],t[:,2]],[t[:,2],t[:,1]],
            [t[:,2],t[:,0]],[t[:,0],t[:,2]]]).T
        hedges = np.unique(hedges,axis=0).astype(np.int64)
        neigh_start = np.cumsum(np.hstack([[0],np.bincount(hedges[:,0])]))
        neigh = hedges[:,1].copy()
        return neigh, neigh_start
        
    @staticmethod
    def _upwardAdjacencies(t):
        flatt = t.flatten()
        order = np.argsort(flatt)
        elementstart = np.cumsum(np.hstack((np.array([0], dtype = 'int'),np.bincount(flatt[order]))))        
        element = order//t.shape[1]
        return element, elementstart

    

        

class Mesh():
#    def __init__(self, filename, verbose=False):
#        gmsh.initialize()
#        gmsh.open(filename)
#        gmsh.model.mesh.renumberNodes()
#        tags, xyz, _  = gmsh.model.mesh.getNodes(-1)
#        order = np.argsort(tags)
#        self.nodetags = tags[order]
#        xy = xyz.reshape([-1,3])[order,:-1]
#        assert(np.all(self.nodetags == np.arange(1, tags.size+1)))
#        boundary_nodes  = list( (gmsh.model.mesh.getNodes(dim = 1, tag = -1, includeBoundary = True)[0]-1).flatten()) #+ list(gmsh.model.mesh.getElements(0,-1)[2][0])
#        ttags, t = gmsh.model.mesh.get_elements_by_type(2)  #getElements return (types, [ [idtype_1] ... id_type_j[] ], [ [nodes_type_i] ...])
#        t = (t-1).reshape((-1,3))
#        self.xy  = xy
#        self.tri2node = t.astype('int')
#        self.boundary_nodes = boundary_nodes
#        #self.node2tri  = nodes2triangles(self.tri2node)
#        self.nnode = len(xy)
#        self.ntri  = len(t)
#        self.tri2tag = ttags
#        self.tag2tri = {tag: i for i,tag in enumerate(ttags) }
#        if verbose : print('mesh ' + gmsh.model.getFileName() + ' loaded, vertex, triangles :',  self.nnode, self.ntri )
        
    def __init__(self, gmshmodelname, verbose=False):
        self.gmshmodelname = gmshmodelname
        old_gmshmodelname = gmsh.model.getCurrent()
        gmsh.model.setCurrent(self.gmshmodelname)
        gmsh.model.mesh.renumberNodes()
        
        tags, xyz, _  = gmsh.model.mesh.getNodes(-1)
        order = np.argsort(tags)
        self.nodetags = tags[order]
        xy = xyz.reshape([-1,3])[order,:-1]
        assert(np.all(self.nodetags == np.arange(1, tags.size+1)))
        boundary_nodes  = list( (gmsh.model.mesh.getNodes(dim = 1, tag = -1, includeBoundary = True)[0]-1).flatten()) #+ list(gmsh.model.mesh.getElements(0,-1)[2][0])
        ttags, t = gmsh.model.mesh.get_elements_by_type(2)  #getElements return (types, [ [idtype_1] ... id_type_j[] ], [ [nodes_type_i] ...])
        t = (t-1).reshape((-1,3))
        self.xy  = xy
        self.tri2node = t.astype('int')
        self.boundary_nodes = boundary_nodes
        #self.node2tri  = nodes2triangles(self.tri2node)
        self.nnode = len(xy)
        self.ntri  = len(t)
        self.tri2tag = ttags
        self.tag2tri = {tag: i for i,tag in enumerate(ttags) }
        if verbose : print('mesh ' + gmsh.model.getFileName() + ' loaded, vertex, triangles :',  self.nnode, self.ntri )
        gmsh.model.setCurrent(old_gmshmodelname)
        
       
       
def areas(txy):
    xy0 = txy[:,0,:]
    xy1 = txy[:,1,:]
    xy2 = txy[:,2,:]
#    area = np.cross(xy1-xy0, xy2-xy0)/2. 
    
    return np.cross(xy1-xy0, xy2-xy0)/2. 

def cog(txy):
    return np.mean(txy,1)

def elementaryGradOperators(txy):    
    exy = np.empty((len(txy),2,2))
    exy[:,0,:] = txy[:,1,:] - txy[:,0,:]
    exy[:,1,:] = txy[:,2,:] - txy[:,0,:]
    #det = np.linalg.det(exy)
    #print(np.min(det), np.max(det))
    G =  np.linalg.inv(exy)@np.array([[-1,1.,0.],[-1.,0.,1.]])
    return G

def eTovOp(xy, t):
    GX = (areas(xy[t])/3.).repeat(3)
    GI = t.flatten()
    GJ = np.arange(len(t)).repeat(3)
    return scipy.sparse.coo_array((GX, (GI , GJ)), (len(xy), len(t)) ).tocsc()
    
def gradOp(xy, t, skip = None, perelemfac = None, elemmap=None, ntmax = None ) :
    nt = len(t)
    nv = len(xy)
    if skip is None :
        GX = elementaryGradOperators(xy[t])
        if perelemfac is not None : GX = perelemfac[:, None, None]  *GX
        GX = GX.flatten()
    else:
        raise
        GX = np.zeros((nt,2,3))
        do = np.array(list(set(range(nt)) - set(skip)))
        GX[do,:,:] = elementaryGradOperators(xy[t[do,:]])
        GX = GX.flatten()
    GI = np.arange(2*nt).repeat(3)
    if elemmap is not None :
        GI = np.vstack((elemmap*2, elemmap*2+1)).flatten('F').repeat(3)
    GJ = t.repeat(2, axis =0).flatten()
    shape = (2*nt, nv)
    if ntmax is not None :
        shape = (2*ntmax, nv)
    return scipy.sparse.coo_array((GX, (GI, GJ)), shape ).tocsc()

def BSymOp(xy, t, lamb, skip = None, elemmap = None, ntmax = None):
    #fac = sqrtweight(xy, t, lamb)   
    perelemfac = np.sqrt(lamb*areas(xy[t]))
    return gradOp(xy, t, skip, perelemfac = perelemfac, elemmap=elemmap, ntmax= ntmax)

def sqrtweight(xy, t, lamb):
    return scipy.sparse.dia_array( (np.sqrt(lamb*areas(xy[t])).repeat(2),0),(2*len(t),2*len(t)))


class ThermicProblem():
    def __init__(self, xy, topo, lamb, diri, source, verbose = False):
        AAt = False
        self.xy = xy
        self.topo = topo
        self.tri2node = self.topo.tri2node
        self.nnode =  len(self.xy)
        self.ntri  =  len(self.tri2node)
                           
        self.lamb = lamb
        self.source = source
        self.verbose = verbose
        self.ddofs, self.deval = diri
        self.fdofs = list(set(range(0, self.nnode)) - set(self.ddofs))
        
        self.T0 =np.zeros(self.nnode)
        self.T0[self.ddofs] = self.deval(self.xy[self.ddofs])
        self.Bd, self.Bf = self.buildB(self.xy, self.tri2node, self.lamb)
        self.BfT = self.Bf.T
        self.Kf = self.BfT@self.Bf
        self.Fsf = self.buildFs(self.xy, self.tri2node)
        self.Rf  = -self.BfT@(self.Bd@self.T0[self.ddofs])
        self.factortime = 0
        if(AAt):
            tic = time.perf_counter()
            self.chol = sksparse.cholmod.cholesky_AAt(self.BfT)
            toc = time.perf_counter()
            if self.verbose : print(f"Factor AAT  in {toc - tic:0.4f} seconds")
        else:
            tic = time.perf_counter()
            self.chol = sksparse.cholmod.cholesky(self.Kf)
            toc = time.perf_counter()
            if self.verbose : print(f"Factor Chol in {toc - tic:0.4f} seconds")
            if self.verbose : print("nnz ", self.Bf.nnz)
        self.T = self.solve()
        
        
    def nodes2triangles(self,ids):
        return self.topo.nodes2triangles(ids)

    def buildB(self, xy, t, lamb, elemmap = None, ntmax = None):
        B  = BSymOp(xy, t, lamb, elemmap = elemmap, ntmax = ntmax)
        Bd = B[:, self.ddofs]
        Bf = B[:, self.fdofs]
        return Bd, Bf
    
    def buildFs(self, xy, t):
        ''' build rhs associated to source term '''
        Fs = eTovOp(xy, t)@self.source(cog(xy[t]))
        return Fs[self.fdofs]
        
    def solve(self):
        tic = time.perf_counter()
        Tred = self.chol(self.Fsf+self.Rf)
        toc = time.perf_counter()
        T = self.T0.copy()
        T[self.fdofs] = Tred
        if self.verbose: print(f" Solved using in {toc - tic:0.4f} seconds")
        return T
            
    def updateNodesAndLambda(self, movednodeid, movednodecoord, changedLambId, newLambVal):
        tic = time.perf_counter()
        xy = self.xy
        mxy = xy.copy()
        mxy[movednodeid] = movednodecoord
        mtid = self.nodes2triangles(movednodeid)
        mtid = np.unique(np.hstack((mtid, changedLambId))).astype(int)
        if self.verbose : print("updated element :", len(mtid))
        mt =   self.tri2node[mtid]
        lamb = self.lamb
        mlamb = lamb.copy()
        mlamb[changedLambId] = newLambVal
        DBd_down,  DBf_down   = self.buildB(xy,  mt,  lamb[mtid], elemmap = mtid, ntmax = self.ntri)  
        DBd_up,    DBf_up     = self.buildB(mxy, mt, mlamb[mtid], elemmap = mtid, ntmax = self.ntri) 
        DBf_downT = DBf_down.T
        DBf_upT = DBf_up.T
        self.Kf +=  DBf_upT@DBf_up - DBf_downT@DBf_down
        toc = time.perf_counter()
        if self.verbose : print(f"Assemble in {toc - tic:0.4f} seconds")
        tic = time.perf_counter()
        #self.chol.update_inplace(DBf_up.T,   subtract=False)
        #self.chol.update_inplace(DBf_down.T, subtract=True)
        #self.chol =  sksparse.cholmod.cholesky(self.Bf.T@self.Bf)
        self.chol.cholesky_inplace(self.Kf) # 15.4
        #self.chol.cholesky_AAt_inplace(self.Bf.T) #16.645011377011542
        toc = time.perf_counter()
        self.factortime += toc-tic
        if self.verbose : print(f"Update Factor in {toc - tic:0.4f} seconds")
        #this seems strange ... verify
        moveddirichletnodeid = list(set(self.ddofs) - set(movednodeid))
        self.T0[moveddirichletnodeid] = self.deval(mxy[moveddirichletnodeid])
        self.Bd += (DBd_up - DBd_down)
        self.Bf += (DBf_up - DBf_down)     
        self.BfT = self.Bf.T
        self.Fsf           +=  self.buildFs(mxy, mt) - self.buildFs(xy, mt)
        self.Rf = -self.BfT@(self.Bd@self.T0[self.ddofs])
        self.T  = self.solve()
        self.xy = mxy
        self.lamb = mlamb
        
        
                #Sherman morisson
        #invK = self.chol.inv()
       # invK = B
#        DBd_down,  DBf_down   = self.buildB(xy,  mt,  lamb[mtid])  
#        DBd_up,    DBf_up     = self.buildB(mxy, mt, mlamb[mtid]) 
#        DB = (DBf_up - DBf_down)
#        tic = time.perf_counter()
#        #cholSM = sksparse.cholmod.cholesky(scipy.sparse.eye(DB.shape[1]) + DB.T@DB) 
#        
#        A = DB[0:2,:]@self.chol(DB[0:2,:].T)
#        #K  =self.Bf.T@self.Bf
#        #Afin = scipy.sparse.find(A.todense())
#        #A = scipy.sparse.coo_matrix((Afin[2], (Afin[0], Afin[1])))
#        print('A', A.shape, A.nnz, 'DB', DB.shape, DB.nnz)#, 'invK', invK.shape, invK.nnz)
#        #cholSM = sksparse.cholmod.cholesky((scipy.sparse.eye(A.shape[0]) + A).todense())
#        cholSM = scipy.linalg.cho_factor((scipy.sparse.eye(A.shape[0]) + A).todense())
#        
#        toc = time.perf_counter()
#        if self.verbose : print(f"cholSM in {toc - tic:0.4f} seconds")
        

    def pyPlotT(self):
        m = meshplotter.simplexMesh(self.xy, self.tri2node)
        return m.plotScalarField(self.T, clabel = False, showmesh = len(self.tri2node)< 1000) #return c, fig, ax
        
    def gmshListPlotT(self, viewname = "T", IntervalsType = 3, NbIso=10):
        return gmshListPlotScalarNodalField(self.xy, self.tri2node, self.T, viewname = viewname, IntervalsType = IntervalsType, NbIso = NbIso)
