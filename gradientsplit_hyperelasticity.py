''' 
    This module compute the partial derivatives of an hyper elastic law
    with regard to F0 and F1 where F = F0F1^{-1}
'''

from __future__ import annotations
from typing import Tuple
import unittest
import numpy as np
import scipy as sp
import hyperelasticity as hy
from execTools import Profile
from smallsize_tensor import FNDArray, inv, cross_likj, cross_iljk, random_lintransform

class GradientSplitHyperElastic():
    """ 
        Compute potential, PKI and dPKIdF assuming a multiplicative spit of F:
        F = F1@F0^{-1}
        Usefull fo searching configurational equilibrium
    """
    # pylint: disable=C0103
    def __init__(self, law: hy.HyperElasticMaterial):
        self.law = law
    def potential(self, F0:FNDArray, F1:FNDArray) -> FNDArray|float:
        ''' return the value of the hyperelastic potential as a function of 
            the split F0, F1'''
        F = F1@inv(F0)
        return self.law.potentialF(F)
    def PKI(self, F0:FNDArray, F1:FNDArray) -> tuple[FNDArray, FNDArray]:
        """ return P0, P1 such as dwdF:DF = dP0:dF0 + dP1:dF1 P:dF = """
        iF0 = inv(F0)
        iFT0 = iF0.swapaxes(-2, -1)
        F = F1@iF0
        P = self.law.PKI(F)
        P1 = P@(iFT0)
        FT = F.swapaxes(-2,-1)
        P0 = -FT@P1
        return P0, P1

    def dPKIdF(self, F0:FNDArray, F1:FNDArray) -> Tuple[FNDArray, FNDArray, FNDArray, FNDArray]:
        """ return tangent operators """
        iF0 = inv(F0)
        F = F1@iF0
        P0, P1 = self.PKI(F0, F1)
        dPdF = self.law.dPKIdF(F)
        # dP1dF1 =  np.einsum("...aI, ...iIjJ, ...bJ -> ...iajb", iF0, dPdF, iF0)
        # dP0dF1 = (-np.einsum("...iI, ...iajb -> ...Iajb", F, dP1dF1)
        #           -np.einsum("...bI, ...ja -> ...Iajb", iF0, P1))
        # tmp    =   np.einsum("...iajb, ...jJ -> ...iaJb", dP1dF1, F)
        # dP1dF0 =  -tmp -np.einsum(" ...ib, ...aI -> ...iaIb", P1, iF0)
        # dP0dF0 = ( np.einsum("...iI, ...iaJb-> ...IaJb",   F, tmp)
        #           -np.einsum("...bI, ...Ja  -> ...IaJb", iF0,  P0)
        #           -np.einsum("...Ib, ...aJ  -> ...IaJb",  P0, iF0 ))
        # return dP0dF0, dP0dF1, dP1dF0, dP1dF1
        dP1dF1 =  np.einsum("...aI, ...iIjJ, ...bJ -> ...iajb", iF0, dPdF, iF0)
        dP0dF1 = -np.einsum("...iI, ...iajb -> ...Iajb", F, dP1dF1) -cross_likj(iF0, P1)
        tmp    =  np.einsum("...iajb, ...jJ -> ...iaJb", dP1dF1, F)
        dP1dF0 = -tmp -cross_iljk(P1, iF0)
        dP0dF0 =  np.einsum("...iI, ...iaJb-> ...IaJb", F, tmp) \
                 -cross_likj(iF0, P0) -cross_iljk(P0, iF0)
        return dP0dF0, dP0dF1, dP1dF0, dP1dF1

def check_derivatives(law: hy.HyperElasticMaterial, F0:FNDArray, F1:FNDArray, verbose = False):
    ''' This function verify if the exact partial derivatives of the elastic energy
        computed view GradientSplitHyperElastic are correct by comparing them to 
        Finite diferences approximations.
    '''
    # pylint: disable=C0103
    splitlaw = GradientSplitHyperElastic(law)
    P0, P1 = splitlaw.PKI(F0,F1)
    dP0dF0, dP0dF1, dP1dF0, dP1dF1 =  splitlaw.dPKIdF(F0, F1)
    def PKInum(F0:FNDArray, F1:FNDArray)-> tuple[FNDArray, FNDArray]:
        def funw(F0:FNDArray, F1:FNDArray)->FNDArray|float:
            F = F1@inv(F0)
            return law.potentialF(F)
        def flat_funw(F0F1:FNDArray)-> FNDArray|float:
            F0 = F0F1[:4].reshape(2,2)
            F1 = F0F1[4:].reshape(2,2)
            return funw(F0,F1)
        F0F1 = np.concatenate((F0.flatten(), F1.flatten()))
        tmp = sp.optimize.approx_fprime(F0F1, flat_funw)
        P0 = tmp[:4].reshape(2,2)
        P1 = tmp[4:].reshape(2,2)
        return P0, P1
    def eval_dP0dF0_num(F0, F1):
        def P0ofF0(F0_flat):
            F0 = F0_flat.reshape(2,2)
            iF0 = inv(F0)
            F = F1@iF0
            P = law.PKI(F)
            P0 = -iF0.T@F1.T@P@iF0.T
            return P0.flatten()
        tmp = sp.optimize.approx_fprime(F0.flatten(), P0ofF0)
        return tmp.reshape(2,2,2,2)

    def eval_dP0dF1_num(F0, F1):
        def P0ofF1(F1_flat):
            F1 = F1_flat.reshape(2,2)
            iF0 = inv(F0)
            F = F1@iF0
            P = law.PKI(F)
            P0 = -iF0.T@F1.T@P@iF0.T
            return P0.flatten()
        tmp = sp.optimize.approx_fprime(F1.flatten(), P0ofF1)
        return tmp.reshape(2,2,2,2)
    def eval_dP1dF0_num(F0, F1):
        def P1ofF0(F0_flat):
            F0 = F0_flat.reshape(2,2)
            iF0 = inv(F0)
            F = F1@iF0
            P = law.PKI(F)
            P1 = P@iF0.T
            return P1.flatten()
        tmp = sp.optimize.approx_fprime(F0.flatten(), P1ofF0)
        return tmp.reshape(2,2,2,2)
    def eval_dP1dF1_num(F0, F1):
        def P1ofF1(F1_flat):
            F1 = F1_flat.reshape(2,2)
            iF0 = inv(F0)
            F = F1@iF0
            P = law.PKI(F)
            P1 = P@iF0.T
            return P1.flatten()
        tmp = sp.optimize.approx_fprime(F1.flatten(), P1ofF1)
        return tmp.reshape(2,2,2,2)

    squeeze=False
    Fshape = F0.shape
    dPdFshape = dP0dF0.shape
    if F0.ndim == 2:
        Fshape = (1,2,2)
        F0.shape = Fshape
        F1.shape = Fshape
        dPdFshape = (1,2,2,2,2)
        squeeze =True

    P0num = np.zeros(Fshape)
    P1num = np.zeros(Fshape)
    dP0dF0_num = np.zeros(dPdFshape)
    dP0dF1_num = np.zeros(dPdFshape)
    dP1dF0_num = np.zeros(dPdFshape)
    dP1dF1_num = np.zeros(dPdFshape)
    for ie, (F0ie, F1ie) in enumerate(zip(F0, F1)):
        P0num[ie], P1num[ie] = PKInum(F0ie, F1ie)
        dP0dF0_num[ie] = eval_dP0dF0_num(F0ie, F1ie)
        dP0dF1_num[ie] = eval_dP0dF1_num(F0ie, F1ie)
        dP1dF0_num[ie] = eval_dP1dF0_num(F0ie, F1ie)
        dP1dF1_num[ie] = eval_dP1dF1_num(F0ie, F1ie)
    if squeeze:
        Pshape = (2,2)
        dPdFshape = (2,2,2,2)
        P0num.shape =  Pshape
        P1num.shape = Pshape
        dP0dF0_num.shape  = dPdFshape
        dP0dF1_num.shape  = dPdFshape
        dP1dF0_num.shape  = dPdFshape
        dP1dF1_num.shape  = dPdFshape

    errP0     = np.linalg.norm(P0-P0num)
    errP1     = np.linalg.norm(P1-P1num)
    errdP0dF0 = np.linalg.norm(dP0dF0-dP0dF0_num)
    errdP0dF1 = np.linalg.norm(dP0dF1-dP0dF1_num)
    errdP1dF0 = np.linalg.norm(dP1dF0-dP1dF0_num)
    errdP1dF1 = np.linalg.norm(dP1dF1-dP1dF1_num)
    if verbose:
        print('check P0')
        print(P0)
        print(P0num)
        print('error |P0 - P0num|:', errP0)
        print('check P1')
        print(P1)
        print(P1num)
        print('error |P1 - P1num|:', errP1)
        print("check dP0dF0")
        print(dP0dF0.reshape((-1,4,4)))
        print(dP0dF0_num.reshape((-1, 4,4)))
        print('error |dP0dF0 - dP0dF0num|:', errdP0dF0)
        print("check dP0dF1")
        print(dP0dF1.reshape((-1,4,4)))
        print(dP0dF1_num.reshape((-1,4,4)))
        print('error |dP0dF1 - dP0dF1num|:', errdP0dF1)
        print("check dP1dF0")
        print(dP1dF0.reshape((-1,4,4)))
        print(dP1dF0_num.reshape((-1,4,4)))
        print('error |dP1dF0 - dP1dF0num|:', errdP1dF0)
        print("check dP1dF1")
        print(dP1dF1.reshape((-1, 4,4)))
        print(dP1dF1_num.reshape((-1, 4,4)))
        print('error |dP1dF1 - dP1dF1num|:', errdP1dF1)

    return {'P0':errP0, 'errP1': errP1,
            'dP0dF0': errdP0dF0, 'dP0dF1': errdP0dF1,
            'dP1dF0': errdP1dF0, 'dP1dF1': errdP1dF1}

class Test(unittest.TestCase):
    # pylint: disable=C0103
    """ testing GradientSplitHyperElastic """

    def test_gradientsplit(self):
        """ - Checking partial derivatives of the energy for gradient split """
        law_test = hy.CompressibleNeoHookeen(E=1., nu = 0.3, hyp = hy.PLANESTRAIN)
        F0_test = np.array([[0.3,- 2.], [0.15,10.]])
        F1_test = np.array([[2.,1.], [-1.,4.]])
        errors = check_derivatives(law_test, F0_test, F1_test)
        for name, err in errors.items():
            if err>= 1.e-4 :
                print('\n Error between exact expression and numerical derivative too too big for'
                      , name,)
            self.assertTrue(err < 1.e-4)
        # this check if it is working prperly on a collection of tensors.
        F0_test = np.array([[[0.3,- 2.], [0.15,10.]], [[1,0.],[0.,1.]]])
        F1_test = np.array([[[2.,1.], [-1.,4.]], [[1,0.],[0.,1.]]])
        errors = check_derivatives(law_test, F0_test, F1_test)
        for name, err in errors.items():
            if err>= 1.e-4 :
                print('\n Error between exact expression and numerical derivative too too big for'
                      , name)
            self.assertTrue(err < 1.e-4)

def benchmark(law:hy.HyperElasticMaterial, n:int):
    """ benchmark : compute and profile GradientSplitHyperElastic : PKI and dPKIDF """
    # pylint: disable=C0103
    dim = hy.HYP2DIM[law.hyp]
    splitlaw = GradientSplitHyperElastic(law)
    gen = random_lintransform
    F0 = gen(n, dim)
    F1 = gen(n, dim)
    print(f'computing PKI0, PKI1 and dPKIDF_IJ for {n:.3e} F, in {law.hyp}')
    Profile.doprofile = True
    Profile.enable()
    __ = splitlaw.PKI(F0, F1)
    __ = splitlaw.dPKIdF(F0,F1)
    Profile.disable()
    Profile.print_stats()

if __name__ == "__main__":
    law3D = hy.CompressibleNeoHookeen(E=1., nu=0.3, hyp = hy.DIM3)
    lawDP = hy.CompressibleNeoHookeen(E=1., nu=0.3, hyp = hy.PLANESTRAIN)
    lawCP = hy.CompressibleNeoHookeen(E=1., nu=0.3, hyp = hy.PLANESTRESS)
    benchmark(law3D, 100000)

# pylint: disable=C0301
#     computing PKI0, PKI1 and dPKIDF_IJ for 1.000e+05 F, in MaterialHypothesis.DIM3
#          218 function calls in 2.496 seconds
#    Ordered by: cumulative time
#    ncalls  tottime  percall  cumtime  percall filename:lineno(function)
#         1    0.100    0.100    2.415    2.415 gradientsplit_hyperelasticity.py:40(dPKIdF)
#        18    0.000    0.000    2.153    0.120 {built-in method numpy.core._multiarray_umath.implement_array_function}
#         9    0.000    0.000    2.153    0.239 <__array_function__ internals>:177(einsum)
#         9    0.000    0.000    2.153    0.239 einsumfunc.py:1009(einsum)
#         9    2.153    0.239    2.153    0.239 {built-in method numpy.core._multiarray_umath.c_einsum}
#         2    0.092    0.046    0.153    0.077 gradientsplit_hyperelasticity.py:29(PKI)
#         1    0.001    0.001    0.097    0.097 hyperelasticity.py:221(dPKIdF)
#         1    0.071    0.071    0.087    0.087 hyperelasticity.py:186(_dPKIdF_exp)
#         5    0.000    0.000    0.080    0.016 smallsize_tensor.py:94(_cross_ijkl)
#         6    0.053    0.009    0.053    0.009 smallsize_tensor.py:36(_cofac)
#         2    0.001    0.000    0.035    0.018 hyperelasticity.py:214(PKI)
#         2    0.000    0.000    0.035    0.017 hyperelasticity.py:164(_PKI_3d)
#         3    0.006    0.002    0.034    0.011 smallsize_tensor.py:78(inv)
#         3    0.005    0.002    0.033    0.011 smallsize_tensor.py:83(det_invt)
#         2    0.000    0.000    0.032    0.016 smallsize_tensor.py:134(cross_likj)
#         2    0.000    0.000    0.032    0.016 smallsize_tensor.py:144(cross_iljk)
#         2    0.010    0.005    0.010    0.005 hyperelasticity.py:171(_PKI_exp)
#         6    0.004    0.001    0.004    0.001 smallsize_tensor.py:52(_det)
