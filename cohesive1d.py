''' Cohesive damage model in 1d, using a discontinuous Galerkin displacement field
    - A lagrange multiplyer at each node to inforce the cohesive law
    - A damage variable d at each node to represente the "softening" of the cohesive law 
'''

from __future__ import annotations
import numpy as np
import numpy.typing as npt
import scipy as sp
import matplotlib.pyplot as plt

FNDArray = npt.NDArray[np.float64]

def maxnorm(x :FNDArray)-> np.float64 :
    ''' return the maximum value of abs(x) ''' 
    return np.linalg.norm(x.flatten(), np.inf)

class Cohesive1d():
    ''' Class to represent the discrete cohesive model
        Allow to compute the energies and all their needed derivative
        knowing the field u, lambda, and d, and the boundary condition ul and ur
        let Nv the number of node of the 1d mesh.
        Ne = Nv-1 the number of edges
        - u is of shape (Ne, 2), and u[i, 0] and u[i, 1] are the value of u at element u 
            respectivelly 
            at it's left and right node. u is supposed linear in x in each element
            (And discontinuous accross nodes)
        - d is of shape (Nv) and represent the damage of each cohesive element (node in 1d)
        - lamb is of shape (Nv) and is a lagrange multiplyer used to enforce the cohesive law
           at each node
        energy as a natural expression :
        phi(u,d) = sum_e 0.5*E*eps_e**2 + sum_i 0.5*k*g(d_i)*jump_i(u) + sum_i ych(d_i)
        where e are the element and i are the nodes
        d_i is the (cohesive) dammage variable at node i 
        eps_e is strain in element e (u_e^r - u_e^l) / (x_e^r - x_e^l)
        u_e^l and u_e^r are  respectively the displacement of the left and right node of element e
        x_e^l and x_e^r are  respectively the position of the left and right node of element e in
          reference configuration
        jump_i(u) is the jump of displacement at node i:
        jump_i(u) = u_i^r - u_i^l
            where u_i^r and u_i^l are respectively the displacement at the right and left of node i
        
        The energy also has a partially dualised expression (psi) to cope with g(0) = np.inf
        (infinite slope of the cohesive law when there is no damage, to recovert elasticity
        solution which should be continuous in space (so no jump))
        psi(u, lamb, d) = um_e 0.5*E*eps_e**2 + sum_i 0.5*k*g(d_i)*jump_i(u) + sum_i ych(d_i)
        
        '''
    def __init__(self, E:float, sigc:float, wc:float, L:float, nv:int):
        ''' Model Constructor
        
        Parameters
        ----------
        E : Young Modulus
        
        sigc : max cohesive strenght
        
        wc : max opening
        
        L : lenght of the bar
        
        nv : number of vertices
        '''
        # pylint: disable=C0103
        self.L = L
        self.nv = nv
        self.x = np.linspace(0.,L, nv)
        self.h = self.x[1:] - self.x[:-1]
        self.E = E
        self.sigc = sigc
        self.wc = wc
        self.k = 2.*sigc/wc
        #self.k = sigc/wc

    def g(self, d: FNDArray) -> FNDArray:
        ''' this is the degradation function '''
        return 1./d -1.

    def dg(self, d: FNDArray) -> FNDArray:
        ''' first derivative of the degradation function '''
        return -1./d**2

    def ych(self, d: FNDArray) -> FNDArray :
        ''' dissipation potential '''
        sigc, wc, k = self.sigc, self.wc, self.k
        return 0.5*d * sigc**2*wc/(d*(sigc - k*wc) + k*wc)

    def dych(self, d :FNDArray)-> FNDArray:
        ''' fist derivative of the dissipation potential '''
        sigc, wc, k = self.sigc, self.wc, self.k
        kwc = wc*k
        delta = sigc - kwc
        den = d*delta + kwc
        num = 0.5*sigc**2*wc
        dych_exa = num*(den - d*delta)/den**2
        # eps = 1.e-6
        # dych_num = (self.ych(d+eps) - self.ych(d))/eps
        # print('error dych', maxnorm(dych_exa -dych_num))
        return dych_exa

    def d2ych(self, d :FNDArray)-> FNDArray:
        ''' second derivative of the dissipation potential '''
        sigc, wc, k = self.sigc, self.wc, self.k
        kwc = wc*k
        delta = sigc - kwc
        den = d*delta + kwc
        num = 0.5*sigc**2*wc
        d2ych_exa = 2*num*delta*(d*delta - den)/den**3
        # eps = 1.e-6
        # d2ych_num = (self.dych(d+eps) - self.dych(d))/eps
        # print('error d2ych', maxnorm(d2ych_exa -d2ych_num))
        return d2ych_exa

    def initfield(self) -> tuple[FNDArray,FNDArray,FNDArray] :
        ''' return an initialized u, d, and lambda of the correct size
        (depending on nv number of vertices)
        '''
        u = np.zeros((self.nv-1, 2))
        lamb = np.zeros(self.nv)
        d = np.zeros(self.nv)
        return u, lamb, d

    def strain(self,  u:FNDArray) -> FNDArray:
        ''' return the strain per element '''
        return (u[:,1] - u[:,0])/self.h

    def dstraindu(self):
        ''' return the derivative of the strain per element by the nodales values of u
            This return depsdu as a a matrix of shape (ne, 2)
            where depdu[i, 0] is the derivative of the strain in element i with regard to the
            displacement ul of the left node of element i
            and depdu[i, 1] is the derivative of the strin in element i with regard to the
            displacement ul of the right node of element i
        '''
        ih = 1/self.h
        return np.hstack([-ih[:, np.newaxis], ih[:, np.newaxis]])

    def jump(self, ul:float, ur:float, u:FNDArray )->FNDArray:
        ''' return the displacement jumps j as an array of size nv 
            j[i] is the jump at node i
        '''
        j = np.zeros(self.nv)
        j[0] = u[0,0] - ul
        j[-1] = ur - u[-1, 1]
        j[1:-1] = u[1:,0] - u[:-1, 1]
        return j

    def djumpdu(self)->FNDArray:
        ''' return the derivative of the jump by the displacement 
            djdu is a matrix of shape (nv, 2*ne)
            where djdu[i, 2*j] is the derivative of the jump at node i by the 
                displacement of the left node of element j
            and djdu[i, 2*j] is the derivative of the jump at node i by the 
                displacement of the right node of element j
        '''
        nv = self.nv
        ne = nv-1
        djdu = np.zeros((nv, 2*ne))
        djdu[0,0] = 1.
        djdu[-1,-1] = -1.
        djdu[1:-1, 1:-1] = sp.linalg.block_diag(*np.array([-1.,1.]*(nv-2)).reshape((nv-2, 1, 2)))
        return djdu

    def phi(self, ul:float, ur:float, u:FNDArray, d:FNDArray)-> np.float64:
        ''' return the energy phi  as a function of u and d 
        '''
        strain = self.strain( u)
        jump = self.jump( ul, ur, u)
        return np.sum(0.5*self.E*strain**2*self.h) \
              +np.sum(self.ych(d)) \
              +np.sum(0.5*jump[d>0.]**2*self.g(d[d>0.])*self.k)

    def psi(self, ul:float, ur:float, u:FNDArray, lamb:FNDArray, d:FNDArray)-> np.float64:
        ''' return the energy psi  as a function of u, lambda and d
            where lambda is a lagrange multiplyer that permits to cope whith g(0) = np.inf
        '''
        strain = self.strain( u)
        jump = self.jump( ul, ur, u)
        # print(strain, jump, self.h, self.ych(d))
        return np.sum(0.5*self.E*strain**2*self.h) \
              +np.sum(lamb*jump  - 0.5*self.k*jump**2 -0.5*lamb**2*d/self.k+ self.ych(d))

    def dphidd(self, ul:float, ur:float, u:FNDArray, d:FNDArray)-> FNDArray:
        ''' derivative of phi(u,d) by d'''
        jump = self.jump( ul, ur, u)
        dedd =  self.dych(d)
        index = d> 0
        dedd[index] += 0.5*jump[index]**2*self.dg(d[index])*self.k
        return dedd

    def phi_elastic_bulk(self, u:FNDArray) -> np.float64:
        ''' return the (elastic) bulk part of the energy (phi or psi) '''
        strain = self.strain( u)
        return np.sum(0.5*self.E*strain**2*self.h)

    def phi_diss(self, d:FNDArray)-> np.float64:
        ''' return the cohesive dissipation part of the energy (phi )'''
        return np.sum(self.ych(d))

    def phi_elastic_cohesive(self, ul:float, ur:float, u:FNDArray, d:FNDArray):
        ''' return the cohesive elastic part of the energy (phi )'''
        jump = self.jump(ul, ur, u)
        return np.sum(0.5*jump[d>0.]**2*self.g(d[d>0.])*self.k)

    def dpsidd(self, ul:float, ur:float, u:FNDArray, lamb:FNDArray, d:FNDArray)-> FNDArray:
        ''' return the derivative of the energy (psi) by d '''
        # pylint: disable=W0613
        return  self.dych(d)-0.5*lamb**2/self.k

    def d2psidd2(self, ul:float, ur:float, u:FNDArray, lamb:FNDArray, d:FNDArray)-> FNDArray:
        ''' return the 2nd derivative of the energy (psi) by d'''
        # pylint: disable=W0613
        return  self.d2ych(d)

    def dpsidu(self, ul:float, ur:float, u:FNDArray, lamb:FNDArray, d:FNDArray)-> FNDArray:
        ''' return the derivative of the energy (psi) by u'''
        # pylint: disable=W0613
        strain = self.strain(u)
        jump = self.jump(ul, ur, u)
        stress_comp = ((self.E*strain*self.h)[:, np.newaxis]*self.dstraindu())
        jump_comp  = ((lamb-self.k*jump)@self.djumpdu()).reshape((-1,2))
        dedu_exa =  stress_comp + jump_comp
        # def fun(X):
        #     u = X.reshape((-1,2))
        #     return self.energy(ul, ur, u, lamb, d)
        #dedu_num = sp.optimize.approx_fprime(u.flatten(), fun).reshape((-1,2))
        #print('error dedu', maxnorm(dedu_exa - dedu_num))
        return dedu_exa

    def dpsidlamb(self, ul:float, ur:float, u:FNDArray, lamb:FNDArray, d:FNDArray)-> FNDArray:
        ''' return the derivative of the energy (psi) by lambda '''
        # pylint: disable=W0613
        jump = self.jump(ul, ur, u)
        dedlamb_exa = jump -lamb*d/self.k
        # dedlamb_num = sp.optimize.approx_fprime(lamb,
        #    lambda lamb : self.energy(ul, ur, u, lamb, d) )
        # print('error dedlamb', maxnorm(dedlamb_exa-dedlamb_num))
        return dedlamb_exa

    def d2psidu2(self, ul:float, ur:float, u:FNDArray, lamb:FNDArray, d:FNDArray)-> FNDArray:
        ''' return the 2nd derivative of the energy (psi) by u '''
        # pylint: disable=W0613
        ih = 1./self.h
        d2edu2_comp1 = self.E*ih[:, np.newaxis, np.newaxis]*np.array([[1.,-1.],[-1, 1]])
        djdu = self.djumpdu()
        d2edu2_exa = sp.linalg.block_diag(*d2edu2_comp1)-self.k*djdu.T@djdu
        # def fun(X):
        #     u = X.reshape((-1,2))
        #     return self.dedu(ul, ur, u, lamb, d).flatten()
        # d2edu2_num = sp.optimize.approx_fprime(u.flatten(), fun)
        # print('error d2edu2', maxnorm(d2edu2_exa-d2edu2_num))
        return d2edu2_exa

    def d2psidlamb2(self, ul:float, ur:float, u:FNDArray, lamb:FNDArray, d:FNDArray)-> FNDArray:
        ''' return the 2nd derivative of the energy (psi) by lamb '''
        # pylint: disable=W0613
        d2psidlamb2_exa = np.diag(-d/self.k)
        # def fun(lamb):
        #     return self.dedlamb(ul, ur, u, lamb, d).flatten()
        # d2psidlamb2_num = sp.optimize.approx_fprime(lamb, fun)
        # print('error d2psidlamb2', maxnorm(d2psidlamb2_exa-d2psidlamb2_num))
        return d2psidlamb2_exa

    def d2psidlambdu(self, ul:float, ur:float, u:FNDArray, lamb:FNDArray, d:FNDArray)-> FNDArray:
        ''' return the 2nd derivative of the energy (psi) by lamb and u '''
        # pylint: disable=W0613
        d2psidlambdu_exa = self.djumpdu()
        # def fun(x):
        #     u = x.reshape((-1,2))
        #     return self.dpsidlamb(ul, ur, u, lamb, d).flatten()
        # d2psidlambdu_num = sp.optimize.approx_fprime(u.flatten(), fun)
        # print('error d2psidlambdu', maxnorm(d2psidlambdu_exa-d2psidlambdu_num))
        return d2psidlambdu_exa

def solve_ulamb( model:Cohesive1d, ul, ur, d:FNDArray, uguess = None, lambguess = None):
    ''' solve equilibrium  with Dirichlet BC ul and ur given and fixed value of d
    Algo : stationary point of psi for fixed d -> linear system of equation (non-positif definite)
    Note : It's a linear system in u lambda because the energy is quadratic in u, lambda
    No initial guess is really needed here since we have a linear problem. I keep the guess input
    any way ta facilitate possible futur extension to NL problems 
    '''
    nv = model.nv
    ne = nv -1
    if uguess is None :
        uv = np.linspace(ul, ur, nv)
        uguess = np.hstack([uv[:-1, np.newaxis],uv[1:,np.newaxis]])
    if lambguess is None:
        lambguess = model.E*(ul-ur)/model.L *np.ones(nv)
    def unpack(x):
        u = x[:2*ne].reshape((-1,2))
        lamb = x[2*ne:]
        return u, lamb
    def pack(u, lamb):
        return np.hstack([u.flatten(), lamb])
    def fun(x:FNDArray ) -> FNDArray:
        u, lamb = unpack(x)
        dedu = model.dpsidu(ul, ur, u, lamb, d)
        dedlamb = model.dpsidlamb(ul, ur, u, lamb, d)
        return pack(dedu, dedlamb)
    def jac(x:FNDArray) -> FNDArray:
        u, lamb = unpack(x)
        juu = model.d2psidu2(ul, ur, u, lamb, d)
        jlu = model.d2psidlambdu(ul, ur, u, lamb, d)
        jll = model.d2psidlamb2(ul, ur, u, lamb, d)
        jac_exa = np.block([[juu, jlu.T],[jlu, jll]])
        # jac_num = sp.optimize.approx_fprime(X, fun )
        # print('error jac', maxnorm(jac_exa-jac_num))
        return jac_exa
    x0 = pack(uguess, lambguess)
    try :
        x = x0- np.linalg.solve(jac(x0), fun(x0))
    except Exception as err:
        print('failure while calling linear solver in solve_ulamb')
        raise err
    return unpack(x)

def solve_d0( model:Cohesive1d, ul, ur, u:FNDArray, lamb:FNDArray, dn:FNDArray, dguess = None):
    ''' Trying to solve for d, knowing u and lambda, by minimizing e(u,l,d)
        Bad idea !!
        indeed, e(u,l,d) is not convex in u,l and an alterned solver between u,l and d might fail !
    '''
    def fun(d):
        return model.psi(ul, ur, u, lamb, d)
    def jac(d):
        return model.dpsidd(ul, ur, u, lamb, d)
    # def hess(d):
    #     return model.d2edd2(ul, ur, u, lamb, d)
    bounds = sp.optimize.Bounds(dn, 1.)
    d = dn.copy() if dguess is None else dguess
    res= sp.optimize.minimize(fun, x0=d, jac=jac, bounds = bounds)
    if not res.success:
        raise ValueError('minimisation failure'+res.message)
    return res.x

def solve_d( model:Cohesive1d, ul:float, ur:float, u:FNDArray, dn:FNDArray,
             dguess:FNDArray|None = None):
    ''' Trying to solve for d, knowing u by minimizing e(u, d)
        it's ok, since at u fixed, derivative of e(u,d) can be computed even if g and primeg
        are inf for d = 0.
        e(u, d) is indeed convex in u and d (separetly) and now the alterned solver really work.
    '''
    def fun(d):
        return model.phi(ul, ur, u, d)
    def jac(d):
        return model.dphidd(ul, ur, u, d)
    # def hess(d):
    #     return model.d2edd2(ul, ur, u, lamb, d)
    bounds = sp.optimize.Bounds(dn, 1.)
    d = dn.copy() if dguess is None else dguess
    res= sp.optimize.minimize(fun, x0=d, jac=jac, bounds = bounds)
    if not res.success:
        raise ValueError('minimisation failure'+res.message)
    return res.x

def solve_alt_ulamb_d(model:Cohesive1d, ul:float, ur:float, dn:FNDArray,
                      dguess:FNDArray|None = None, itmax:int = 1000, tol:float=1.e-6) \
                      -> tuple[FNDArray, FNDArray, FNDArray, int]:
    ''' Solve the model (find u, lamb and d field) for imposed bc ul, and ur and minimum value of
        d imposed by dn
    
        This version solve by minimising e(u, lamb, d) over  dn <d < 1
        using an alterned scheme :
        solve u, lambd  = arg min_u, max_lamb e(u, lamb, d) for fixed d
        the solve d = argmin_ dn<=d <=1. e(u, d) for fixed u 
        until max(d^{i+1}-d^{n}) < eps or it > itmax
        if succeed, return u, lamb, d and nbit (nbit is the number of iteration)
        else raise ValueError 
    '''
    it = 0
    if dguess is None :
        dguess = dn
    d = dguess.copy()
    while True:
        u, lamb = solve_ulamb(model, ul, ur, d)
        #d1 = solve_d0(model, ul, ur, u, lamb, dn)
        d1 = solve_d(model, ul, ur, u, dn)
        it += 1
        if maxnorm(d-d1) < tol or it > itmax:
            d = d1.copy()
            break
        d = d1.copy()
        # print('d', d)
    if it > itmax :
        raise ValueError(f'solve_alt_ulamb_d did not converge after {it:d} iterations')
    # print(f'Success after {it:d} iterations')
    return u, lamb, d, it

def solve_ulambd( model:Cohesive1d, ul:float, ur:float, dn:FNDArray, dguess = None)\
    -> tuple[FNDArray, FNDArray, FNDArray, int]:
    ''' Solve the model (find u, lamb and d field) for imposed bc ul, and ur and minimum value of
        d imposed by dn
    
        This version solve by minimising e(u(d), lamb(d), d) over  dn <d < 1
        where u(d) and lamb(d) is the solution for a fixed value d
        
        if succeed, return u, lamb, d and nbit (nbit is the number of iteration)
        else raise ValueError 
    '''
    def fun(d):
        u, lamb = solve_ulamb(model, ul, ur, d)
        return model.psi(ul, ur, u, lamb, d)
    def jac(d):
        u, lamb = solve_ulamb(model, ul, ur, d)
        jac_exa = model.dpsidd(ul, ur, u, lamb, d)
        return jac_exa
    # def hess(d):
    #     return model.d2edd2(ul, ur, u, lamb, d)
    bounds = sp.optimize.Bounds(dn, 1.)
    d = dn.copy() if dguess is None else dguess
    res= sp.optimize.minimize(fun, x0=d, jac= jac, bounds = bounds)
    if not res.success:
        print('minimisation failure', res.message)
        raise ValueError('solve_ulambd failed : sp.optimize.minimize failed ' + res.message)
    d = res.x
    u, lamb = solve_ulamb(model, ul, ur, d)
    return u, lamb, d, res.nit

class Plotter():
    ''' Class to plot the result of cohesive model 
        The class set up 4 figures :
        - graph stress(ur) (the global response in stress to an imposed displacement ur)
        - graph stress(w)  (the cohesive law)
        - a figure contening the graph of the different energy as a function of ur
        - The displacement as a function of x for each value of ur
    '''
    # pylint: disable=C0103
    def __init__(self, model:Cohesive1d, urmax:float, delay:float = 0.5) -> None:
        ''' Constructor
        
        Parameters
        ----------
        model : a Cohesive1d object
        
        urmax : a float, used to comput the axis limits. Corresponding to the maximum value
            of the imposed displacement
        
        delay : a float defining the time each figure stay in place
        '''
        self.model = model
        self.delay = delay
        #Construction 4 figs
        self.fig_sur, self.ax_sur = plt.subplots(1, 1, figsize=(6, 4))
        self.fig_sw, self.ax_sw = plt.subplots(1, 1, figsize=(6, 4))
        self.fig_eng, self.ax_eng = plt.subplots(1, 1, figsize=(6, 4))
        self.fig_ux, self.ax_ux = plt.subplots(1, 1, figsize=(6, 4))
        #Set up for plot sigma(ur)
        ax = self.ax_sur
        ax.set_xlabel('ur')
        ax.set_ylabel('stress')
        ax.set_ylim(-.1, 1.1*model.sigc)
        ax.set_xlim(-.1, 1.1*urmax)
        self.line_sur = ax.plot([], [], lw=2, linestyle = '-',  marker = 'o')[0]
        self.fig_sur.canvas.draw_idle()
        #Set up for plot sigma(w)
        ax = self.ax_sw
        ax.set_xlabel('w')
        ax.set_ylabel('stress')
        ax.set_ylim(-.1, 1.1*model.sigc)
        ax.set_xlim(-.1, 1.1*model.wc)
        self.line_sw = ax.plot([], [], lw=2, linestyle = '-',  marker = 'o')[0]
        self.fig_sw.canvas.draw_idle()
        #Setup for ploting energies(ur)
        ax = self.ax_eng
        ax.set_xlabel('ur')
        ax.set_ylabel('energy')
        Gc= model.sigc*model.wc/2.
        ax.set_ylim(-.1, 1.1*Gc)
        ax.set_xlim(-.1, 1.1*urmax)
        ax.set_title('energy balance')
        self.line_ein = ax.plot([], [], lw=2, linestyle = '-',  marker = 'o', label='Total')[0]
        self.line_eex = ax.plot([], [], lw=2, linestyle = '-',  marker = '+', label='Exterieur')[0]
        self.line_ein_bulk_el = ax.plot([], [],
                                        lw=2, linestyle = '-',  marker = 'x', label='Bulk_el')[0]
        self.line_ein_cohe_el = ax.plot([], [],
                                        lw=2, linestyle = '--',  marker = 'x', label = 'Coh_el')[0]
        self.line_ein_cohe_di = ax.plot([], [],
                                        lw=2, linestyle = '-',  marker = '.', label = 'Coh_dis')[0]
        ax.legend()
        self.fig_eng.canvas.draw_idle()
        #Setup for plotting the displacment field
        ax = self.ax_ux
        ax.set_xlabel('x')
        ax.set_ylabel('u')
        ax.set_xlim(0, model.L)
        ax.set_ylim(-.1, 1.1*urmax)
        # creating the 'Discontinuous Galerkin Mesh' for ploting the (discontinuous at vertex)
        # u field
        x = model.x
        xu = np.hstack([x[:-1, np.newaxis], x[1:,np.newaxis]]).flatten()
        u = np.zeros_like(xu)
        self.line_ux = ax.plot(xu, u, lw=2, linestyle = '-',  marker = 'o', label='Total')[0]
        self.fig_ux.canvas.draw_idle()
        #setup up "memmory" for plots in time
        self.ur_mem = []
        self.s_mem = []
        self.w_mem = []
        self.ein_mem = []
        self.eex_mem = []
        self.ein_bulk_el_mem = []
        self.ein_cohe_di_mem = []
        self.ein_cohe_el_mem = []

    def update(self, ul:float, ur:float, u:FNDArray, lamb:FNDArray, d:FNDArray):
        ''' update the 4 figures
        Parameters
        ----------
        
        ul, ur, u, lamb, d: current state of the model (see Cohesive1d)
            used to compute the plots
        '''
        model = self.model
        # L  = model.L
        # sc = model.sigc
        # wc = model.wc
        strain = model.strain(u)
        s      = np.max(model.E*strain)
        wmax  = np.max(model.jump(ul, ur, u))
        ein = model.psi(ul, ur, u, lamb, d)
        ein_bulk_el =   model.phi_elastic_bulk(u)
        ein_cohe_el =   model.phi_elastic_cohesive(ul, ur, u, d)
        ein_cohe_di =   model.phi_diss(d)

        if len(self.eex_mem) == 0:
            eex = 0.
        else:
            eex = self.eex_mem[-1] + (ur-self.ur_mem[-1])*(s+self.s_mem[-1])/2.
        self.ur_mem.append(ur)
        self.w_mem.append(wmax)
        self.s_mem.append(s)
        self.ein_mem.append(ein)
        self.eex_mem.append(eex)
        self.ein_bulk_el_mem.append(ein_bulk_el)
        self.ein_cohe_di_mem.append(ein_cohe_di)
        self.ein_cohe_el_mem.append(ein_cohe_el)

        self.line_sur.set_data(self.ur_mem, self.s_mem )
        self.line_sw.set_data(self.w_mem, self.s_mem)
        self.line_ein.set_data(self.ur_mem, self.ein_mem)
        self.line_eex.set_data(self.ur_mem, self.eex_mem)
        self.line_ein_bulk_el.set_data(self.ur_mem, self.ein_bulk_el_mem)
        self.line_ein_cohe_el.set_data(self.ur_mem, self.ein_cohe_el_mem)
        self.line_ein_cohe_di.set_data(self.ur_mem, self.ein_cohe_di_mem)
        self.line_ux.set_ydata(u.flatten())
        for fig in [self.fig_sur, self.fig_sw, self.fig_eng]:
            fig.canvas.draw_idle()
        plt.pause(self.delay)

def solve_steps(model:Cohesive1d, steps, solver):
    ''' Solving a Cohesive model, using solver
        
    solver = solve_alt_ulamb_d  or solve_ulambd
    for ul = 0.
    ur as imposed by the content of step
    '''
    u, lamb, d =  model.initfield()
    nv = model.nv
    d[nv//2] = 0.001
    ul = 0.
    dn = d.copy()
    urmax = np.max(steps)
    plotter = Plotter(model, urmax, 0.001)
    print(f"#{'ur,':>9}{'stress,':>11}{'wmax,':>11}{'energy,':>11}{'maxd,':>11}{'nbit :':>6} ")
    for ur in steps :
        u, lamb, d, nbit = solver(model, ul, ur, dn)
        dn = d.copy()
        stress = np.max(model.E*model.strain( u))
        energy = model.psi(ul, ur, u, lamb, d)
        wmax = np.max(model.jump(ul, ur, u))
        print(f'{ur:.3e}, {stress:.3e}, {wmax:.3e}, {energy:.3e}, {np.max(d):.3e}, {nbit:5d}')
        plotter.update(ul, ur, u, lamb, d)
    input()

def example1(solver):
    ''' A simple example of solving the problem '''
    # pylint: disable=C0103
    E = 1
    sigc = 1.
    wc = 10.
    L = 1.
    nv = 3
    model = Cohesive1d(E=E, sigc=sigc, wc=wc, L=L, nv = nv)
    nstep_el, nstep_dis1, nstep_dis2 = 5, 10,20
    urmax = 1.5*wc
    uelmax = sigc*L/E
    elsteps = list(np.linspace(0., uelmax*0.99, nstep_el))
    disstep1 = list(np.linspace(uelmax, (uelmax + wc)/2., nstep_dis1))
    disstep2 = list(np.linspace((uelmax + wc)/2., 0., nstep_el))
    disstep3 = list(np.linspace(0., (uelmax + wc)/2., nstep_el))
    disstep4 = list(np.linspace((uelmax + wc)/2., urmax, nstep_dis2))
    steps = np.array(elsteps+disstep1+disstep2+disstep3+disstep4)
    solve_steps(model, steps, solver)

if __name__== "__main__":
    # solve example 1 using the alterned solver
    # example1(solve_alt_ulamb_d)
    # # Solve Example 2 using a more "direct" solver
    example1(solve_ulambd)
