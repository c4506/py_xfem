""" A simple test case for Hyper Elasticity """

from __future__ import annotations
import sys
import getopt
import numpy as np
import gmsh
import oneLevelSimplexMesh as sm
from execTools import Profile
import gmsh_mesh_generator as mg
import hyperelasticity_solver as hs

OUTDIR:None|str = None
IGMSH = True
CASE = "HOLE"

argv = sys.argv
def print_help():
    ''' Print the help string'''
    print('usage : ', argv[0], ' -c casename -b -o dirname/')
    print('Compute an hyperelastic problem')
    print('options :')
    print('-h [--help]: this help')
    print('-c casename [--case <casename>]: case to treat. Curently defined :')
    print( "L and HOLE, default HOLE")
    print('-o <dir> [--output-dir=<dir>] : output directory for all the figure.')
    print("the directory must exist (default post/) if none given, the results won''t be saved")
    print("-b [--nogmsh_popup]: (batch) don''t open gmsh for post pro at the end \n \
            (don''t launch gmsh.fltk.run()) ")
try:
    opts, args = getopt.getopt(argv[1:], "hbo:c:",
                                ["help", "nogmsh-popup", "output-dir", "case"])
except getopt.GetoptError:
    print('Input Error')
    print_help()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print_help()
        sys.exit()
    elif opt in ("-b", "--nogmsh-popup"):
        IGMSH = False
    elif opt in ("-o", "--output-dir"):
        OUTDIR = arg
    elif opt in ("-c, --case"):
        CASE = arg

Profile.doprofile = True
gmsh.initialize()
gmsh.option.setNumber('General.Terminal', 0)

if CASE == "L":
    elem_size = 0.05 # pylint: disable=C0103
    model_name, physical_dict = mg.l_shape(relh=elem_size)
    mesh = sm.gmshModel2sMesh(model_name, ["dOmega_left", "dOmega_right"])
    diriBC = {'dOmega_left': {'x':0., 'y':0.}, 'dOmega_right': {'y':-4.,'x':4.} }
    loadstep = np.linspace(0., 1., int(80*elem_size/0.1))
elif CASE == "HOLE":
    elem_size = 0.05 # pylint: disable=C0103
   
    model_name, physical_dict = mg.hole_in_square(relh=elem_size)
   
    mesh = sm.gmshModel2sMesh(model_name, ["left", "right"])
    diriBC = {'left': {'x':0., 'y':0.}, 'right': {'y':0.,'x':4.} }
    loadstep = np.linspace(0., 1., int(40*0.1/elem_size))
else:
    print("CASE ",CASE, " not defined. stop")
    sys.exit(2)

gmsh.finalize()
print(f"nv : {mesh.getNbVertices():d}, nf : {mesh.getNbTris():d}")
Profile.enable()
hs.solve(mesh, diriBC, loadstep, savedir = OUTDIR, popup_gmsh = IGMSH )
Profile.disable()
#hs.solve(mesh, diriBC, loadstep, savedir = None)
Profile.print_stats()
