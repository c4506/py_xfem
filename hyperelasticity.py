#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module is dedicated to hyperelasticity
Created on Thu April 2024 14:09:29 2024
@author: nchevaug
"""
from __future__ import annotations
import time
from typing import Callable, Tuple
from enum import Enum
import unittest
import numpy as np
from scipy.special import lambertw
from smallsize_tensor import FNDArray, det_invt, cross_ijkl, df_num,\
                            random_lintransform, random_lintransform2dto3d,\
                            metric, det, trace,  trace2, inv, invt, dinvt, check_relerr
from execTools import Profile

class MaterialHypothesis(Enum):
    """ Enum class to define the avalaible Material hypothesis"""
    PLANESTRESS = 1
    PLANESTRAIN = 2
    DIM3 = 3

PLANESTRESS = MaterialHypothesis.PLANESTRESS
PLANESTRAIN = MaterialHypothesis.PLANESTRAIN
DIM3 = MaterialHypothesis.DIM3
HYP2DIM = {PLANESTRESS:2, PLANESTRAIN:2, DIM3:3}

def vecroots(f:Callable, fprime:Callable, x0:np.ndarray|float,
             atol:float=1.e-20, rtol=1.e-12, maxiter:int = 100, verbose = False)->np.ndarray|float:
    """ compute roots of an array of independant equations using newton's
    method with line search so that all x stay > 0"""
    r = f(x0)
    if verbose:
        print(0, x0, np.abs(r), 1.)
    err0 = np.max(np.abs(r))
    err = err0
    if err0 < atol:
        return x0
    it = 0
    while it < maxiter:
        alpha=1.
        j = fprime(x0)
        s = -r/j
        x = x0 + s
        while np.any(x<=0.):
            alpha = 0.5*alpha
            x = x0 + alpha*s
        r = f(x)
        while np.max(np.abs(r)) > err:
            alpha = 0.5*alpha
            x = x0 + alpha*s
            r = f(x)
        err = np.max(np.abs(r))
        if err < atol or err < rtol*err0 :
            return x
        x0 = x
        it += 1
        if verbose:
            print(it, x0, np.abs(r), alpha)
    msg = f"Failed to converge after {maxiter:d} iterations"
    raise RuntimeError(msg)

class HyperElasticMaterial():
    """ Virtual base class for HyperElastic material """
    # pylint: disable=C0103
    # pylint: disable=W2301
    def __init__(self, hyp:MaterialHypothesis=DIM3):
        self.hyp = hyp
    def potentialF(self, F:FNDArray )-> FNDArray|float:
        """ Compute the Elastic Potential as a Function of F"""
        raise NotImplementedError()
    def potentialC(self, C:FNDArray )-> FNDArray|float:
        """ Compute the Elastic Potential as a Function of C"""
        raise NotImplementedError()
    def PKI(self, F:FNDArray)->FNDArray:
        """ Compute Piola Kirchhoff I Stress tensor as a function of F """
        raise NotImplementedError()
    def PKII(self, C:FNDArray)->FNDArray:
        """ Compute Piola Kirchhoff I Stress tensor as a function of C = F^TF """
        raise NotImplementedError()
    def dPKIdF(self, F:FNDArray)->FNDArray:
        """ Compute the deriviatives of the Piola Kirchhoff I Stress tensor
            with regard to F
        """
        raise NotImplementedError()
    def dPKIIdC(self, C:FNDArray)->FNDArray:
        """ Compute the deriviatives of the Piola Kirchhoff II Stress tensor
            with regard to C
        """
        raise NotImplementedError()

# class SimpleLaw0(HyperElasticMaterial):
#     """ A very simple hyper elastic law. Mostly to check derivatives of I_C """
#     # pylint: disable=C0103
#     def __init__(self):
#         super().__init__(PLANESTRAIN)
#     def potential(self, F:FNDArray)-> FNDArray|float:
#         m = metric(F)
#         I_C = 1.+trace(m)
#         return 0.5*I_C
#     def PKI(self, F:FNDArray)->FNDArray:
#         return F
#     def dPKIdF(self, F:FNDArray)->FNDArray:
#         tshape = F.shape[-2:]
#         nbfirstaxis = F.ndim-2
#         I40 = np.eye(tshape[0]*tshape[1]).reshape(tshape+tshape)
#         return I40[(np.newaxis,)*nbfirstaxis]

# class SimpleLaw1(HyperElasticMaterial):
#     """ A very simple hyper elastic law. Mostly to check derivatives of log(J) """
#     # pylint: disable=C0103
#     def __init__(self):
#         super().__init__(PLANESTRAIN)
#     def potential(self, F:FNDArray)-> FNDArray|float:
#         return 0.5*np.log(det(metric(F)))

#     def PKI(self, F:FNDArray)->FNDArray:
#         return invt(F)

#     def dPKIdF(self, F:FNDArray)->FNDArray:
#         return dinvt(F)

# class SimpleLaw2(HyperElasticMaterial):
#     """ A very simple hyper elastic law. Mostly to check derivatives of J """
#     # pylint: disable=C0103
#     def __init__(self):
#         super().__init__(PLANESTRAIN)
#     def potential(self, F:FNDArray)-> FNDArray|float:
#         return np.sqrt(det(metric(F)))

#     def PKI(self, F:FNDArray)->FNDArray:
#         J, invFT = det_invt(F)
#         return np.atleast_1d(J)[..., np.newaxis, np.newaxis] * invFT

#     def dPKIdF(self, F:FNDArray)->FNDArray:
#         J, invFT = det_invt(F)
#         J_11 = np.atleast_1d(J)[..., np.newaxis, np.newaxis]
#         PKI = J_11 * invFT
#         J_1111 = J_11[..., np.newaxis, np.newaxis]
#         return cross_ijkl(PKI, invFT) + J_1111*dinvt(F)

class CompressibleNeoHookeen(HyperElasticMaterial):
    """ Implementation of the NeoHookeen Hyperelastic material """
    # pylint: disable=C0103
    def __init__(self,*, # pylint: disable=R0913
                 E:float|None=None,
                 nu:float|None=None,
                 lamb:float|None=None,
                 mu:float|None=None,
                 hyp:MaterialHypothesis=DIM3):
        super().__init__(hyp)
        if isinstance(E, float) and isinstance(nu, float):
            self.E = E
            self.nu = nu
            self.lamb = E*nu/(1.+nu)/(1.-2*nu)
            self.mu = E/2./(1+nu)
        elif isinstance(lamb, float) and isinstance(mu, float):
            self.lamb = lamb
            self.mu = mu
            self.E = mu*(3.0*lamb + 2.0*mu)/(lamb + mu)
            self.nu = 0.5*lamb/(lamb + mu)
        else:
            raise ValueError("E and nu or lambda and mu must be set (exclusive)")

    def _checktensorsize(self, F:FNDArray):
        if F.shape[-2:] == (3,3) and self.hyp == DIM3:
            return
        if F.shape[-2:] in [(2,2), (3,2)]:
            if self.hyp in [PLANESTRAIN, PLANESTRESS]:
                return
        raise ValueError("invalid shape for F. F.shape must be (...2,2) or (...,3,3)or (..., 3,2)")

    def l3_planestress(self, J2d:FNDArray|float)->FNDArray|float:
        """ return the principal elongation lambda_3 in the direction orthogonal to the plane """
        lamb, mu = (self.lamb, self.mu)
        a = mu
        b = lamb/2
        c = lamb*np.log(J2d) -mu
        return np.sqrt(b*np.real(lambertw( a* np.exp(-c/b)/b))/a)
        # def resl3(l3):
        #     return lamb*np.log(J2d*l3) + mu*(l3*l3 - 1)
        # def jacl3(l3):
        #     return lamb/l3 + 2*mu*l3
        # if isinstance(J2d, float) or isinstance(J2d, int):
        #     x0 = 1.
        # else:
        #     x0 = np.ones(J2d.shape)
        # l3 = vecroots(resl3, jacl3, x0, atol=1.e-20, rtol=1.e-12, maxiter=100)
        #return l3

    # def _dl3dF2d_planestress(self, l3, invFT2d):
    #     lamb = self.lamb
    #     mu   = self.mu
    #     return -l3/(1+2*mu*l3**2/lamb)*invFT2d

    # def dl3F2d_planestress(self, F2d):
    #     """ Compute the derivative of the third principal strain as a fonction of
    #         the plane part of F (Assuming Plan Stress)"""
    #     J2d, invFT2d = det_invt(F2d)
    #     l3 = self.l3_planestress(J2d)
    #     return self._dl3dF2d_planestress(l3, invFT2d)

    def _potential(self, J:FNDArray|float, I_C:FNDArray|float)-> FNDArray|float:
        logJ = np.log(J)
        return self.mu*(0.5*(I_C-3)- logJ) +  self.lamb*logJ**2/2

    def _potential_3d(self, F:FNDArray)-> FNDArray|float:
        J = det(F)
        I_C = trace2(F)
        return self._potential(J, I_C)

    def _potential_planestrain(self, F2d:FNDArray)-> FNDArray|float:
        if F2d.shape[-2:] == (3,2):
            m = metric(F2d)
            J = np.sqrt(det(m))
            I_C = 1.+trace(m)
        elif F2d.shape[-2:] == (2,2):
            J = det(F2d)
            I_C = trace2(F2d) +1
        else :
            raise ValueError('_potential_planestrain, F2d must be of shape (...,3,2) or (...,2,2)')
        return self._potential(J, I_C)

    def _potential_planestress(self, F2d:FNDArray)-> FNDArray|float:
        if F2d.shape[-2:] == (3,2):
            m = metric(F2d)
            J2d = np.sqrt(det(m))
            I_C2d = trace(m)
        elif F2d.shape[-2:] == (2,2):
            J2d = det(F2d)
            I_C2d = trace2(F2d)
        else :
            raise ValueError('_potential_planestress, F2d must be of shape (...,3,2) or (...,2,2)')
        l3 = self.l3_planestress(J2d)
        J = l3*J2d
        I_C = l3**2+I_C2d
        return self._potential(J, I_C)

    def _PKI_3d(self, F:FNDArray)->FNDArray:
        '''Compute Piola Kirchhoff I Stress tensor as a function of F: 3D case (and Plane Strain)'''
        if F.shape[-2:] not in [(3,3), (3,2), (2,2)]:
            raise ValueError("F must be a nd.array of shape[-2:] in [(3,3), (3,2), (2,2)")
        J, invFT = det_invt(F)
        return self._PKI_exp(J, F, invFT)

    def _PKI_exp(self, J, F, invFT)->FNDArray:
        ''' Compute Piola Kirchhoff I Stress tensor as a function of J, F, invFT'''
        return self.mu*(F-invFT) + self.lamb*np.log(J)[..., np.newaxis, np.newaxis]*invFT

    def _PKI_planestress(self, F2d:FNDArray)->FNDArray:
        ''' Compute Piola-Kirchhoff I Stress tensor as a function of F : plane stress case)'''
        if F2d.shape[-2:] not in[(2,2), (3,2)]:
            raise ValueError("F must be a nd.array of shape (...,3,3)")
        J2d, invFT2d = det_invt(F2d)
        if np.any(J2d<=0.):
            raise ValueError("det(F2d) must be > 0.")
        l3 = self.l3_planestress(J2d)
        J = l3*J2d
        return self._PKI_exp(J, F2d, invFT2d)

    def _dPKIdF_exp(self, c0, c1, c2, invFT)->FNDArray:
        c0 = np.atleast_1d(c0)[(...,) + (np.newaxis,)*4]
        c1 = np.atleast_1d(c1)[(...,) + (np.newaxis,)*4]
        c2 = np.atleast_1d(c2)[(...,) + (np.newaxis,)*4]
        tshape = invFT.shape[-2:]
        I4_ = np.eye(tshape[0]*tshape[1]).reshape(tshape+tshape)
        I4_.shape = (1,)*(invFT.ndim-2)+I4_.shape
        c0_I4 = c0*I4_
        invFTxinvFT = cross_ijkl(invFT, invFT) # iFT x d(log(J)) = iFTxiFT
        c2_invFTxinvFT = c2*invFTxinvFT
        c1_dinvFTdF  = -c1*(invFTxinvFT.transpose(list(range(invFTxinvFT.ndim-4)) + [-4,-1,-2,-3]))
        #c1_dinvFTdF  = c1*dinvt(invFT, invFT)
        return c0_I4 + c1_dinvFTdF +c2_invFTxinvFT

    def _dPKIdF_exp_v2(self, F)->FNDArray:
        mu, lamb = self.mu, self.lamb
        tshape = F.shape[-2:]
        I4_ = np.eye(tshape[0]*tshape[1]).reshape(tshape+tshape)
        I4_.shape = (1,)*(F.ndim-2)+I4_.shape
        J, iFT = det_invt(F)
        J_1111 = np.atleast_1d(J)[(...,) + (np.newaxis,)*4]
        return mu*I4_ + lamb*cross_ijkl(iFT, iFT) + (lamb*np.log(J_1111)-mu)*dinvt(F)

    def potentialF(self, F:FNDArray)-> FNDArray|float:
        """ return the Hyperelastic potential as a function of F"""
        self._checktensorsize(F)
        if self.hyp == DIM3:
            return self._potential_3d(F)
        if self.hyp == PLANESTRAIN:
            return self._potential_planestrain(F)
        if self.hyp == PLANESTRESS:
            return self._potential_planestress(F)
        raise ValueError("MaterialHypothesis" + str(self.hyp) +"Not implemented")

    def PKI(self, F:FNDArray)->FNDArray:
        ''' Compute Piola Kirchhoff I Stress tensor as a function of F'''
        if self.hyp in [PLANESTRAIN, DIM3]:
            return self._PKI_3d(F)
        return self._PKI_planestress(F)

    def dPKIdF(self, F:FNDArray)->FNDArray:
        ''' Compute the deriviatives of the Piola Kirchhoff I Stress tensor
            with regard to F at
        '''
        self._checktensorsize(F)
        J, invFT = det_invt(F)
        mu =self.mu
        lamb = self.lamb
        c0 = mu
        if self.hyp in [DIM3, PLANESTRAIN]:
            c1 = lamb*np.log(J)-mu
            c2 = lamb
        elif self.hyp == PLANESTRESS :
            J2d = J
            l3 =  self.l3_planestress(J2d)
            J = l3*J2d
            c1 = lamb*np.log(J)-mu
            c2 = lamb*(1.-lamb/(lamb+2*mu*l3*l3))
        else:
            raise ValueError(f"dPKIdF not implemented for {str(self.hyp):}")
        if F.shape[-2:] == (3,2):
            I4_ = np.eye(6).reshape((3,2,3,2))
            I4_.shape = (1,)*(F.ndim-2)+I4_.shape
            c1 = np.atleast_1d(c1)[(...,)+ (np.newaxis,)*4]
            c2 = np.atleast_1d(c2)[(...,)+ (np.newaxis,)*4]
            return c0*I4_ + c2*cross_ijkl(invFT, invFT) + c1*dinvt(F)
        return self._dPKIdF_exp(c0, c1, c2, invFT)

    def C33_planestress(self, II_C2D:FNDArray) -> FNDArray:
        """ Return the square of the thichkess dilatation in plane stress Case,
            knowing the secons invariant of the planar componant of C """
        mu, lamb= (self.mu, self.lamb)
        a = 0.5*lamb/mu
        ia = 1/a
        b = np.log(II_C2D)
        return  a*np.real(lambertw( np.exp(ia-b)*ia ))

    def potentialC(self, C:FNDArray) -> FNDArray:
        """ Compute The elastic potential as a function of C"""
        mu, lamb= (self.mu, self.lamb)
        if C.shape[-2:] == (3,3):
            I = trace(C)
            logIII  = np.log(det(C))
        elif C.shape[-2:] == (2,2):
            I_C2D  = trace(C)
            II_C2D = det(C)
            if self.hyp == MaterialHypothesis.PLANESTRESS :
                C33 = self.C33_planestress(II_C2D)
            elif self.hyp == MaterialHypothesis.PLANESTRAIN:
                C33 = 1.
            else:
                raise ValueError("self.hyp must be either PLANESTRESS or PLANESTRAIN")
            I  = I_C2D + C33
            logIII  = np.log(II_C2D*C33)
        else :
            raise ValueError("C must be of shape (..., 2,2) or (..., 3, 3)")
        return mu*(I - 3 - logIII)/2. + lamb*logIII**2/8

    def PKII(self, C:FNDArray) -> FNDArray:
        """ Return second Piola Kirchoff Stres as a function of C """
        lamb, mu = (self.lamb, self.mu)
        if C.shape[-2:] == (3,3) or \
            (C.shape[-2:] == (2,2) and self.hyp == MaterialHypothesis.PLANESTRAIN) :
            dim = C.shape[-1]
            III = det(C).reshape( C.shape[:-2] + (1,1))
            logIII = np.log(III)
            Id  = np.eye(dim).reshape( (1,)*(C.ndim-2) + (dim, dim) )
            iCT = invt(C, III)
            kappa = 0.5*lamb*logIII - mu
        elif C.shape[-2:] == (2,2) and self.hyp == MaterialHypothesis.PLANESTRESS :
            Id = np.eye(2).reshape ((1,)*(C.ndim-2) + (2,2))
            II_2D = det(C).reshape(C.shape[:-2]+(1,1))
            iCT = invt(C, II_2D)
            C33 = self.C33_planestress(II_2D)
            logIII = np.log(C33*II_2D)
            kappa =  mu*C33*(lamb*(logIII-1) - 2*mu )/(2*mu*C33+lamb)
        else :
            raise NotImplementedError()
        return mu*Id  + kappa*iCT

    def dPKIIdC(self, C:FNDArray) -> FNDArray:
        lamb, mu = (self.lamb, self.mu)
        iCT = invt(C)
        iCTxiCT = cross_ijkl(iCT, iCT)
        #diCdC = dinv_sym(C, iC)
        diCTdC = dinvt(C, iCT)
        if C.shape[-2:] == (3,3) or \
            (C.shape[-2:] == (2,2) and self.hyp == MaterialHypothesis.PLANESTRAIN) :
            III = det(C).reshape( C.shape[:-2] + (1,1,1,1))
            logIII = np.log(III)
            kappa = 0.5*lamb*logIII- mu
            IIIdkappadIII = 0.5*lamb
            dkappa     = IIIdkappadIII
            return (0.5*lamb)*iCTxiCT + (0.5*lamb*logIII- mu)*diCTdC
        elif C.shape[-2:] == (2,2) and self.hyp == MaterialHypothesis.PLANESTRESS:
            II_2D = det(C).reshape(C.shape[:-2]+(1,1,1,1))
            C33 = self.C33_planestress(II_2D)
            III = C33*II_2D
            logIII = np.log(III)
            a = (2*mu*C33+lamb)
            kappa = mu*C33*(lamb*(logIII-1) - 2*mu)/a
            II_2DxdkappadII_2D = mu*lamb*C33*( a**2 - 2*mu*lamb*(C33-1) -lamb**2*(logIII) ) /a**3
            dkappa = II_2DxdkappadII_2D
            #II_2DxdkappadII_2D = mu*lamb*C33*( a**2 - 2*mu*lamb*(C33-1) -lamb**2*(logIII) ) /a**3
        else :
            raise ValueError("incorrect size for C (..., 2,2) or (..., (3,3))")
        return dkappa*iCTxiCT + kappa*diCTdC


def test_l3(law:CompressibleNeoHookeen, verbose = False)->bool:
    """ testing computation of 3 elongation (plane stress) """
    for j2d in [100.,10.,1.,0.1,0.01]:
        l3=law.l3_planestress(j2d)
        if verbose :
            print(f"J2D: {j2d:.3e}, lambda_3:{l3:.3e}")
    return True

def test_material_law_F(law:HyperElasticMaterial, F:FNDArray)->Tuple[bool, bool]:
    ''' check PKI(F) and dPKIdF(F) return PKI_ok and dPKIDF_ok 
        PKI_ok and dPKIdF_ok, respectively True if PKI (dPKIdF) is close to 
        the finite difference approximation, False otherwise.
    '''
    # pylint: disable=C0103
    PKI = law.PKI(F)
    PKI_num = df_num(law.potentialF, F, F.shape[-2:], ())
    PKI_ok = check_relerr(PKI, PKI_num)
    dPKIdF = law.dPKIdF(F)
    dPKIdF_num = df_num(law.PKI, F, F.shape[-2:], F.shape[-2:])
    dPKIdF_ok = check_relerr(dPKIdF, dPKIdF_num)
    return PKI_ok, dPKIdF_ok
def test_material_law_C(law:HyperElasticMaterial, C:FNDArray)->Tuple[bool, bool]:
    ''' check PKII(C) and dPKIIdC(C) return PKII_ok and dPKIIDC_ok 
        PKII_ok and dPKIIdC_ok, respectively True if PKII (dPKIIdC) is close to 
        the finite difference approximation, False otherwise.
    '''
    # pylint: disable=C0103
    verb = 1
    PKII = law.PKII(C)
    shape = C.shape[-2:]
    PKII_num = 2.*df_num(law.potentialC, C, shape, ())
    PKII_ok = check_relerr(PKII, PKII_num, verbose= verb)
    dPKIIdC = law.dPKIIdC(C)
    dPKIIdC_num = df_num(law.PKII, C, shape, shape)
    # dPKIIdC_num2 = df_num(lambda C : law.PKII( C.swapaxes(-1,-2)), C, shape, shape)
    # dPKIIdC_num = 0.5*(dPKIIdC_num + dPKIIdC_num2)
    dPKIIdC_ok = check_relerr(dPKIIdC, dPKIIdC_num)
    return PKII_ok, dPKIIdC_ok

class Test(unittest.TestCase):
    # pylint: disable=C0103
    """ testing HyperElastic law"""
    def test_compressible_neo_hookean(self):
        """ test PKI and dPKIdF for compressible_neo_hookean """
        lawname = CompressibleNeoHookeen
        lawDP = CompressibleNeoHookeen(E=1., nu = 0.3, hyp=PLANESTRAIN)
        lawCP = CompressibleNeoHookeen(E=1.1, nu = 0.3, hyp=PLANESTRESS)
        law3D = CompressibleNeoHookeen(E=1., nu = 0.3, hyp=DIM3)
        laws2D = [lawDP, lawCP]
        laws3D = [law3D]
        # lawTEST1 = SimpleLaw0()
        # lawTEST2 = SimpleLaw1()
        # lawTEST3 = SimpleLaw2()
        gen = random_lintransform
        F22_1 = gen(1, 2).squeeze()
        F22_2 = gen(10, 2).squeeze()
        F32_1 = random_lintransform2dto3d(1).squeeze()
        F32_2 = random_lintransform2dto3d(10)
        F2D = [F22_1, F22_2, F32_1, F32_2]
        F1 = gen(1, 3).squeeze()
        F2 = gen(10, 3).squeeze()
        F3D = [F1, F2]
        for laws, Fs in zip([laws2D, laws3D], [F2D, F3D] ):
            for law in laws : #, lawTEST1, lawTEST2, lawTEST3]:
                for F in Fs:
                    PKI_ok, dPKI_ok = test_material_law_F(law, F)
                    C = metric(F)
                    PKII_ok, dPKII_ok = test_material_law_C(law, C)
                    self.assertTrue(PKI_ok, msg = f" PKI computation wrong for {lawname} {law.hyp}")
                    self.assertTrue(dPKI_ok, msg = f" dPKI computation wrong for {lawname} {law.hyp}")
                    self.assertTrue(PKII_ok, msg = f" PKII computation wrong for {lawname} {law.hyp}")
                    self.assertTrue(dPKII_ok, msg = f" dPKII computation wrong for {lawname} {law.hyp}")

def benchmark(law:HyperElasticMaterial, n:int, mem = False, prof = True):
    """ benchmark : compute and profile  law.PKI and law.dPKIDF """
    # pylint: disable=C0103
    dim = HYP2DIM[law.hyp]
    if law.hyp in [PLANESTRESS, PLANESTRAIN] and mem:
        F = random_lintransform2dto3d(n)
        print(f'computing PKI and dPKIDF for {n:.3e} F32, in in {law.hyp}')
    else :
        gen = random_lintransform
        F = gen(n, dim)
        print(f'computing PKI and dPKIDF for {n:.3e} F, in {law.hyp}')
    Profile.doprofile = prof
    Profile.enable()
    start = time.time()
    __ = law.PKI(F)
    __ = law.dPKIdF(F)
    end = time.time()
    Profile.disable()
    Profile.print_stats()
    print('time : ', end - start)

if __name__ == "__main__":
    law3D0 = CompressibleNeoHookeen(E=1.2, nu = 0.3, hyp=DIM3)
    F = random_lintransform(1,3)
    C  =metric(F)
    print(law3D0.potentialC(C))
    print(law3D0.potentialF(F))
    
    print(inv(F)@law3D0.PKI(F))
    print(test_material_law_C(law3D0, C))
    benchmark(law3D0, 1000000, prof=True)
    # computing PKI and dPKIDF for 1.000e+06 F, in MaterialHypothesis.DIM3
    # 550 function calls (532 primitive calls) in 1.540 seconds
    # lawPSTRAIN = CompressibleNeoHookeen(E=1.2, nu = 0.3, hyp=PLANESTRAIN)
    # lawPSTRESS = CompressibleNeoHookeen(E=1.2, nu = 0.3, hyp=PLANESTRESS)
    # # benchmark(lawPSTRESS, 10000, mem =True, prof =True)
    # law1 = SimpleLaw0()
    # law2 = SimpleLaw1()
    # law3 = SimpleLaw2()
    # F2d0 = np.array([[2.,0.1],[1.,1.3]])
    # F2d1 = np.array([[[2.,0.1],[1.,1.3]]])
    # F2d2 = np.array([[[2.,0.1],[1.,1.3]],
    #                 [[2.,0.5],[3.,1.1]]
    #                 ])
    # Fm = np.array( [
    #                [[1.,0.],[0.,1.],[0,0]],
    #                [[1.,0.],[0.,0.],[0,1]]
    #                ])
    # #Fm = np.array([[1.,0.],[0.,1.],[0,0]])
    # Fm2d = np.array([[2.,0.1],[1.,1.3],[0.,0.]])
    # Fm = np.array([[[2.,0.], [0.,1.], [0,0]],
    #                [[2.,0.1],[0.1,1.],[0,0]]])
    # Fm0= np.array([[[3.,0.1], [1., 0.1], [1.,1.]],
    #                [[2.,0.1],[0.1,1.],[0.1,0.2]]])
    # Fm1= np.array([[[3.,0.1], [1., 0.1], [1.,1.]],
    #                [[2.,0.1],[0.1,1.],[0.1,0.2]]])
    # lawu = lawPSTRESS
    # Fu  = Fm1
    # print(test_material_law(lawu, Fu))

# computing PKI and dPKIDF for 1.000e+06 F, in MaterialHypothesis.DIM3
#          62 function calls in 1.243 seconds
#    Ordered by: cumulative time
#    ncalls  tottime  percall  cumtime  percall filename:lineno(function)
#         1    0.013    0.013    1.031    1.031 hyperelasticity.py:221(dPKIdF)
#         1    0.682    0.682    0.865    0.865 hyperelasticity.py:186(_dPKIdF_exp)
#         2    0.035    0.017    0.310    0.155 smallsize_tensor.py:69(det_invt)
#         2    0.262    0.131    0.262    0.131 smallsize_tensor.py:25(_cofac)
#         1    0.000    0.000    0.211    0.211 hyperelasticity.py:214(PKI)
#         1    0.003    0.003    0.211    0.211 hyperelasticity.py:164(_PKI_3d)
#         1    0.182    0.182    0.182    0.182 smallsize_tensor.py:79(cross_ijkl)
#         1    0.052    0.052    0.052    0.052 hyperelasticity.py:171(_PKI_exp)
#         2    0.014    0.007    0.014    0.007 smallsize_tensor.py:41(_det)
