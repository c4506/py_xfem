#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module is dedicated to implement faster functionnality for tensor on spaces of dim 2 and 3
than the equivalent numpy call
Created on May1 2024
@author: nchevaug
"""
from __future__ import annotations
import unittest
from typing import Tuple, Callable, Literal
#from warnings import warn
#import unittest
import numpy as np
import numpy.typing as npt
import scipy as sp
import mygrad as mg
#from execTools import Profile
# USE_NUMBA = False
# if USE_NUMBA :
#     import numba as nu

FNDArray = npt.NDArray[np.float64]
I4_3D = np.eye(9).reshape((3,3,3,3))
I4_2D = np.eye(4).reshape((2,2,2,2))
I4 = {2:I4_2D, 3:I4_3D}
""" unit Order4 tensor"""

def random_orthtransform(n:int, dim:Literal[2,3]) -> FNDArray:
    """ generate n tensor F of shape dimxdim such as det(F)>=0. """
    # pylint: disable=C0103
    if dim==2 :
        alpha = np.random.rand(n)*2*np.pi
        sin = np.sin(alpha)
        cos = np.sin(alpha)
        Q = np.zeros((n, 2, 2))
        Q[:,0,0] = cos
        Q[:,0,1] = -sin
        Q[:,1,0] = sin
        Q[:,1,1] = cos
        return Q
    if dim == 3 :
        Q = np.random.rand(n, dim, dim)
        Q[...,:,0] = Q[...,:,0]/np.linalg.norm(Q[...,:,0], axis = -1, keepdims=True)
        Q[...,:,1] = np.cross(Q[...,:,2], Q[...,:,0])
        Q[...,:,1] = Q[...,:,1]/np.linalg.norm(Q[...,:,1], axis = -1, keepdims=True)
        Q[...,:,2] = np.cross(Q[...,:,0], Q[...,:,1])
        return Q

def vecprodmat(n:FNDArray) -> FNDArray:
    """ from an array of vector n return an array of matrix Q,
        such as Q[i].x[i] = n[i] cross x[i] """
    # pylint: disable=C0103
    assert n.shape[-1] == 3
    Q = np.zeros(n.shape[:-1] + (3,3))
    Q[...,0,1] = -n[...,2]
    Q[...,0,2] =  n[...,1]
    Q[...,1,0] =  n[...,2]
    Q[...,1,2] = -n[...,0]
    Q[...,2,0] = -n[...,1]
    Q[...,2,1] =  n[...,0]
    return Q

def random_lintransform(n:int, dim:int) -> FNDArray:
    """ generate n tensor F of shape dimxdim such as det(F)>=0. """
    # pylint: disable=C0103
    F = np.random.rand(n, dim, dim)
    Jneg = np.sign(det(F)) <= 0.
    F[Jneg,0,:] = -F[Jneg,0,:]
    return F

def random_lintransform2dto3d(n:int) -> FNDArray:
    """ generate n tensor F of shape dimxdim such as det(F)>=0. """
    # pylint: disable=C0103
    F = np.random.rand(n, 3, 2)
    return F

def metric(F:FNDArray) ->FNDArray:
    """ return the metric of F m =(FT.F), where F is interpreted as the gradient of a
    transformation """
    # pylint: disable=C0103
    return trans(F)@F

def dmetric(F:FNDArray) -> FNDArray:
    """ return a collection of 4 order tensor : R[k, iajb] = dm[k,ab]/dF[k, ij] """
    # pylint: disable=C0103
    dim = F.shape[-1]
    I = np.eye(dim)
    return np.einsum("...ac,...ib->...abic", I, F) + np.einsum("...bc,...ia->...abic", I, F)

def d2metric(F:FNDArray) -> FNDArray:
    """ return a collection of 6 order tensor : R[k, iajb] = d2m[k,ab]/d2F[k, ij] """
    # pylint: disable=C0103
    I0 = np.eye(F.shape[-2])
    I1 = np.eye(F.shape[-1])
    d2m =  np.einsum("...ac, ...ij, ...bd ->...abicjd", I1, I0, I1)  \
         + np.einsum("...bc, ...ij, ...ad ->...abicjd", I1, I0, I1)
    d2m.shape = (1,)*(F.ndim-2) + d2m.shape
    return d2m

def invmetric(F:FNDArray)->FNDArray:
    """ return an array of invert metric R[k, ab] = inv(F[k][ia]F[k][ib]) """
    # pylint: disable=C0103
    return inv(metric(F))

def dinvmetric(F):
    """ return the derivative of inverse of metric of F"""
    # pylint: disable=C0103
    im = invmetric(F)
    return -np.einsum("...ac,...ib->...abic", im, F@im)-np.einsum("...cb,...ia->...abic", im, F@im)

def trace(A:FNDArray) -> FNDArray:
    """ return an array of the trace of each tensor in A"""
    # pylint: disable=C0103
    if A.shape[-1] != A.shape[-2]:
        raise ValueError("in trace, each array in the collection must be square")
    dim = A.shape[-1]
    if dim == 1:
        tr  = A[...,0,0]
    elif dim == 2:
        tr = A[...,0,0] + A[...,1,1]
    elif dim == 3:
        tr = A[...,0,0] + A[...,1,1] + A[...,2,2]
    else:
        tr = np.einsum('...ii', A)
    return tr

def trace2(A:FNDArray) -> FNDArray:
    """ return an array of the trace ATA (second invariant)"""
     # pylint: disable=C0103
    if A.shape[-1] != A.shape[-2]:
        raise ValueError("in trace, each array in the collection must be square")
    return np.sum(np.square(A), axis = (-1,-2))

def trans(A:FNDArray)-> FNDArray:
    """ return the transposes of the input collection of second order tensors """
    # pylint: disable=C0103
    return A.swapaxes(-1,-2)

def contract(A:FNDArray, B:FNDArray)-> FNDArray:
    """ from Collection A and collection C of order 2 tensors , return the collection iC = iA:iB """
    # pylint: disable=C0103
    assert A.shape == B.shape
    assert A.ndim >= 2
    if A.shape[:-2] == (2,2):
        return A[...,0,0]*B[...,0,0]+A[...,0,1]*B[...,0,1] \
             + A[...,1,0]*B[...,1,0]+ A[...,1,1]*B[...,1,1]
    return np.einsum("...ij, ...ij-> ...", A, B)

def _cofac(A:FNDArray) -> FNDArray:
    """compute cofac matrix for stack of 2x2 or 3x3 matrices """
    # pylint: disable=C0103
    if A.shape[-2:] == (2,2):
        cA = np.empty_like(A)
        cA[..., 0, 0] =  A[..., 1, 1]
        cA[..., 0, 1] = -A[..., 1, 0]
        cA[..., 1, 0] = -A[..., 0, 1]
        cA[..., 1, 1] =  A[..., 0, 0]
        return cA
    if A.shape[-2:] == (3,3):
        cA = np.empty_like(A)
        cA[...,0,:] = cross(A[...,1,:], A[...,2,:])
        cA[...,1,:] = cross(A[...,2,:], A[...,0,:])
        cA[...,2,:] = cross(A[...,0,:], A[...,1,:])
        return cA
    raise ValueError("A must be a (..,2,2) or (...,3,3) array_like")

def _det(A:FNDArray) -> FNDArray:
    """ return the determinant of A (2,2) or (3,3). fasterthan  np.linalg.det for this sizes """
    # pylint: disable=C0103
    if A.shape[-2:] == (3,2):
        m = metric(A)
        return np.sqrt(_det(m))
    if A.shape[-2:] == (2,2):
        return A[...,0,0]*A[...,1,1]- A[...,0,1]*A[...,1,0]
    if A.shape[-2:] == (3,3):
        a= A[...,0,0]
        b= A[...,0,1]
        c= A[...,0,2]
        d= A[...,1,0]
        e= A[...,1,1]
        f= A[...,1,2]
        g= A[...,2,0]
        h= A[...,2,1]
        i= A[...,2,2]
        detA = a*e*i +b*f*g +c*d*h - c*e*g -b*d*i -a*f*h
        return detA
    raise ValueError(f"Size error {str(A.shape)}")

det = _det
def ddet(A:FNDArray)->FNDArray:
    """ Return the derivative of the determinant"""
    # pylint: disable=C0103
    if A.shape[-1]==A.shape[-2]:
        return cofac(A)
    J, invAT = det_invt(A)
    return  ((np.atleast_1d(J)[:, np.newaxis, np.newaxis])*invAT).reshape(A.shape)

cofac = _cofac

# if USE_NUMBA:
#     det = nu.njit(_det)
#     cofac = nu.njit(_cofac)

def cross(A:FNDArray, B:FNDArray) -> FNDArray:
    """ Compute the cross product of the vectors in A(..., 3) and  in B (...,3)"""
    # pylint: disable=C0103
    if isinstance(A, np.ndarray) and isinstance(B, np.ndarray):
        return np.cross(A,B)
    if A.shape[-1] != 3 or B.shape[-1] !=3 :
        raise ValueError("A and be most be of shape (..., 3) ")
    C = np.empty_like(A)
    C[...,0] =  A[..., 1] * B[...,2]  - A[..., 2] * B[...,1]
    C[...,1] = -A[..., 0] * B[...,2]  + A[..., 2] * B[...,0]
    C[...,2] =  A[..., 0] * B[...,1]  - A[..., 1] * B[...,0]
    return C

def invariants(A:FNDArray) -> FNDArray:
    """ Return the 3 (or 2) invariants of A :
      - trace(A), 0.5(trace(A)**2 - trace(A.T@A))", det(A)  for 3d tensor
      - trace(A), det(A) for 2d tensor
    """
    # pylint: disable=C0103
    if A.shape[-2:] == (2,2):
        r = np.empty( A.shape[:-2]+(2,))
        # print("A.shape", A.shape, "trace(A).shape", trace(A).shape, "det(A).shape", det(A).shape)
        # print("r.shape", r.shape)
        r[...,0] = trace(A)
        r[...,1] = det(A)
        return r
    if A.shape[-2:] == (3,3):
        r = np.empty( A.shape[:-2]+(3,))
        trA = trace(A)
        r[...,0] = trA
        r[...,1] =  0.5*( np.square(trA) - trace(trans(A)@A) )
        r[...,2] =  det(A)
        return r
    raise ValueError("A must be of size (..., 2, 2) or (..., 3, 3)")

def dinvariants(A:FNDArray) -> FNDArray:
    """ Return the 3 (or 2) derivatives of invariants of A with regard to A :
      - dtrace(A)dA, d0.5(trace(A)**2 - trace(A.T@A))dA, ddet(A)dA  for 3d tensor
      - dtrace(A)dA, ddet(A)dA for 2d tensor
    """
    # pylint: disable=C0103
    if A.shape[-2:] == (2,2):
        r = np.empty( A.shape[:-2]+(2,2,2))
        I = np.eye(2)[(np.newaxis,)*len(A.shape[:-2]) ]
        r[...,0, :, :] = I
        r[...,1, : ,:] = cofac(A)
        return r
    if A.shape[-2:] == (3,3):
        nfirstdim = len(A.shape[:-2])
        r = np.empty( A.shape[:-2]+(3,3,3))
        I = np.eye(3)[(np.newaxis,)*nfirstdim ]
        trA = trace(A)[..., None, None]
        r[...,0,:,:] =  I
        r[...,1,:,:] =  trA*I-A
        r[...,2,:,:] =  cofac(A)
        return r
    raise ValueError("A must be of size (..., 2, 2) or (..., 3, 3)")

def invariants2Dto3D(invariant2D):
    """ from the invariant of a "2.5d tensor", compute the invariants of the equivalent 3D tensor 
        what I mean by a 2.5d tensor is for example the gradiant of the 
        transformation in plan stress  case :
            F = F2D_{a,b} e_a x e_b  + l2 e_2 x e_2 , where a, b in [0,1] and e_2 = e_0 vec e_1
        the invarariant 2D are strored in a (..., 3) ndarray, where
        invariant2D[..., 0] is an array containing  I(F2D) = trace(F2D)
        invariant2D[..., 1] is an array containing  
            II(F2D) = 0.5(trace(F2D)^2 - trace(F2D.T@F2D)) = det(F2D)
        invariant2D[..., 2] is an array containing  l2
    """
    # pylint: disable=C0103
    invariant3D = np.empty(invariant2D.shape)
    I2D  =  invariant2D[...,0]
    II2D =  invariant2D[...,1]
    l2   =  invariant2D[...,2]
    invariant3D[..., 0] = I2D  + l2
    invariant3D[..., 1] = II2D + l2*I2D
    invariant3D[..., 2] = II2D * l2
    return invariant3D

def dinvariants2Dto3D(invariant2D:FNDArray) -> FNDArray:
    """ return the matrix of the derivatives of the 3D invariants of tensor A
        computed as a function of the 2D invariant.
        input invariant2D an FNDArray of shape[-1] = 3 where :
        invariant2D[..., 0]  = I2D = trace(A2D)
        invariant2D[..., 1]  = II2D = det(A2D) = 0.5*(trace(A2D)**2 - trace(A2D@A2D**2)
        invariant2D[..., 2]  = A33
        return dinvariant3D an FNDarray of shape (..., 3, 3)
        where dinvariant3D[..., i, j] contain the derivative of the ith 3d invarariant with regard
        to the jth 2d invariant
    """
    # pylint: disable=C0103
    I2D  =  invariant2D[...,0]
    II2D =  invariant2D[...,1]
    l2   =  invariant2D[...,2]
    dinvariant3D = np.ones(invariant2D.shape + (3,))
    #dinvariant3D[..., 0, 0] = 1.
    dinvariant3D[..., 0, 1] = 0.
    #dinvariant3D[..., 0, 2] = 1.
    dinvariant3D[..., 1, 0] = l2
    #dinvariant3D[..., 1, 1] = 1.
    dinvariant3D[..., 1, 2] = I2D
    dinvariant3D[..., 2, 0] = 0.
    dinvariant3D[..., 2, 1] = l2
    dinvariant3D[..., 2, 2] = II2D
    return dinvariant3D

def d2invariants2Dto3D(invariant2D):
    """ return the matrix of the second derivatives of the 3D invariants of tensor A
        computed as a function of the 2D invariants.
        input invariant2D an FNDArray of shape[-1] = 3 where :
        invariant2D[..., 0]  = I2D = trace(A2D)
        invariant2D[..., 1]  = II2D = det(A2D) = 0.5*(trace(A2D)**2 - trace(A2D@A2D**2)
        invariant2D[..., 2]  = A33
        return dinvariant3D an FNDarray of shape (..., 3, 3, 3)
        where dinvariant3D[..., i, j, k] contain the derivative of the ith 3d invarariant 
        with regard to the jth 2d invariant and the kth 2d invarariant. 
        Note that the resuluting 27 component matrix contain only 4 non zero terms ...
    """
    # pylint: disable=C0103
    d2invariant3D = np.zeros(invariant2D.shape + (3, 3))
    d2invariant3D[..., 1, 0, 2] = 1.
    d2invariant3D[..., 1, 2, 0] = 1.
    d2invariant3D[..., 2, 1, 2] = 1.
    d2invariant3D[..., 2, 2, 1] = 1.
    return d2invariant3D

def inv(A:FNDArray, detA:FNDArray|float|None = None) -> FNDArray:
    """compute inverse matrix for stack of 2x2 or 3x3 matrices """
    # pylint: disable=C0103
    if A.shape[-2:] == (3,2):
        m = metric(A)
        iA = inv(m)@trans(A)
        return iA
    if detA is None :
        detA = det(A)
    if not isinstance(detA, (float, int)):
        detA.shape = A.shape[:-2]+(1,1)
    iA = np.empty_like(A)
    if A.shape[-2:] == (2,2):
        iA[..., 0,0] =   A[...,1,1]
        iA[..., 0,1] =  -A[...,0,1]
        iA[..., 1,0] =  -A[...,1,0]
        iA[..., 1,1] =   A[...,0,0]
    elif A.shape[-2:] == (3,3):
        iA[...,:,0] = cross(A[...,1,:], A[...,2,:])
        iA[...,:,1] = cross(A[...,2,:], A[...,0,:])
        iA[...,:,2] = cross(A[...,0,:], A[...,1,:])
    if A.shape[:-2] == ():
        iA = iA.squeeze()
    return iA/detA

def dinv(A:FNDArray, iA:FNDArray|None = None )-> FNDArray:
    """ return a collection of 4 order tensor : R[k, iajb] = d F^-1[k,ab]/dF[k] [ij] """
    # pylint: disable=C0103
    if A.shape[-2:] == (3,2):
        im = inv(metric(A))
        diA = np.empty(A.shape[:-2] + (2,3,3,2))
        diA = -np.einsum("...ab,...de,...jd,...ie->...aijb", im, im, A, A )
        diA -= np.einsum("...ad,...be,...jd,...ie->...aijb", im, im, A, A )
        diA += np.einsum("...ab, ...ij -> ...aijb", im, np.eye(3))
        return diA
    iA = inv(A)
    return -np.einsum("...ac,...db->...abcd", iA, iA)

def invt(A:FNDArray, detA:FNDArray|float|None = None) -> FNDArray:
    """compute inverse transpose matrix for stack of 2x2 or 3x3 matrices or pseudo inverse transpose
        if matrix are 3x2
    """
    # pylint: disable=C0103
    return np.swapaxes(inv(A, detA), -1,-2)
    # if A.shape[-2:] == (3,2):
    #     m = metric(A)
    #     return A@inv(m)
    # if A.shape[-2:] in [(2,2), (3,3)]:
    #     if detA is None:
    #         detA = det(A)
    #     return (cofac(A)/np.atleast_1d(detA)[..., None, None]).reshape(A.shape)
    # else:
    #     raise ValueError(" A.shape[-2:] must be (2,2), (3,2) or (3,3)")

def dinv_sym(A, iA = None) -> FNDArray:
    " return the derivative of the inverse of A with regard to A, Assuming A sym"
    # pylint: disable=C0103
    if A.shape[-2:]  in [(3,3), (2,2)]:
        if iA is None :
            iA = inv(A)
        return  -0.5*cross_iklj(iA, iA) -0.5*cross_ilkj(iA, iA)
    raise ValueError(" A.shape[-2:] must be (2,2) or (3,3)")

def dinvt(F, iFT =None, im = None, dimdF = None)->FNDArray:
    """ return the derivative of F^-T """
    # pylint: disable=C0103
    if F.shape[-2:] == (3,2):
        I = np.eye(3)
        im = invmetric(F) if im is None else im
        dimdF = dinvmetric(F) if dimdF is None else dimdF
        return np.einsum("...ij, ...ba -> ...iajb", I, im) \
             + np.einsum("...il, ...lajb-> ...iajb", F, dimdF)
    iFT = invt(F) if iFT is None else iFT
    return -cross_ilkj(iFT, iFT)



def det_invt(A:FNDArray) -> Tuple[FNDArray|float, FNDArray]:
    """ compute both detA and A^{-T}, faster than calling np.det and np.inv for the 2d case.
        Also treat the "membrasne case" where A is of shape (3,2)
    """
    # pylint: disable=C0103
    if A.shape[-2:] == (3,2):
        m = metric(A)
        detm = det(m)
        invAT = A@inv(m)
        return np.sqrt(detm), invAT
    if A.shape[-2:] in [(2,2), (3,3)]:
        detA = det(A)
        adjA = cofac(A)
        invAT = adjA / np.atleast_1d(detA)[..., None, None]
        return detA, invAT
    raise ValueError("A.shape[-2,:] should be (2, 2) or (3, 3) or (3,2)"+str(A.shape) )

def _cross_ijkl(A:FNDArray, B: FNDArray) -> FNDArray:
    """  For A and B a (...,n,n) compute C (..., n, n, n, n)
    C[..., i,j,k,l] = A[..., i,j]*B[..., k, l]
    """
    # pylint: disable=C0103
    return np.einsum("...ij, ...kl-> ...ijkl", A, B)
    # dim0 = A.shape[-2]
    # dim1 = A.shape[-1]
    # colshape = A.shape[:-2]
    # #C= A.reshape(-1,dim**2,1)@B.reshape(-1, 1, dim**2)
    # C = np.matmul(A.reshape(-1,dim0*dim1,1), B.reshape(-1, 1, dim0*dim1))
    # return C.reshape(colshape+(dim0, dim1, dim0, dim1))
    # unfortunatly the above implementation is not compatible with numba ..
    # numba only support the basic dot product
    # dim = A.shape[-1]
    # colshape = A.shape[:-2]
    # if colshape == ():
    #     colshape= (1,)
    # dim2 = dim**2
    # entries = A.size//(dim**2)
    # A = A.reshape(entries, dim2)
    # B = B.reshape(entries, dim2)
    # Cshape = (entries,) + (dim2, dim2)
    # C = np.zeros(Cshape)
    # for i in range(entries):
    #     for k in range(dim2):
    #         for l in range(dim2):
    #             C[i,k,l] = A[i,k]*B[i,l]
    # return C.reshape(colshape+(dim, dim, dim, dim))

cross_ijkl = _cross_ijkl

def cross_ikjl(A:FNDArray, B: FNDArray) -> FNDArray:
    """  For A and B a (...,n,n) compute C (..., n, n, n, n)
    C[..., i,j,k,l] = A[..., i,k]*B[..., j, l]
    """
    # pylint: disable=C0103
    return np.einsum("...ik, ...jl-> ...ijkl", A, B)

def cross_iklj(A:FNDArray, B: FNDArray) -> FNDArray:
    """  For A and B a (...,n,n) compute C (..., n, n, n, n)
    C[..., i,j,k,l] = A[..., i,k]*B[..., l, j]
    """
    # pylint: disable=C0103
    return np.einsum("...ik, ...lj-> ...ijkl", A, B)

def cross_ilkj(A:FNDArray, B: FNDArray) -> FNDArray:
    """  For A and B a (...,n,n) compute C (..., n, n, n, n)
    C[..., i,j,k,l] = A[..., i,l]*B[..., k, j]
    """
    # pylint: disable=C0103
    return np.einsum("...il, ...kj-> ...ijkl", A, B)

# if USE_NUMBA:
#     cross_iljk = nu.njit(_cross_ijkl)

# if USE_NUMBA:
#     F = random_lintransform(n=100, dim=3)
#     __ = det(F)
#     __ = cofac(F)
#     __ = cross_ijkl(F,F)

def cross_likj(A:FNDArray, B: FNDArray) -> FNDArray:
    """  For A and B a (...,n,n) compute C (..., n, n, n, n)
    C[..., i,j,k,l] = A[..., l,i]*B[..., k, j]
    """
    # pylint: disable=C0103
    AijBkl= cross_ijkl(A,B)
    axis = list(range(AijBkl.ndim-4)) + [-3,-1,-2,-4]
    AliBkj =  AijBkl.transpose(axis)
    return AliBkj

def cross_iljk(A:FNDArray, B: FNDArray) -> FNDArray:
    """  For A and B a (...,n,n) compute C (..., n, n, n, n)
    C[..., i,j,k,l] = A[..., i,l]*B[..., j, k]
    """
    # pylint: disable=C0103
    AijBkl = cross_ijkl(A,B)
    axis = list(range(AijBkl.ndim-4)) + [-4,-2,-1,-3]
    AilBjk = AijBkl.transpose(axis)
    return AilBjk

def df_num( f:Callable[[FNDArray], FNDArray|float], A_in:FNDArray|float,
           shape_iA:tuple[int,...], shape_ifA:tuple[int,...])-> FNDArray:
    """ compute the numerical derivative of f(A) at A"""
    # pylint: disable=C0103
    A = np.atleast_1d(A_in)
    dim_coll = A.ndim - len(shape_iA)
    shape_coll = A.shape[:dim_coll]
    shape_idfA = shape_ifA + shape_iA
    df_num_res = np.zeros(shape_coll+shape_idfA)
    def fflat(iAflat):
        fiA = f(iAflat.reshape(shape_iA))
        if isinstance(fiA, np.ndarray) :
            fiA = fiA.flatten()
        return fiA
    for k in np.ndindex(A.shape[:dim_coll]):
        Ak = A[k]
        df_num_res[k] = sp.optimize.approx_fprime(Ak.flatten(), fflat).reshape(shape_idfA)
    if not isinstance(A_in, np.ndarray) :
        df_num_res = df_num_res.squeeze()
        # if isinstance(A_in, float) and shape_ifA == ():
        #     df_num_res = df_num_res[0]
    return df_num_res

def df_vector_auto(Fun, X):
    """ compute the auto derivative of fun(A) at A where A is a vector and fun(A) a vector"""
    # pylint: disable=C0103
    dim = X.shape[-1]
    dFundX_shape = X.shape + (dim,)
    dFundX = np.zeros(dFundX_shape)
    mgX = mg.Tensor(X)
    for i in range(dim):
        fxi = Fun(mgX)[..., i]
        fxi.backward()
        dFundX[...,i,:] = np.array(mgX.grad)
    return dFundX

def df_tensor_auto(Fun, X, shape_fun=None):
    # pylint: disable=C0103
    """ compute the auto derivative of fun(A) at A where A is a tensor and fun(A) a tensor"""
    xdim0 = X.shape[-2]
    xdim1 = X.shape[-1]
    if shape_fun is None:
        shape_fun=(xdim0, xdim1)
    fdim0 = shape_fun[0]
    fdim1 = shape_fun[1]
    shapedfdx = X.shape[:-2] + ( fdim0, fdim1, xdim0, xdim1)
    dFdX = np.empty(shapedfdx)
    mgX = mg.Tensor(X)
    for i in range(fdim0) :
        for j  in range(fdim1):
            FX = Fun(mgX)[..., i, j]
            FX.backward()
            dFdX[..., i,j, :,: ] = mgX.grad
    return dFdX

def check_relerr(ref:FNDArray, val:FNDArray, reltol:float = 1.e-3, abstol:float = 1.e-5,
                 checkname:str="", verbose:Literal[0,1,2] = 0) -> bool:
    """ Utility function to compare 2 results """
    err = np.abs((ref-val))
    if verbose == 2:
        print(f"check {checkname}")
        print("ref \n", ref,"\n val \n", val,  "\n err \n", err)
    if np.all(err < abstol):
        return True
    if np.any(err > np.abs(ref)*reltol):
        if verbose > 0:
            print(f"check {checkname} Failed, for abstol = {abstol:.4e}, reltol {reltol:.4e} ")
            delta = err - np.abs(ref)*reltol
            imax = np.argmax(delta)
            deltamax = delta.flatten()[imax]
            print(f"argmax, max (abs(ref-val) - abs(ref)*reltol) : {imax:d},  {deltamax:.4e}")
        return False
    return True

class Test(unittest.TestCase):
    """ testing small_sizetensor """
    # pylint: disable=C0103
    def test_vecprodmat(self):
        """ check function that compute Q(a) such as Q(a)@b = cross(a,b) """
        print('  - checking vecprodmat')
        verb = 1
        a = np.random.rand(3*10).reshape(10,3)
        b = np.random.rand(3*10).reshape(10,3)
        c_ref = np.cross(a, b)
        Q = vecprodmat(a)
        c = (Q@b.reshape(b.shape + (1,))).squeeze()
        self.assertTrue(check_relerr(c_ref, c, reltol = 1.e-3, abstol=1.e-6, verbose = verb),
                        msg = "problems in vecprodmat")

    def test_randomorth(self):
        """ testing Random Orh Transform again QTQ- I = 0" """
        for dim in [2,3]:
            n= 5
            print(f"\nGenerating {n:d} dim {dim:d}x{dim:d} transformations")
            Q = random_orthtransform(n, 3).squeeze()
            Z = trans(Q)@Q - np.eye(3)[np.newaxis]
            nZ = np.linalg.norm(Z, axis=(-1,-2))
            self.assertTrue(np.all(nZ < 1.e-6), msg = "QTQ -I != 0")
            self.assertTrue( np.all( np.abs(det(Q)-1.) < 1.e-6), msg ="detQ != 1.")

    def test_derivatives_num(self):
        """ test the derivative (d_funname) of the function defined in this file by comparing them
            to numerical derivatives """
        # pylint: disable=C0103
        F22 = random_lintransform(1, 2).squeeze()
        F22_1 = random_lintransform(1, 2)
        F22_4 = random_lintransform(4, 2)
        F33 = random_lintransform(1, 3).squeeze()
        F33_1 = random_lintransform(1, 3)
        F33_4 = random_lintransform(4, 3)
        F32   = np.random.rand(3*2).reshape(3,2)
        F32_1   = np.random.rand(3*2).reshape(1, 3, 2)
        F32_4   = np.random.rand(4*3*2).reshape(4, 3, 2)
        Ftest_square = [F22, F22_1, F22_4, F33, F33_1, F33_4]
        Ftest_all = [F22, F22_1, F22_4, F33, F33_1, F33_4, F32, F32_1, F32_4]

        f_fshape_deriv = [(det, lambda shapeF: (), ddet, Ftest_all),
                           (metric, lambda shapeF: (shapeF[1],)*2, dmetric, Ftest_all),
                           (invmetric, lambda shapeF: (shapeF[1],)*2, dinvmetric, Ftest_all),
                           (inv, lambda shapeF: (shapeF[-1], shapeF[-2]), dinv, Ftest_all),
                           (invt, lambda shapeF: shapeF, dinvt, Ftest_all),
                           (invariants, lambda shapeF: shapeF[-1:], dinvariants, Ftest_square)
                         ]
        print('\n')
        for f, fshape, df, Ftest in f_fshape_deriv:
            print(f'  - checking {df.__name__}')
            for F in Ftest:
                iFshape  = F.shape[-2:]
                ifFshape = fshape(iFshape)
                dfF_num = df_num(f, F, iFshape, ifFshape)
                dfF = df(F)
                ok = check_relerr(dfF, dfF_num, reltol = 1.e-3, abstol=1.e-6, verbose = 1)
                self.assertTrue(ok, msg = f"Error in function {df.__name__}")

    def test_derivative_auto(self):
        """ test the derivative of function on tensor by comparing to auto differentiation  """
        F22 = random_lintransform(1, 2).squeeze()
        F22_1 = random_lintransform(1, 2)
        F22_4 = random_lintransform(4, 2)
        F33 = random_lintransform(1, 3).squeeze()
        F33_1 = random_lintransform(1, 3)
        F33_4 = random_lintransform(4, 3)
        F32   = np.random.rand(3*2).reshape(3,2)
        F32_1   = np.random.rand(3*2).reshape(1, 3, 2)
        F32_4   = np.random.rand(4*3*2).reshape(4, 3, 2)
        Ftest = [F22, F22_1, F22_4, F33, F33_1, F33_4, F32, F32_1, F32_4]
        fun_dfun_shapefun= [(metric, dmetric, lambda shape: (shape[-1], shape[-1])),
                            (invmetric, dinvmetric, lambda shape: (shape[-1], shape[-1])),
                            (inv, dinv, lambda shape: (shape[-1], shape[-2]) ),
                            (invt, dinvt, lambda shape: shape[-2:] ),
                            ]
        # f_fshape_deriv = [(det, lambda shapeF: (), ddet),
        #                     (metric, lambda shapeF: (shapeF[1],)*2, dmetric),
        #                     (invmetric, lambda shapeF: (shapeF[1],)*2, dinvmetric),
        #                     #(inv, lambda shapeF: shapeF, dinv),
        #                     (invt, lambda shapeF: shapeF, dinvt)
        #                     ]
        print('\n')
        for fun, dfun, shape_fun in fun_dfun_shapefun:
            print(f'  - checking {dfun.__name__}')
            for F in Ftest:
                dfF_auto = df_tensor_auto(fun, F, shape_fun(F.shape))
                dfF = dfun(F)
                ok = check_relerr(dfF, dfF_auto, reltol = 1.e-6,  verbose = 1)
                self.assertTrue(ok, msg = f"Error in function {dfun.__name__}")
