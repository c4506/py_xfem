#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 14:58:41 2024

@author: nchevaug
"""
import numpy as np
import gmsh
import gmsh_mesh_model_interface as meshInterface
import gmsh_mesh_generator as mg
import gmsh_post_pro_interface as gp
import thermic_solver as ts
import exact_solutions as ex


def convergence_ridge(enr=None, algo=''):
    """ Do a convergence analysis using xfem/ridge enrichment
    on a problem with discontiunuous Gradient """
    # Conductivities :
    k1 = 1.
    k2 = 10.*k1
    # Lenght of the square
    lenght = 2.
    # radius of the disk separting conductivities.
    ri = 0.4
    # element size_i is elementSize0**i for i in steps
    h0 = 0.5
    steps = np.arange(2, 8)
    # linear solver
    solver = "cholmod"
    # list to store results for each elementsize. h is computed as sqrt(S/ntri),
    # where S is the surface of the domain, ntri the number of triangle in the mesh
    he = []
    errorl2 = []
    errorh1 = []
    rconds = []
    # definition of the exact solution
    exactsol = ex.laplacianCircularInclusionQuad(k1, k2, ri=ri, re=np.sqrt(2))

    def exafun(xy):
        return exactsol.exactSolution(xy)

    def exagrad(xy):
        return exactsol.exactGradient(xy)
    if algo == 'tracking fem':
        # only for tracking fem, plot the exact solution on a given mesh.
        model_name, physical_dict = mg.disk_in_ref_square(
            length=lenght, radius=ri, relh=0.01)
        xy = meshInterface.get_vertices_coord(model_name)
        tris = meshInterface.get_triangles(model_name)
        tris_xy = xy[tris]
        gp.listPlotFieldTri(tris_xy, exagrad(np.sum(tris_xy, axis=1)/3.), P0=True,
                            viewname='GradT_exact', VectorType="Displacement", Range=[0, 0.25])
    for i in steps:
        h = h0**i
        gmsh.option.setNumber('Mesh.SurfaceEdges',
                              1 if h > 0.05 else 0)
        if algo == 'tracking fem':
            model_name, physical_dict = mg.disk_in_ref_square(
                length=lenght, radius=ri, relh=h)
        else:
            model_name, physical_dict = mg.square(
                length=lenght, relh=h)
        nv = meshInterface.nb_vertices(model_name)
        nf = meshInterface.nb_triangles(model_name)
        print('Model ', model_name,  'constructed.')
        print('nb elem : ',  nf, 'nb node :',  nv)
        print('Start solving using ', algo, 'enr = ', enr)
        lsfunc = exactsol.get_ls()[0]
        physname_to_dirichlet_value = {
            'left': exafun, 'right': exafun, 'bottom': exafun, 'top': exafun}
        if algo != 'xmesh':
            pbxfem = ts.ThermicSolver(model_name, physical_dict,
                                      [k1, k2],   physname_to_dirichlet_value, source=1.,
                                      lsfunforconductivity=lsfunc, enrich=enr, dosvd=False)
        else:
            pbxfem = ts.ThermicSolver(model_name, physical_dict,
                                      [k1, k2], physname_to_dirichlet_value, source=1.,
                                      lsfunforconductivity=lsfunc, enrich=enr,
                                      xmesh_relay=True, dosvd=False)
        u_dofs, rcondi = pbxfem.solve(solver=solver, compute_rcond=True)
        tris_xy_cut, u_tris_xy = pbxfem.postproVal(u_dofs)  # pylint: disable=[W0612]
        tris_xy_cut, gradu_tris_xy = pbxfem.postproGrad(u_dofs)
        gp.listPlotFieldTri(tris_xy_cut, gradu_tris_xy, P0=True, viewname='GradT_' +
                            algo, VectorType="Displacement", Range=[0, 0.25])
        hi = np.sqrt(4/nf)
        errorh1i = pbxfem.H1_error(u_dofs, exagrad)
        errorl2i = pbxfem.L2_error(u_dofs, exafun)
        print('End Solve')
        print(f"Results : h= {hi:.4e}, L2 error={errorl2i:.4e}, \
            H1 error={errorh1i:.4e}, rcond={rcondi:.4e}")
        he.append(hi)
        errorh1.append(errorh1i)
        errorl2.append(errorl2i)
        rconds.append(rcondi)
        gmsh.model.remove()
        model_name, physical_dict = mg.square(relh=0.55)
    return np.array(he), np.array(errorl2), np.array(errorh1), np.array(rconds)


if __name__ == '__main__':
    import pylab as plt
    from execTools import Profile
    plt.close('all')
    GMSHPOPUP = True
    Profile.doprofile = False
    gmsh.initialize()
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)
    # 'xRidge' IGFEM
    enrichment = [None, None, 'xFEMSpace', 'xRidgeSpaceP1', None]
    algorithm = ['tracking fem', 'capturing',
                 'xfem enrichment 1', 'xfem enrichment 2', 'xmesh']
    fig1, ax1 = plt.subplots(1, 1)
    ax1.set_ylabel('L2 error')
    ax1.set_xlabel('h')
    fig2, ax2 = plt.subplots(1, 1)
    ax2.set_ylabel('H1 error')
    ax2.set_xlabel('h')
    fig3, ax3 = plt.subplots(1, 1)
    ax3.set_ylabel('rcond')
    ax3.set_xlabel('h')
    for enrich, alg in zip(enrichment, algorithm):
        print('### algo : ', alg)
        ledge, errorL2, errorH1, rcond = convergence_ridge(enr=enrich, algo=alg)
        ax1.loglog(ledge, errorL2, 'o-', label=alg)
        ax2.loglog(ledge, errorH1, 'o-', label=alg)
        ax3.loglog(ledge, rcond, 'o-', label=alg)
    ax1.legend()
    ax2.legend()
    ax3.legend()
    plt.show()
    if GMSHPOPUP:
        gmsh.fltk.run()
    gmsh.finalize()
    Profile.print_stats()
