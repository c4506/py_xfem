#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23 14:05:34 2023

@author: nchevaug
"""
import gmsh
import numpy as np

#listPlotScalarNodalField
def listPlotScalarNodalField(xyz, tri2node, nodedata, viewname = "T", IntervalsType = 3, NbIso=10, Range = None, gv =None):
    if gv is None :    gv = gmsh.view.add(viewname)
    #nnode = len(xyz)
    ntri =  len(tri2node)
    #ldata = np.moveaxis(np.hstack((xy, np.zeros((nnode,1)), nodedata[:, None]))[tri2node],1, -1).flatten()
    if xyz.shape[-1] == 2 :
        z = np.zeros(xyz.shape[:-1]+(1,))
        xyz = np.concatenate((xyz,z),1)
    ldata = np.moveaxis(np.hstack((xyz, nodedata[:, None]))[tri2node],1, -1).flatten()
    gmsh.view.addListData(gv, "ST", ntri, ldata)
    gmsh.view.option.setNumber(gv,"IntervalsType",IntervalsType)
    if Range is not None :
        gmsh.view.option.setNumber(gv, "RangeType",  2)
        gmsh.view.option.setNumber(gv, "CustomMin", Range[0])
        gmsh.view.option.setNumber(gv, "CustomMax", Range[1])
    
    gmsh.view.option.setNumber(gv,"NbIso",NbIso)
    gmsh.view.option.setNumber(gv,"ShowElement",1)
    return gv
    
def listPlotScalarElementField(xyz, tri2node, elementdata, viewname = "T", IntervalsType = 3, NbIso=10, gv =None):
    if gv is None : gv = gmsh.view.add(viewname)
    #nnode = len(xy)
    ntri =  len(tri2node)
    if xyz.shape[-1] == 2 :
        z = np.zeros(xyz.shape[:-1]+(1,))
        xyz = np.concatenate((xyz,z),1)
    txyz   = xyz[tri2node]
    if np.isscalar(elementdata) : elementdata = np.broadcast_to(elementdata, ntri)
    datat  = np.repeat(elementdata,3).reshape((ntri, 3,1))
    ldata = np.moveaxis((np.concatenate((txyz,datat), axis = 2)), 1,-1).flatten()
    gmsh.view.addListData(gv, "ST", ntri, ldata)
    gmsh.view.option.setNumber(gv,"IntervalsType",IntervalsType)
    gmsh.view.option.setNumber(gv,"NbIso",NbIso)
    gmsh.view.option.setNumber(gv,"ShowElement",1)
    return gv 

def listPlotScalarField(tris_xyz, data, viewname ='T', IntervalsType = 3, NbIso = 10, Range = None, gv = None):
    if len(tris_xyz.shape) == 2 :
        tris_xyz = tris_xyz.reshape((1,) + tris_xyz.shape)
    data     = np.atleast_2d(data)
    ntri =  len(tris_xyz)
    if tris_xyz.shape[-1] == 2 :
        z = np.zeros(tris_xyz.shape[:-1]+(1,))
        tris_xyz = np.concatenate((tris_xyz,z),2)
    rtris_xyz = np.moveaxis(tris_xyz, 1,2)
    ldata = np.concatenate((rtris_xyz, data.reshape(-1,1,3)),1).flatten()
    if gv is None : gv = gmsh.view.add(viewname)
    gmsh.view.addListData(gv, "ST", ntri, ldata)
    gmsh.view.option.setNumber(gv,"IntervalsType", IntervalsType)
    gmsh.view.option.setNumber(gv,"NbIso",NbIso)
    gmsh.view.option.setNumber(gv,"ShowElement", 0 if ntri > 10000 else 1)
    if Range is not None :
        gmsh.view.option.setNumber(gv, "RangeType",  2)
        gmsh.view.option.setNumber(gv, "CustomMin", Range[0])
        gmsh.view.option.setNumber(gv, "CustomMax", Range[1])
    return gv
    
def listPlotVectorField(tris_xyz, vdata, viewname ='T', IntervalsType = 3, NbIso = 10,  Range = None, norm = False, gv =None):
    if len(tris_xyz.shape) == 2 :
        tris_xyz = tris_xyz.reshape((1,) + tris_xyz.shape)
    vdata     = np.atleast_2d(vdata)
    ntri =  len(tris_xyz)
    if tris_xyz.shape[-1] == 2 :
        z = np.zeros(tris_xyz.shape[:-1]+(1,))
        tris_xyz = np.concatenate((tris_xyz,z),2)
    if vdata.shape[-1] ==2:
        z = np.zeros(vdata.shape[:-1]+(1,))
        vdata = np.concatenate((vdata,z),2)  
    rtris_xyz = np.moveaxis(tris_xyz, 1,2)
    ldata = np.concatenate((rtris_xyz, vdata.reshape(-1,3,3)),1).flatten()
    if gv is None  : gv = gmsh.view.add(viewname)
    gmsh.view.addListData(gv, "VT", ntri, ldata)
    gmsh.view.option.setNumber(gv,"IntervalsType", IntervalsType)
    gmsh.view.option.setNumber(gv,"NbIso",NbIso)
    gmsh.view.option.setNumber(gv,"ShowElement", 0 if ntri > 10000 else 1)
    if norm :
        gmsh.view.option.setNumber(gv,"DisplacementFactor", 0)
        gmsh.view.option.setNumber(gv,"VectorType",  5)
    if Range is not None :
        gmsh.view.option.setNumber(gv, "RangeType",  2)
        gmsh.view.option.setNumber(gv, "CustomMin", Range[0])
        gmsh.view.option.setNumber(gv, "CustomMax", Range[1])
        
        
    return gv

def listPlotElementScalarField(tris_xyz, data, viewname ='T', IntervalsType = 3, NbIso = 10, Range = None,  gv = None):
    data = np.repeat(data.reshape((-1,1)), 3, axis=0)
    return listPlotScalarField(tris_xyz, data, viewname = viewname, IntervalsType = IntervalsType , NbIso = NbIso, Range = Range, gv = gv)
    
def listPlotElementVectorField(tris_xyz, data, viewname ='T', IntervalsType = 3, NbIso = 10, Range = None, norm = False, gv = None):
    data = np.repeat(data, 3, axis=0).reshape((-1,3, data.shape[1]))
    return listPlotVectorField(tris_xyz, data, viewname = viewname, IntervalsType = IntervalsType,  NbIso = NbIso, Range = Range, norm = norm, gv = gv)


def listPlotLineScalar(line_xyz, data, viewname = 'T', gv = None):
    if gv is None : gv = gmsh.view.add(viewname)
    if line_xyz.shape[-1] == 2 :
        z = np.zeros(line_xyz.shape[:-1]+(1,))
        line_xyz = np.concatenate((line_xyz,z),2)
    rline_xyz = np.moveaxis(line_xyz, 1,2)
    ldata = np.concatenate((rline_xyz, data.reshape(-1,1, 2)),1).flatten()
    gmsh.view.addListData(gv, "SL", len(data), ldata)
    gmsh.view.option.setNumber(gv,"LineType",1)
    return gv 

def listPlotPointScalar(xyz, data, viewname = 'T', gv = None):
    if xyz.shape[-1] == 2 :
        z = np.zeros(xyz.shape[:-1]+(1,))
        xyz = np.concatenate((xyz,z),1)
    ldata = np.concatenate((xyz, data.reshape(-1,1)),1).flatten()
    if gv is None : gv = gmsh.view.add(viewname)
    gmsh.view.addListData(gv, "SP", len(data), ldata)
    gmsh.view.option.setNumber(gv,"PointType",1)
    return gv
  
    
if __name__ == '__main__':
    
    gmsh.initialize()
##    xyz = np.array([[0.,0.,0.],[1.,0.,0.],[1.,1.,0.],[0.,1.,0.]])
##    tris = np.array([[0,1,2], [0,2,3]])
##    sidee = np.array( [-1,1])
##    listPlotScalarElementField(xyz, tris, [-1,1], viewname = "T", IntervalsType = 3, NbIso=10)
##    
##    
    listPlotScalarField(np.array([[[0.,0.,0.], [1.,0.,0.], [0.,1.,0.]], [[1,0.,0.],[2,0.,0.],[1.,1.,0.]]]), np.array([[1,2,3], [4,5,6]]), viewname ='Test_list', IntervalsType = 3, NbIso = 7)
#    
    gmsh.fltk.run()
    gmsh.finalize()