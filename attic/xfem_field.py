#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 23:30:08 2023

@author: nchevaug
"""
import numpy as np
import levelSetMeshCut as lscut
import gmsh_post as gp
import mesh_adjacencies as ma
import mapping

 
class FEMSpaceScalarP1():
    _dNdu = np.array([[-1.,1.,0.],[-1.,0.,1.]])
    def N(uv) :
        u = uv[...,0]
        v = uv[...,1]
        return np.stack( (1.-u-v, u, v), axis = len(u.shape) )
    
    def Ni(i, uv) :
        if i == 0 :  return (1.-uv[...,0]-uv[...,0])
        if i == 1 :  return uv[..., 0]
        if i == 2 :  return uv[..., 1]
        
    def dNdu(uv) :
        #    return np.array([ scalarField_p1._dNdu ]*len(uv))
        return np.broadcast_to( FEMSpaceScalarP1._dNdu, (len(uv),) + (2,3))
     
    def dNdx(uv, tri_xy):
        map_etri = mapping.t3map_old(tri_xy)
        return np.einsum('ijk, jm-> imk', FEMSpaceScalarP1.dNdu(uv), map_etri.invF())
              
    def __init__(self, name, xy, tris):
        self.name  = name
        self.xy      = xy
        self.tris    = np.atleast_2d(tris)
        self.tris_xy = self.xy[self.tris]
        self.vid   = np.unique(tris.flatten())
        
        self.dofid = np.arange(len(self.vid))
        self.vid2dofid = { vid : dofid  for (dofid, vid) in enumerate(self.vid) }
        
    
    def interpolate(self, f):
        fv = f(self.xy)
        return  fv[self.vid]
    
    def evalFunction(self, key, evaltrisuv, appro2evals_xy=None):
        vid = self.vid[key]
        support = getattr(self, 'support', None)
        if support is None : self.support = ma.nodes2triangles(self.tris)
        supportvid = self.support[vid]
        xy = []
        N = []
        for ia in  supportvid :
            #iN = np.where(self.tris[ia] == vid)[0]
            Nuv   = FEMSpaceScalarP1.N(evaltrisuv)
            a_xy  = self.tris_xy[ia]
            uv_xy = Nuv.dot(a_xy)
            xy.append(uv_xy)
            asi = np.where(self.tris[ia] == vid)[0]
            N.append(Nuv[asi])
#            print('ga')
#            print(uv_xy)
#            print(Nuv[asi])
        return xy, N            
            
    def evalOp( self, evaltrisuv, evaltris_xy= None, eval2approtris = None, evaltrisdata =None ):
        approtris_xy = self.tris_xy
        N    = []
        dofs = []
        if evaltris_xy is None : 
            for iatri, (atri_xy, eintegrator) in enumerate(zip (approtris_xy, evaltrisuv)) :
                a_uv = eintegrator
                N_a_uv = FEMSpaceScalarP1.N(a_uv)
                vids = self.tris[iatri]
                e_dofs = np.array([self.vid2dofid[vid] for vid in vids])
                N.append(N_a_uv)
                dofs.append(e_dofs)
            return {'N': N, 'dofs' : dofs} 
        if len(evaltris_xy.shape) !=3 :
            evaltris_xy = evaltris_xy.reshape((1,3,2))
        for (iatri, eintegrator, etri_xy) in zip (eval2approtris, evaltrisuv, evaltris_xy) :
            atri_xy = approtris_xy[iatri]
            a_uv = mapping.t3map_old.e_uv_to_a_uv(etri_xy, atri_xy, eintegrator.uv)
            N_a_uv = FEMSpaceScalarP1.N(a_uv)
            vids = self.tris[iatri]
            e_dofs = np.array([self.vid2dofid[vid] for vid in vids])
            N.append(N_a_uv)
            dofs.append(e_dofs)
        return {'N': N, 'dofs' : dofs}

    def evalGradOp( self, evaltrisuv, evaltris_xy= None, eval2approtris = None, evaltrisdata =None ):
        approtris_xy = self.tris_xy
        dNdx    = []
        dofs = []
        if evaltris_xy is None : 
            for iatri, (atri_xy, eintegrator) in enumerate(zip (approtris_xy, evaltrisuv)) :
                a_uv = eintegrator.uv
                dN_a_uv_dx = FEMSpaceScalarP1.dNdx(a_uv, atri_xy)
                vids = self.tris[iatri]
                e_dofs = np.array([self.vid2dofid[vid] for vid in vids])
                dNdx.append(dN_a_uv_dx)
                dofs.append(e_dofs)
            return {'dNdx': dNdx, 'dofs' : dofs} 
        if len(evaltris_xy.shape) !=3 :
            evaltris_xy = evaltris_xy.reshape((1,3,2))
        for (iatri, eintegrator, etri_xy) in zip (eval2approtris, evaltrisuv, evaltris_xy) :
            atri_xy = approtris_xy[iatri]
            a_uv = mapping.t3map_old.e_uv_to_a_uv(etri_xy, atri_xy, eintegrator.uv)
            N_a_uv = FEMSpaceScalarP1.N(a_uv)
            vids = self.tris[iatri]
            e_dofs = np.array([self.vid2dofid[vid] for vid in vids])
            dNdx.append(N_a_uv)
            dofs.append(e_dofs)
        return {'N': N, 'dofs' : dofs}
    
def plotEnrichmentValue(enrichment, viewname=None):
    if viewname is None : viewname= enrichment.name+"_value"
    evaltri_uv = T3_nodespoints().uv
    data = np.array( [enrichment.evalAtIntegPoint(eval_tri_id, enrichment.eval2appro[eval_tri_id], evaltri_uv) \
                     for eval_tri_id in range(len(enrichment.eval_tris))])
    gp.listPlotScalarField(enrichment.eval_xy[enrichment.eval_tris], data, viewname = viewname)
        
def plotEnrichmentGradient(enrichment, viewname=None):
    if viewname is None : viewname= enrichment.name+"_gradient"
    evaltri_uv = T3_gausspoints(0).uv
    data = np.array( [enrichment.devaldxy_AtIntegPoint(eval_tri_id, enrichment.eval2appro[eval_tri_id], evaltri_uv)  \
                      for eval_tri_id in range(len(enrichment.eval_tris))]).reshape((-1,2))
    gp.listPlotElementVectorField(enrichment.eval_xy[enrichment.eval_tris], data, viewname = viewname)   

class heaviside_enrichment():
    def __init__(self, name, eval_xy, eval_tris, eval2appro, appro_tris, phi_at_eval_vertices):
        self.name = name
        self.eval_xy = eval_xy
        self.eval_tris = eval_tris
        self.appro_tris = appro_tris
        self.eval2appro = eval2appro
        self.H_at_eval_tris  =  np.where(np.max(phi_at_eval_vertices[eval_tris],1) <= 0., 1. , 0.)
        
    def evalAtIntegPoint(self, eval_tri_id, appro_tri_id, evaltri_uv):
        return np.broadcast_to(self.H_at_eval_tris[eval_tri_id], len(evaltri_uv) )
    
    def devalduv_AtIntegPoint(self, eval_tri_id, appro_tri_id, evaltri_uv):
        return np.zeros((len(evaltri_uv),2)) 
        
    def devaldxy_AtIntegPoint(self, eval_tri_id, appro_tri_id, evaltri_uv):
        return np.zeros((len(evaltri_uv),2)) 
                
    def evalOp(self, evaltrisuv, evaltris_xy, eval2approtris, side):
        N    = []
        dofs = []
        xy   = []
        for ietri, etri_xy in enumerate(evaltris_xy) :
           e_uv = evaltrisuv[ietri].uv
           map_etri = mapping.t3map_old(etri_xy)
           e_xy = map_etri.uv2xy(e_uv)
           if side[ietri] == -1 :
               N_a_uv = np.array([0.]*len(e_uv)).reshape((-1,1))
           else :
               N_a_uv = np.array([1.]*len(e_uv)).reshape((-1,1)) #e_xy[:,0].reshape(-1,1)
           e_dofs = np.array([0])
           N.append(N_a_uv)
           dofs.append(e_dofs)
           xy.append(e_xy)
        return {'xy': xy,  'N': N, 'dofs' : dofs}
    
class ridge_enrichment():
    def __init__(self, name, eval_xy, eval_tris, eval2appro, appro_tris, phi_at_eval_vertices):
        self.name = name
        self.eval_xy = eval_xy
        self.eval_tris = eval_tris
        self.appro_tris = appro_tris
        self.phi = phi_at_eval_vertices
        self.eval2appro = eval2appro
    
    def evalAtIntegPoint(self, eval_tri_id, appro_tri_id, evaltri_uv):
        if appro_tri_id == eval_tri_id : return np.zeros(len(evaltri_uv)) 
        eval_tri = self.eval_tris[eval_tri_id]
        appro_tri = self.appro_tris[ appro_tri_id]
        approtri_uv = mapping.t3map_old.e_uv_to_a_uv(self.eval_xy[eval_tri], self.eval_xy[appro_tri], evaltri_uv)
        return ridge_enrichment.evalAtIntegPoint_(self.phi[eval_tri], self.phi[appro_tri], evaltri_uv, approtri_uv)
        
    def evalAtIntegPoint_(phi_eval_tri_vertices, phi_appro_tri_vertices, evaltri_uv, approtri_uv):
        eu = evaltri_uv[:,0]
        ev = evaltri_uv[:,1]
        au = approtri_uv[:,0]
        av = approtri_uv[:,1]
        abs_phi_appro_tri_vertices = np.abs(phi_appro_tri_vertices)
        abs_phi_eval_tri_vertices  = np.abs(phi_eval_tri_vertices)
        ridge =  (1-au-av)*abs_phi_appro_tri_vertices[0] + au*abs_phi_appro_tri_vertices[1] + av*abs_phi_appro_tri_vertices[2] \
                -(1-eu-ev)*abs_phi_eval_tri_vertices[0]  - eu*abs_phi_eval_tri_vertices[1]  - ev*abs_phi_eval_tri_vertices[2]   
        return ridge
    
    def devalduv_AtIntegPoint(self, eval_tri_id, appro_tri_id, evaltri_uv):
        if appro_tri_id == eval_tri_id : return np.zeros((len(evaltri_uv),2)) 
        eval_tri = self.eval_tris[eval_tri_id]
        appro_tri = self.appro_tris[ appro_tri_id]
        map_a2x = mapping.t3map_old(self.eval_xy[appro_tri])
        map_e2x = mapping.t3map_old(self.eval_xy[eval_tri])
        Fappro2eval = map_e2x.invF.dot(map_a2x.F())
        dridgeduv = ridge_enrichment.devalduv_appro_AtIntegPoint_(self.phi[eval_tri], self.phi[appro_tri], Fappro2eval)
        return np.broadcast_to(dridgeduv, (len(evaltri_uv), 2))
    
    def devaldxy_AtIntegPoint(self, eval_tri_id, appro_tri_id, evaltri_uv):
        if appro_tri_id == eval_tri_id : return np.zeros((len(evaltri_uv),2)) 
        eval_tri = self.eval_tris[eval_tri_id]
        appro_tri = self.appro_tris[ appro_tri_id]
        map_a2x = mapping.t3map_old(self.eval_xy[appro_tri])
        Fa2x = map_a2x.F()
        Fx2a = map_a2x.invF()
        Fx2e = mapping.t3map_old(self.eval_xy[eval_tri]).invF()
        Fappro2eval = Fx2e.dot(Fa2x)
        dridgeduv = ridge_enrichment.devalduv_appro_AtIntegPoint_(self.phi[eval_tri], self.phi[appro_tri], Fappro2eval)
        dridgedxy  = dridgeduv.dot(Fx2a)
        return np.broadcast_to(dridgedxy, (len(evaltri_uv), 2))
    
    def devalduv_appro_AtIntegPoint_(phi_eval_tri_vertices, phi_appro_tri_vertices, Fappro2eval):
        abs_phi_appro_tri_vertices = np.abs(phi_appro_tri_vertices)
        abs_phi_eval_tri_vertices =  np.abs(phi_eval_tri_vertices)
        dridge_appro_part =  abs_phi_appro_tri_vertices[1:] - abs_phi_appro_tri_vertices[0]
        dridge_eval_part  = -(abs_phi_eval_tri_vertices[1:]  - abs_phi_eval_tri_vertices[0]).dot(Fappro2eval)
        return dridge_appro_part+dridge_eval_part

#class XFEMSpaceScalarP1():
class xfspace_heaviside():
    def __init__(self, name, appro_xy, appro_tris, ls_at_appro_nodes, enrichment_function  = heaviside_enrichment):
        self.name  = name
        self.appro_xy      = appro_xy
        self.appro_tris    = np.atleast_2d(appro_tris)
        self.appro_tris_xy = self.appro_xy[self.appro_tris]
        self.appro_vertices_id   = np.unique(appro_tris.flatten())
        self.vid2vindex = {vid : vindex for vindex, vid in enumerate(self.appro_vertices_id) }
        self.eval_xy, self.eval_phi, self.eval_tris, self.eval_sides, self.eval2appro \
            = lscut.levelSetIsoCut(appro_xy, appro_tris, ls_at_appro_nodes, absfittol = 0., relfittol = 0.02, returnparent = True)
        
        self.appro2evals = np.array([[] for i in range(len(self.appro_tris))], dtype = object)
        
        for ii, ia in enumerate(self.eval2appro) : self.appro2evals[ia] += [ii]
            
        min_support = +np.inf*np.ones(len(self.vid2vindex))
        max_support = -np.inf*np.ones(len(self.vid2vindex))
        for tri in self.appro_tris :
            for vid in tri :
                phitri = self.eval_phi[tri]
                min_support[self.vid2vindex[vid]] = min(min_support[self.vid2vindex[vid]], np.min(phitri))
                max_support[self.vid2vindex[vid]] = max(max_support[self.vid2vindex[vid]], np.max(phitri))
        
        enriched_vertex = []
        for index, (min_support_i, max_support_i) in enumerate(zip(min_support,max_support)):
            print('minmw', min_support_i, max_support_i)
            if min_support_i <0 and max_support_i > 0 : enriched_vertex = enriched_vertex + [self.vid2vindex[index]]
                
        self.enriched_vertex = enriched_vertex
        self.isenriched_vertex  = np.zeros(len(self.appro_xy) , dtype = bool)
        self.isenriched_vertex[self.enriched_vertex] = True
        self.enrichment = enrichment_function(self.name+"enrichment", self.eval_xy, self.eval_tris, self.eval2appro, self.appro_tris, self.eval_phi)
        
        
    

    def gmsh_plot_enriched_vertices(self, viewname = "Heaviside Enriched Vertices"):
        gp.listPlotPointScalar(self.appro_xy[self.enriched_vertex], np.ones(len(self.enriched_vertex)), viewname = viewname )
        
    def gmsh_plot_enrichment_function(self):
        plotEnrichmentValue(self.enrichment, self.name+'_enrichment')
    
    
        
#        self.dofid = np.arange(len(self.vid))
#        self.vid2dofid = { vid : dofid  for (dofid, vid) in enumerate(self.vid) })
        
class integration_points():
    def __init__(self, w, uv):
        self.w = w
        self.uv = uv

class T3_gausspoints():
    def __init__(self, order=0):
        if order in [0,1] :
            self.w = np.array([0.5])
            self.uv = np.array([[1./3., 1./3.]])
            return
        print('Error : T3_gausspoints are only defined up to order 1')
        raise
        
class T3_nodespoints():
    def __init__(self):
        self.w = np.array([0.5/3.]*3)
        self.uv = np.array([[0.,0.], [1.,0.], [0.,1.]])
        
if __name__== '__main__' :   
    
    import gmsh
    import gmsh_meshes_examples as gmshmesh
    
    gmsh.initialize()             
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)
    
    modelname, physical_dict = gmshmesh.generate_L_in3parts(0.2)
    appro_xy      = gmsh.model.mesh.getNodes()[1].reshape((-1,3))[:, :-1]
    appro_tris    = gmsh.model.mesh.getElementsByType(2)[1].reshape((-1,3))-1
    appro_tris_xy = appro_xy[appro_tris]
    
    #xyz, phi, tris, side, parent = lscut.levelSetIsoCut(xyz_appro, tris_appro, phi1(xyz_appro), absfittol = 0., relfittol = 0.002, returnparent = True)
    #gp.listPlotScalarNodalField(xyz, tris, phi, viewname = "isocut_ls", IntervalsType = 3, NbIso=10)
    #gp.listPlotScalarElementField(xyz, tris, side, viewname = "isocut_side", IntervalsType = 3, NbIso=10)
     
    Tfield    = FEMSpaceScalarP1("Temperature", appro_xy, appro_tris)
    
    class somefield():
        def fun(xy) : return xy[:,1]*np.cos(2*np.pi*xy[:,0])
        def grad(xy) : 
            gx = -xy[:,1]*2*np.pi*np.sin(2*np.pi*xy[:,0])
            gy =      np.cos(2*np.pi*xy[:,0])
            return np.stack((gx,gy),1)
        
    class linearfield():
        def fun(xy) : return 3*xy[:,0]+4*xy[:,1]
        def grad(xy) : 
            gx = 3*np.ones(len(xy))
            gy = 4*np.ones(len(xy))
            return np.stack((gx,gy),1)
                
    Tfun      = lambda xy : somefield.fun(xy)
    Tgrad     = lambda xy : somefield.grad(xy)
    
    Th = Tfield.interpolate(Tfun)
    evalpointatcorner = T3_nodespoints().uv
    evalOp_OnMesh = Tfield.evalOp( [evalpointatcorner]*len(appro_tris)) 
    e_tris_T2 = np.array( [N.dot(Th[dofs]) for dofs, N in zip(evalOp_OnMesh['dofs'], evalOp_OnMesh['N'])])
    gp.listPlotScalarField(appro_tris_xy, e_tris_T2, NbIso = 10, viewname = 'evalOnMesh')
    
    
    
    #gp.listPlotScalarNodalField(xyz_appro, tris_appro, Tfun(xyz_appro), viewname = "T_on_mesh_nodes",  NbIso=10)
    #gp.listPlotElementVectorField(a_tris_xy, Tgrad(a_cog), viewname ='gradT', IntervalsType = 3, NbIso = 3)
   
#    xyz = np.array([[1.3,0.,0.], [2.,0.,0.], [1.,1.,0.]])
#    tris = np.array([[0,1,2]])
#    Tfield    = FEMSpaceScalarP1("T", xyz[:,:-1], tris)
#    Tfun      = lambda xy : (xy[:,0]**2 + xy[:,1]**2)
#    Th = Tfield.interpolate(Tfun)
#    Th = np.array([0.,0., 1.])
#    
#    evalpointatcorner = T3_nodespoints()
#    e_xyz = np.array([[1.3,0.,0.], [2.,0.,0.], [1.,1,0.], [1.65,0.,0.]])
#    e_tris = np.array([[0,3,2], [1,2,3]])
#    e_tris_xy = e_xyz[e_tris][...,:-1]
#    
#       
#    #evalOp_OnCutMesh = Tfield.evalOp( [evalpointatcorner]*2)
#    evalOp_OnCutMesh = Tfield.evalOp( [evalpointatcorner]*2, e_tris_xy, [0]*2)
#       
#    e_tris_T_OnCutMesh = np.array( [N.dot(Th[dofs]) for dofs, N in zip(evalOp_OnCutMesh['dofs'], evalOp_OnCutMesh['N'])])
#       
#    gp.listPlotScalarField(e_tris_xy, e_tris_T_OnCutMesh, NbIso = 10, viewname = 'evalOnCutMesh')
#       
#    #evalOp_OnMesh = Tfield.evalOp( [evalpointatcorner], xyz[:, :-1], [0])
#    
#       
#    
#       
#       
#    Tfield.evalFunction(0, T3_nodespoints().uv)
#       


#import numpy as np
#import xfem_field as xf
#
#def phi(xy):
#    x = np.atleast_2d(xy)[:,0]
#    y = np.atleast_2d(xy)[:,1]
#    #return np.sqrt((x-0.5)**2+y**2 )- 0.7
#    return x -0.5
#
#hsp = xf.xfspace_heaviside("T", pb.xy, pb.tris, phi(pb.xy))
##gp.listPlotScalarNodalField(hsp.eval_xy, hsp.eval_tris, hsp.eval_phi, viewname = "isocut_ls", IntervalsType = 3, NbIso=10)
##gp.listPlotScalarElementField(hsp.eval_xy, hsp.eval_tris, hsp.eval2appro, viewname = "isocut_ls", IntervalsType = 3, NbIso=10)
#hsp.gmsh_plot_enriched_vertices()
#hsp.gmsh_plot_enrichment_function()
#
#xyi, Ni = pb.Tfield.evalFunction(32, xf.T3_nodespoints().uv, appro2evals_xy=None)
#
##gp.listPlotPointScalar(hsp.xy[hsp.enriched_vertex], np.ones(len(hsp.enriched_vertex)) )
##gv = gmsh.view.add("testpoint")
##gmsh.view.addListData(gv, "SP", 1, [0.22,0.22,0.,1.])
#gmsh.fltk.run()
#gmsh.finalize()     
#            
    gmsh.fltk.run()
    gmsh.finalize()
      
    


        