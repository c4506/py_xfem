#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 23:30:08 2023

@author: nchevaug
"""
import numpy as np
import levelSetMeshCut as lscut
import gmsh_post as gp
import mesh_adjacencies as ma
import mapping
import scipy as sp
import quadrature as quad

class FEMSpaceScalarP1():
    _dNdu = np.array([[-1.,1.,0.],[-1.,0.,1.]])
    lenN  = 3
    def N(uv) :
        u = uv[...,0]
        v = uv[...,1]
        return np.stack( (1.-u-v, u, v), axis = len(u.shape) )
    
    def Ni(i, uv) :
        if i == 0 :  return (1.-uv[...,0]-uv[...,1])
        if i == 1 :  return uv[..., 0]
        if i == 2 :  return uv[..., 1]
        
    def dNdu(uv) :
        return np.broadcast_to( FEMSpaceScalarP1._dNdu, (uv.shape[:-1]) + (2,3))
               
    def __init__(self, name, xy, tris, tris_map = None):
        self.name  = name
        self.xy      = xy
        self.tris    = np.atleast_2d(tris)
        if tris_map is None : tris_map = mapping.t3map(self.xy[self.tris])
        self.tris_map =tris_map
        self.vid   = np.unique(tris.flatten()) 
        self.dofid = np.arange(len(self.vid))
        if len(self.vid) != len(self.xy) :
            #self.vid2dofid = { vid : dofid  for (dofid, vid) in enumerate(self.vid) }
            self.vid2dofid =  dict(zip( self.vid , np.arange(len(self.vid)) ))
            self.a2dofs    =  np.array([ [self.vid2dofid[ vid ]  for vid in t] for t in self.tris])
        else :
            self.vid2dofid = np.arange(len(self.vid))
            self.a2dofs    = self.vid2dofid[self.tris]
            
    
    def size(self): return len(self.vid)
    
    def interpolate(self, f):
        fv = f(self.xy)
        return  fv[self.vid]
    
    def dNdx(self, points_uv, elem_ids = None):
        if elem_ids is None :
            invFs = self.tris_map.invF
        else:
            invFs = self.tris_map.invF[elem_ids]
        dNdu = FEMSpaceScalarP1._dNdu
        dNdx = np.einsum ( '...ij,...ik->...jk', invFs, dNdu)
        #map is constant and DNdU is constant. repeat the value as much as the number of points per elem 
        return np.repeat(dNdx[:, np.newaxis, :, :], points_uv.shape[1], axis =1)
        
    def evalFunction(self, key, evaltrisuv, appro2evals=None):
        vid = self.vid[key]
        supports = getattr(self, 'supports', None)
        if supports is None : self.supports = ma.nodes2triangles(self.tris)
        support_appro_tris_id = list(self.supports[vid])
        
        #xy = []
        if appro2evals is None :
            Nuv   = FEMSpaceScalarP1.N(evaltrisuv)
            N = np.array([ Nuv[np.where(self.tris[ia] == vid)[0] ]  for ia in support_appro_tris_id]).squeeze()
            return support_appro_tris_id, N
        
        all_support_eval_tris_ids = []
        all_N = []
        for ia in support_appro_tris_id :
            amap = mapping.t3map_old(self.tris_xy[ia])
            support_eval_tris_id = appro2evals.approId2EvalIds[ia]
            all_support_eval_tris_ids = all_support_eval_tris_ids + [support_eval_tris_id]
            iN = np.where(self.tris[ia] == vid)[0]
            for ie in support_eval_tris_id :
                ie_v = appro2evals.eval_tris[ie]
                emap = mapping.t3map_old(appro2evals.eval_xy[ie_v])
                a_uv = amap.xy2uv(emap.uv2xy(evaltrisuv))
                Ni_a_uv   =  FEMSpaceScalarP1.Ni(iN, a_uv)
                all_N = all_N + [Ni_a_uv]
        return support_appro_tris_id, all_support_eval_tris_ids, all_N

    def evalOp( self, evaltrisuv, return_evaltrisxy = False ):
        N_a_uv  = np.broadcast_to(FEMSpaceScalarP1.N(evaltrisuv), (len(self.tris), len(evaltrisuv), FEMSpaceScalarP1.lenN))
        npts = len(self.tris) * len(evaltrisuv)
        N = sp.sparse.csr_array((N_a_uv.flatten(), 
                                 np.repeat(self.a2dofs, len(evaltrisuv), axis=0).flatten(),
                                 np.arange(0, (npts+1)*FEMSpaceScalarP1.lenN, FEMSpaceScalarP1.lenN) ),
                                 shape =(npts, len(self.vid)) )
        if return_evaltrisxy:
            return N, self.tris_map.xy
        return N        
    
    def evalOpOnSons_old( self, evaltrisuv, index_parent2sons, sons, sons_mapping,  return_evaltrisxy = False ):
        indextype           = index_parent2sons.dtype
        npt_per_eval        = len(evaltrisuv)
        appro2nbsons        = index_parent2sons[1:] - index_parent2sons[:-1]
        approEQeval         = np.where(appro2nbsons == 0)[0] # elements for which eval element is the appro element (appro as no sons)
        approNEQeval        = np.where(appro2nbsons > 0)[0] # elements for which eval element is the appro element (appro as no sons)
        appro2nbevals       = np.where(appro2nbsons == 0, 1, appro2nbsons)
        appro2evals_index = np.zeros(len(appro2nbevals)+1, dtype = indextype)
        np.cumsum(appro2nbevals, dtype = indextype, out = appro2evals_index[1:])
        sons_point_xy            =  sons_mapping.uv2xy(evaltrisuv)[sons]
        sons_point_on_parents_uv = self.tris_map.xy2uv(sons_point_xy, elem_ids = np.repeat(approNEQeval,  appro2nbsons[approNEQeval]))
        nbevals = appro2evals_index[-1]
        parent2eval_index = appro2evals_index[approEQeval ]
        son2eval_index =  np.setdiff1d(np.arange(nbevals), parent2eval_index)
        npts   = nbevals * npt_per_eval
        
        
        N = np.zeros( (nbevals, npt_per_eval, FEMSpaceScalarP1.lenN) )
        N[ parent2eval_index ] = FEMSpaceScalarP1.N(evaltrisuv)
        N[ son2eval_index ] = FEMSpaceScalarP1.N(sons_point_on_parents_uv)
        N = sp.sparse.csr_array((N.flatten(),
                                 np.repeat(self.a2dofs, appro2nbevals*npt_per_eval, axis=0).flatten(), 
                                 np.arange(0, (npts+1)*FEMSpaceScalarP1.lenN, FEMSpaceScalarP1.lenN)),
                                 shape=(npts, len(self.vid)))  
        if return_evaltrisxy :
                xytris = np.zeros( (nbevals,) + (3,2) )
                xytris[ parent2eval_index ] = self.tris_map.xy[approEQeval]
                xytris[ son2eval_index    ] = sons_mapping.xy[sons]
                return N,  xytris
        return N
    
    def evalOpOnSons( self, evaltrisuv, index_parent2sons, sons, sons_mapping,  return_evaltrisxy = False ):
        rmap = mapping.remapHelper(index_parent2sons, sons, self.tris_map, sons_mapping)
        npt_per_eval        = len(evaltrisuv)        
        npts   = rmap.nbevals * npt_per_eval
        sons_point_on_parents_uv = rmap.remapOnParent(evaltrisuv)
        N = np.zeros( (rmap.nbevals, npt_per_eval, FEMSpaceScalarP1.lenN) )
        N[ rmap.parent2eval_index ] = FEMSpaceScalarP1.N(evaltrisuv)
        N[ rmap.son2eval_index ]   = FEMSpaceScalarP1.N(sons_point_on_parents_uv)
        N = sp.sparse.csr_array((N.flatten(),
                                 np.repeat(self.a2dofs, rmap.appro2nbevals*npt_per_eval, axis=0).flatten(), 
                                 np.arange(0, (npts+1)*FEMSpaceScalarP1.lenN, FEMSpaceScalarP1.lenN)),
                                 shape=(npts, len(self.vid)))  
        if return_evaltrisxy : return N, rmap.getEvalTris()
        return N
    
    def evalGradOp( self, evaltrisuv, return_evaltrisxy = False ):
        dim = 2
        npts = len(self.tris) * len(evaltrisuv)
        points_uv = np.zeros((len(self.tris), len(evaltrisuv), dim ))
        # gradient is constant by element, and independent of uv in our case -
        # > no need to really eval at each point, only the shape of point_uv is needed
        dNdx  =  self.dNdx(points_uv)
        dNdx = sp.sparse.csr_array((dNdx.flatten(), 
                                    np.repeat(self.a2dofs, len(evaltrisuv)*dim, axis=0).flatten(), 
                                    np.arange(0, (dim*npts+1)*FEMSpaceScalarP1.lenN, FEMSpaceScalarP1.lenN) ),
                                    shape =(dim*npts, len(self.vid)) )
        if return_evaltrisxy:
            return dNdx, self.tris_map.xy
        return dNdx
  
    def evalGradOpOnSons( self, evaltrisuv, index_parent2sons, sons, sons_mapping,  return_evaltrisxy = False ):
        dim = 2
        indextype             = index_parent2sons.dtype
        npt_per_eval          = len(evaltrisuv)
        appro2nbsons          = index_parent2sons[1:] - index_parent2sons[:-1]
        approEQeval           = np.where(appro2nbsons == 0)[0] # elements for which eval element is the appro element (appro as no sons)
        approNEQeval          = np.where(appro2nbsons > 0)[0] # elements for which eval element is the appro element (appro as no sons)
        appro2nbevals         = np.where(appro2nbsons == 0, 1, appro2nbsons)
        appro2evals_index     = np.zeros(len(appro2nbevals)+1, dtype = indextype)
        np.cumsum(appro2nbevals, dtype = indextype, out = appro2evals_index[1:])
        sons_point_xy         =  sons_mapping.uv2xy(evaltrisuv)[sons]
        son2parent_elem_ids = np.repeat(approNEQeval,  appro2nbsons[approNEQeval])
        sons_point_on_parents_uv = self.tris_map.xy2uv(sons_point_xy, elem_ids = son2parent_elem_ids)
        nbevals = appro2evals_index[-1]
        parent2eval_index = appro2evals_index[approEQeval ]
        son2eval_index =  np.setdiff1d(np.arange(nbevals), parent2eval_index)
        npts   = nbevals * npt_per_eval
        dNdx = np.zeros( (nbevals, npt_per_eval, dim, FEMSpaceScalarP1.lenN) )
        parent_points_uv = np.zeros((len(approEQeval), len(evaltrisuv), dim ))
        # gradient is constant by element, and independent of uv in our case -
        # > no need to really eval at each point, only the shape of point_uv is needed
        dNdx[ parent2eval_index ] = self.dNdx(parent_points_uv, elem_ids = approEQeval)
        dNdx[ son2eval_index ]    = self.dNdx(sons_point_on_parents_uv, elem_ids = son2parent_elem_ids)
        dNdx = sp.sparse.csr_array((dNdx.flatten(),
                                 np.repeat(self.a2dofs, appro2nbevals*npt_per_eval*dim, axis=0).flatten(), 
                                 np.arange(0, (dim*npts+1)*FEMSpaceScalarP1.lenN, FEMSpaceScalarP1.lenN)),
                                 shape=(dim*npts, len(self.vid)))
        if return_evaltrisxy :
                xytris = np.zeros( (nbevals,) + (3,2) )
                xytris[ parent2eval_index ] = self.tris_map.xy[approEQeval]
                xytris[ son2eval_index    ] = sons_mapping.xy[sons]
                return dNdx, xytris
        return dNdx
    
def plotEnrichmentValue(enrichment, viewname=None):
    if viewname is None : viewname= enrichment.name+"_value"
    evaltri_uv = quad.T3_nodes().uv
    data = np.array( [enrichment.evalAtIntegPoint(eval_tri_id, enrichment.eval2appro[eval_tri_id], evaltri_uv) \
                     for eval_tri_id in range(len(enrichment.eval_tris))])
    gp.listPlotScalarField(enrichment.eval_xy[enrichment.eval_tris], data, viewname = viewname)
        
def plotEnrichmentGradient(enrichment, viewname=None):
    if viewname is None : viewname= enrichment.name+"_gradient"
    evaltri_uv = quad.T3_gauss(0).uv
    data = np.array( [enrichment.devaldxy_AtIntegPoint(eval_tri_id, enrichment.eval2appro[eval_tri_id], evaltri_uv)  \
                      for eval_tri_id in range(len(enrichment.eval_tris))]).reshape((-1,2))
    gp.listPlotElementVectorField(enrichment.eval_xy[enrichment.eval_tris], data, viewname = viewname)   

class heaviside():
    import levelSetMeshCut as lscut
    def __init__(self, name, xy, tris, ls, tris_map = None): 
        self.name = name
        self.xy = xy
        self.tris = tris
        self.tris_xy = self.xy[tris]
        self.appro_mapping = mapping.t3map(self.tris_xy)
        
        self.ls = ls
        self.mesh_mods, self.ls_mods = lscut.levelSetIsoCut(self.xy, self.tris, self.ls, returnparent = True) 
        self.appro2evals_index, self.appro2evals = mesh_mods.parents2sons()
        
        self.eval_tris_xy = mesh_mods.xy_new_vertices[self.mesh_mods.new_tris]
        self.eval_mapping = mapping.t3map(self.eval_tris_xy)
        self.side_appro   = (1.-lscut.compute_face_side(self.ls[self.tris]))*0.5
        self.side_eval    = (1.-lscut.compute_face_side(self.ls_mods[self.mesh_mods.new_tris] ))*0.5
        
        self.rmap = mapping.remapHelper(self.appro2evals_index, self.appro2evals, self.appro_mapping, self.eval_mapping)
        
        
    def evalOn(self, evaltrisuv, return_evaltrisxy = False):
        return self.evalOnSons_(evaltrisuv, self.rmap,  return_evaltrisxy = return_evaltrisxy)
        
    def evalOnSons_( self, evaltrisuv, rmap,  return_evaltrisxy = False ):
        npt_per_eval        = len(evaltrisuv)
        H = np.zeros( (rmap.nbevals, npt_per_eval) )
        H[ rmap.parent2eval_index ] = np.repeat(self.side_appro[rmap.approEQeval, np.newaxis], npt_per_eval, axis = 1)
        H[ rmap.son2eval_index ]    = np.repeat(self.side_eval[rmap.sons,  np.newaxis], npt_per_eval, axis = 1)
        if return_evaltrisxy : return H, rmap.getEvalTris()
        return H

class ridge_enrichment():
    def __init__(self, name, eval_xy, eval_tris, eval2appro, appro_tris, phi_at_eval_vertices):
        self.name = name
        self.eval_xy = eval_xy
        self.eval_tris = eval_tris
        self.appro_tris = appro_tris
        self.phi = phi_at_eval_vertices
        self.eval2appro = eval2appro
    
    def evalAtIntegPoint(self, eval_tri_id, appro_tri_id, evaltri_uv):
        if appro_tri_id == eval_tri_id : return np.zeros(len(evaltri_uv)) 
        eval_tri = self.eval_tris[eval_tri_id]
        appro_tri = self.appro_tris[ appro_tri_id]
        approtri_uv = mapping.t3map_old.e_uv_to_a_uv(self.eval_xy[eval_tri], self.eval_xy[appro_tri], evaltri_uv)
        return ridge_enrichment.evalAtIntegPoint_(self.phi[eval_tri], self.phi[appro_tri], evaltri_uv, approtri_uv)
        
    def evalAtIntegPoint_(phi_eval_tri_vertices, phi_appro_tri_vertices, evaltri_uv, approtri_uv):
        eu = evaltri_uv[:,0]
        ev = evaltri_uv[:,1]
        au = approtri_uv[:,0]
        av = approtri_uv[:,1]
        abs_phi_appro_tri_vertices = np.abs(phi_appro_tri_vertices)
        abs_phi_eval_tri_vertices  = np.abs(phi_eval_tri_vertices)
        ridge =  (1-au-av)*abs_phi_appro_tri_vertices[0] + au*abs_phi_appro_tri_vertices[1] + av*abs_phi_appro_tri_vertices[2] \
                -(1-eu-ev)*abs_phi_eval_tri_vertices[0]  - eu*abs_phi_eval_tri_vertices[1]  - ev*abs_phi_eval_tri_vertices[2]   
        return ridge
    
    def devalduv_AtIntegPoint(self, eval_tri_id, appro_tri_id, evaltri_uv):
        if appro_tri_id == eval_tri_id : return np.zeros((len(evaltri_uv),2)) 
        eval_tri = self.eval_tris[eval_tri_id]
        appro_tri = self.appro_tris[ appro_tri_id]
        map_a2x = mapping.t3map_old(self.eval_xy[appro_tri])
        map_e2x = mapping.t3map_old(self.eval_xy[eval_tri])
        Fappro2eval = map_e2x.invF.dot(map_a2x.F())
        dridgeduv = ridge_enrichment.devalduv_appro_AtIntegPoint_(self.phi[eval_tri], self.phi[appro_tri], Fappro2eval)
        return np.broadcast_to(dridgeduv, (len(evaltri_uv), 2))
    
    def devaldxy_AtIntegPoint(self, eval_tri_id, appro_tri_id, evaltri_uv):
        if appro_tri_id == eval_tri_id : return np.zeros((len(evaltri_uv),2)) 
        eval_tri = self.eval_tris[eval_tri_id]
        appro_tri = self.appro_tris[ appro_tri_id]
        map_a2x = mapping.t3map_old(self.eval_xy[appro_tri])
        Fa2x = map_a2x.F()
        Fx2a = map_a2x.invF()
        Fx2e = mapping.t3map_old(self.eval_xy[eval_tri]).invF()
        Fappro2eval = Fx2e.dot(Fa2x)
        dridgeduv = ridge_enrichment.devalduv_appro_AtIntegPoint_(self.phi[eval_tri], self.phi[appro_tri], Fappro2eval)
        dridgedxy  = dridgeduv.dot(Fx2a)
        return np.broadcast_to(dridgedxy, (len(evaltri_uv), 2))
    
    def devalduv_appro_AtIntegPoint_(phi_eval_tri_vertices, phi_appro_tri_vertices, Fappro2eval):
        abs_phi_appro_tri_vertices = np.abs(phi_appro_tri_vertices)
        abs_phi_eval_tri_vertices =  np.abs(phi_eval_tri_vertices)
        dridge_appro_part =  abs_phi_appro_tri_vertices[1:] - abs_phi_appro_tri_vertices[0]
        dridge_eval_part  = -(abs_phi_eval_tri_vertices[1:]  - abs_phi_eval_tri_vertices[0]).dot(Fappro2eval)
        return dridge_appro_part+dridge_eval_part

#class XFEMSpaceScalarP1():
class xfspace_heaviside():
    def __init__(self, name, appro_xy, appro_tris, ls_at_appro_nodes, enrichment_function  = heaviside):
        self.name  = name
        self.appro_xy      = appro_xy
        self.appro_tris    = np.atleast_2d(appro_tris)
        self.appro_tris_xy = self.appro_xy[self.appro_tris]
        self.appro_vertices_id   = np.unique(appro_tris.flatten())
        self.vid2vindex = {vid : vindex for vindex, vid in enumerate(self.appro_vertices_id) }
        self.eval_xy, self.eval_phi, self.eval_tris, self.eval_sides, self.eval2appro \
            = lscut.levelSetIsoCut(appro_xy, appro_tris, ls_at_appro_nodes, absfittol = 0., relfittol = 0.02, returnparent = True)
        self.appro2evals = np.array([[] for i in range(len(self.appro_tris))], dtype = object)
        
        for ii, ia in enumerate(self.eval2appro) : self.appro2evals[ia] += [ii]
            
        min_support = +np.inf*np.ones(len(self.vid2vindex))
        max_support = -np.inf*np.ones(len(self.vid2vindex))
        for tri in self.appro_tris :
            for vid in tri :
                phitri = self.eval_phi[tri]
                min_support[self.vid2vindex[vid]] = min(min_support[self.vid2vindex[vid]], np.min(phitri))
                max_support[self.vid2vindex[vid]] = max(max_support[self.vid2vindex[vid]], np.max(phitri))
        
        enriched_vertex = []
        for index, (min_support_i, max_support_i) in enumerate(zip(min_support,max_support)):
            print('minmw', min_support_i, max_support_i)
            if min_support_i <0 and max_support_i > 0 : enriched_vertex = enriched_vertex + [self.vid2vindex[index]]
                
        self.enriched_vertex = enriched_vertex
        self.isenriched_vertex  = np.zeros(len(self.appro_xy) , dtype = bool)
        self.isenriched_vertex[self.enriched_vertex] = True
        self.enrichment = enrichment_function(self.name+"enrichment", self.eval_xy, self.eval_tris, self.eval2appro, self.appro_tris, self.eval_phi)

    def gmsh_plot_enriched_vertices(self, viewname = "Heaviside Enriched Vertices"):
        gp.listPlotPointScalar(self.appro_xy[self.enriched_vertex], np.ones(len(self.enriched_vertex)), viewname = viewname )
        
    def gmsh_plot_enrichment_function(self):
        plotEnrichmentValue(self.enrichment, self.name+'_enrichment')
    
    
        
#        self.dofid = np.arange(len(self.vid))
#        self.vid2dofid = { vid : dofid  for (dofid, vid) in enumerate(self.vid) })
        
        

if __name__== '__main__' :   
    from exec_tools import profile
    import gmsh
    import gmsh_meshes_examples as gmshmesh
    
    testT3Space       = True
    testHeaviside     = True
    
    profile.doprofile = True
    popUpGmsh         = True
    elementSize       = 0.05
    
    gmsh.initialize()             
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)
    modelname, physical_dict = gmshmesh.generate_L_in3parts(elementSize)
    appro_xy      = gmsh.model.mesh.getNodes()[1].reshape((-1,3))[:, :-1]
    appro_tris    = gmsh.model.mesh.getElementsByType(2)[1].reshape((-1,3))-1
    appro_tris_xy = appro_xy[appro_tris]
    print('nb elements =', len(appro_tris))
    print('nb vertices =', len(appro_xy))
    
    if testT3Space:
        profile.enable()
        Tspace    = FEMSpaceScalarP1("Temperature", appro_xy, appro_tris)
        profile.disable()
        f= 1.
        class somefield():    
            def fun(xy) : return xy[:,1]*np.cos(f*np.pi*xy[:,0])
            def grad(xy) : 
                gx = -xy[:,1]*f*np.pi*np.sin(f*np.pi*xy[:,0])
                gy =      np.cos(f*np.pi*xy[:,0])
                return np.stack((gx,gy),1)
        class linearfield():
            def fun(xy) : return 3*xy[:,0]+4*xy[:,1]
            def grad(xy) : 
                gx = 3*np.ones(len(xy))
                gy = 4*np.ones(len(xy))
                return np.stack((gx,gy),1)
                    
        Tfun      = lambda xy : somefield.fun(xy)
        Tgrad     = lambda xy : somefield.grad(xy)
        Tdofs = Tspace.interpolate(Tfun)
        evalpointatcorner = quad.T3_nodes().uv
       
        profile.enable()
        #create evalOp and gradOp
        N, atris_xy = Tspace.evalOp( evalpointatcorner, return_evaltrisxy= True) 
        dNdx = Tspace.evalGradOp( evalpointatcorner, return_evaltrisxy= False) 
        profile.disable()
        
        # create levelset mesh cut and evalOp and gradOp on cut mesh
        profile.enable()
        phi = lscut.simpleLevelSetFunction.disk([0.,0.],0.4)
        appro_ls = phi(appro_xy)
        lscut.fit_to_vertices(appro_ls, appro_tris, absfittol = 1.e-12, relfittol = 1.e-3)
        mesh_mods, lsmods = lscut.levelSetIsoCut(appro_xy, appro_tris, appro_ls, returnparent = True)
        
        eval_mapping = mapping.t3map(mesh_mods.xy_new_vertices[mesh_mods.new_tris])
        appro2evals_index, appro2evals = mesh_mods.parents2sons()
        N_cut, etris_xy = Tspace.evalOpOnSons( evalpointatcorner, appro2evals_index, appro2evals, eval_mapping, return_evaltrisxy= True) 
        dNdx_cut, etris_xy_2 = Tspace.evalGradOpOnSons( evalpointatcorner, appro2evals_index, appro2evals, eval_mapping, return_evaltrisxy= True) 
        profile.disable()
        
        gp.listPlotScalarField(atris_xy, N.dot(Tdofs).reshape((-1, 3)), NbIso = 10, viewname = 'evalOnMesh')
        gp.listPlotVectorField(atris_xy, dNdx.dot(Tdofs).reshape((-1, 3, 2)), NbIso = 10, viewname = 'evalGradOnMesh') 
        gp.listPlotScalarField(etris_xy, N_cut.dot(Tdofs).reshape((-1, 3)), NbIso = 10, viewname = 'evalOnCutMesh')    
        gp.listPlotVectorField(etris_xy_2, dNdx_cut.dot(Tdofs).reshape((-1, 3, 2)), NbIso = 10, viewname = 'evalGradOnCutMesh') 
        #Timing 08/01/2023,
        #mesh size = 0.005, 139640 vertices. evalOp, evalGradOp, evalOpOnSons, evalGradOpOnSons,  0.012, 0.087, 0.044,  0.129  
    
    if testHeaviside:
        phi = lscut.simpleLevelSetFunction.disk([0.,0.], 0.4)
        appro_ls = phi(appro_xy)
        lscut.fit_to_vertices(appro_ls, appro_tris, absfittol = 1.e-12, relfittol = 1.e-3)
        mesh_mods, ls_mods = lscut.levelSetIsoCut(appro_xy, appro_tris, appro_ls, returnparent = True)
        eval_xy, eval_tris, vls = mesh_mods.getNewMesh(appro_ls, ls_mods)
        gp.listPlotScalarNodalField(eval_xy, eval_tris, vls, viewname = "isocut_ls", IntervalsType = 3, NbIso=10)
        profile.enable()
        Hfun = heaviside("H", appro_xy, appro_tris, appro_ls)
        H, etris_xy = Hfun.evalOn(quad.T3_nodes().uv, return_evaltrisxy = True)
        profile.disable()
        gp.listPlotScalarField(etris_xy, H, NbIso = 10, viewname = 'HOnCutMesh')  
      
        
        if ridgepatchtest :
        print('error : borken test ... To be cleaned up')
        phi = lambda x : x[:, 1] - 0.5
        
        name1 = 'case 1'
        xy1 = np.array([[0.,0.], [1.,0.], [2.,0.],
                       [0.,1.], [1.,1.], [2.,1.]
                       ])
        tris1 = np.array([[0,1,3], [1,4,3], [1,5,4], [1,2,5]])
        phi1 = phi
        
        name2 = 'case 2'
        xy2 = np.array([[0.,0.], [1.5,0.], [3.,0.],
                       [0.,1.], [1.,1.], [2.,1.], [3., 1.]
                       ])
        tris2 = np.array([[0,1,3], [1,4,3], [1,5,4], [1,6,5], [1,2,6]])
        phi2 = phi
        
        name3 = 'case 3'
        elementSize = 0.55
        modelName, physical_dict = gmshmesh.generate_ref_square(elementSize)
        xy3   = meshInterface.getVerticesCoord(modelName)
        tris3 = meshInterface.getTris(modelName) 
        def phi3(xy):
            return np.sqrt(xy[:,0]**2 + xy[:,1]**2) - 0.4
 
        xys = [ xy3]
        triss = [ tris3]
        phis = [phi3]
        names = [name3]
        
        for name, xy, tris, phi in zip(names, xys, triss, phis) :
            print('##### '+name)
            ls = phi(xy)
            lscut.fit_to_vertices(ls, tris, absfittol = 1.e-12, relfittol = 1.e-3)
            mesh_mods, lsmods = lscut.levelSetIsoCut(xy, tris, ls, returnparent = True)
            #xspace = xRidgeSpaceP1('test', mesh_mods, ls, lsmods, dosvd = True, dodebugplot = True, version = 8)
            xtestspace = xTestRidgeSpaceP1('test', mesh_mods, ls, lsmods, dosvd = True, dodebugplot = True, version = 8)
            #data  = xtestspace.R
            
            N_sparse, etris_xy = xtestspace.evalOp(quad.T3_nodes().uv,return_evaltrisxy = True)
            dN_sparse          = xtestspace.evalGradOp(quad.T3_nodes().uv)
            
            T=  np.ones(N_sparse.shape[1])
            gp.listPlotScalarField(etris_xy, N_sparse.dot(T).reshape((-1,3)), NbIso = 10, viewname = name+'Renriched_function_sum')
            gp.listPlotVectorField(etris_xy, dN_sparse.dot(T).reshape((-1, 3, 2)), NbIso = 10, viewname = name+'dRenriched_function_sum')   
            for i in range(N_sparse.shape[1]) :
                T=  np.zeros(N_sparse.shape[1])
                T[i] = 1.
                gp.listPlotScalarField(etris_xy, N_sparse.dot(T).reshape((-1,3)), NbIso = 10, viewname = name+'Renriched_function_{}'.format(i))
                gp.listPlotVectorField(etris_xy, dN_sparse.dot(T).reshape((-1, 3, 2)), NbIso = 10, viewname = name+'dRenriched_function_{}'.format(i))   
            
                
        N = np.zeros((10,10))
        N[1,0] = 1
        N[2,1] = 1
        N[3,2] = 1
        N[4,3] = 1
        N[7,4] = 1
        N[8,5] = 1
        N[[4, 5, 6] ,6] = [1, 2, 1] 
        N[[5, 6, 7], 7] = [2, 1, 1]
        N[[0, 8, 9], 8] = [1, 1, 2]
        N[[0, 1,9]  ,9] = [2, 1, 2]
        
        
        N = np.zeros((10,10))
        N[1,0] = 1
        N[2,1] = 1
        N[3,2] = 1
        N[4,3] = 1
        N[7,4] = 1
        N[8,5] = 1
        N[0:7, 6] = [1,1,1,1,1,2,1]
        N[[5,6, 7, 8, 9], 7]   = [1,2,1,1,1]
        N[[0, 6,7,8,9], 8] = [1,1,1,1,2]
        N[[0,1,2,3,4,5, 9], 9] = [2,1,1,1,1,1,1]
        
        N1 = np.zeros((8,8))
        N1[0,0] = 1
        N1[1, 1:3] = 1
        N1[2,3]   = 1
        N1[3, 4] = 1
        N1[ 4, 5:7] = 1
        N1[5, 7] = 1
        N1[6, :] = [1,1,0,0,0,0,1,1]        
        #N[:, 7] = [-1,-1,0,0,0,2,1,-1]
        N1[ 7,:] = [0,0,1,1,1,1,0,0]
        
        N1 = np.zeros((8,8))
        N1[0,0] = 1
        N1[1, 1:3] = 1
        N1[2,3]   = 1
        N1[3, 4] = 1
        N1[ 4, 5:7] = 1
        N1[5, 7] = 1
        N1[6, :] = [1,1,0,0,0,0,1,1]        
        #N[:, 7] = [-1,-1,0,0,0,2,1,-1]
        N1[ 7,:] = [0,0,1,1,1,1,0,0]
        
        N2 = np.eye(8)
        
#        raise
#        
#        svd = np.linalg.svd(N_sparse.todense())
#        print(svd[1])
#        
#        N_sparse_collapse = N_sparse[:, [0,1,2,3,5]]
#        N_sparse_collapse[:, [3]] += N_sparse[:, [4]]
#        N_sparse_collapse[:, [4]] += N_sparse[:, [4]]
#        
#        svd = np.linalg.svd(N_sparse_collapse.todense())
#        print(svd[1])
#        
#        xspace = xRidgeSpaceP1('test', mesh_mods, ls, lsmods, version = 5, dosvd = True, dodebugplot = True)
#        N_sparse = xspace.evalOp(quad.T3_nodes().uv)
#       
#        print('#### case 2')
#        xy = np.array([[0.,0.], [1.5,0.], [3.,0.],
#                       [0.,1.], [1.,1.], [2.,1.], [3., 1.]
#                       ])
#        tris = np.array([[0,1,3], [1,4,3], [1,5,4], [1,6,5], [1,2,6]])
#        ls = phi(xy)
#        lscut.fit_to_vertices(ls, tris, absfittol = 1.e-12, relfittol = 1.e-3)
#        mesh_mods, lsmods = lscut.levelSetIsoCut(xy, tris, ls, returnparent = True)
#        #xspace = xRidgeSpaceP1_('test', mesh_mods, ls, lsmods, version = 0, dosvd = False, dodebugplot = True)
#        xspace = xRidgeSpaceP1_('test', mesh_mods, ls, lsmods,  dosvd = False, dodebugplot = True)
        
#        N_sparse = xspace.evalOp(quad.T3_nodes().uv)
#        N_dense = N_sparse.todense()
#        q, r = np.linalg.qr(N_dense)
#        #r2 = r[:-1,:-1]
#        #r2[:-1] +=r[:-1,-1]
#        #N_reduced = q[:,:-1].dot( r[:-1,:-1] + np.diag(r[:-1,-1]) )
#        N_reduced = q[:,:-1].dot( r2 )
#        
#        pu0 = N_dense.dot(np.ones(N_dense.shape[1]))
#        pu1 = N_reduced.dot(np.ones(N_reduced.shape[1]))# + q[:,:-1].dot(r[:-1,-1])
#        
#        
#        
#        
#        svd = np.linalg.svd(N_sparse.todense(), full_matrices = False)
#        print(svd[1])
#        N1 =N_sparse.todense() 
#        qr = np.linalg.qr(N_sparse.todense())
#        print(qr)
#        qr0 = np.linalg.qr(N_sparse.todense()[:,ls<0])
#        qr1 = np.linalg.qr(N_sparse.todense()[:,ls>0])
#        
#        
#        N_sparse_collapse = N_sparse[:, [0,1,2,3,6]]
#        N_sparse_collapse[:, [3]] += N_sparse[:, [4]] +N_sparse[:, [5]]
#        N_sparse_collapse[:, [4]] += N_sparse[:, [4]] +N_sparse[:, [5]]
#        
#        
#        svd = np.linalg.svd(N_sparse_collapse.todense(), full_matrices = False)
#        print('m2', svd[1])
#        
#        N_sparse_collapse = N_sparse[:, [0,1,2,3,4, 6]]+ N_sparse[:, [0,1,2,3,5, 6]]
#        #N_sparse_collapse[:, [4]] += N_sparse[:, [5]]
#        #N_sparse_collapse[:, [5]] += N_sparse[:, [5]] +N_sparse[:, [5]]
#        
#        
#        svd = np.linalg.svd(N_sparse_collapse.todense(), full_matrices = False)
#        print('m1', svd[1])
        
        
        
        #xspace = xRidgeSpaceP1('test', mesh_mods, ls, lsmods, version = 5, dosvd = True, dodebugplot = True)
        #N_sparse = xspace.evalOp(quad.T3_nodes().uv)
            
    if popUpGmsh : gmsh.fltk.run()
    gmsh.finalize()
       
    profile.print_stats()
    


        