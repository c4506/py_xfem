#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20 16:26:58 2023

@author: nchevaug
"""
import numpy as np

class dmesh():
    def getCommonVertex(e0, e1):
            v0e0, v1e0  = e0.vertices
            v0e1, v1e1  = e1.vertices
            if v0e0 == v0e1 : return v0e0
            if v0e0 == v1e1 : return v0e0
            if v1e0 == v0e1 : return v1e0
            if v1e0 == v1e1 : return v1e0
            raise 
    def getCommonEdge(f0, f1):
        ef0 = set(f0.edges)
        ef1 = set(f1.edges)
        e = ef0.intersection(ef1)
        if len(e) == 1 : return e.pop()
        raise
    class vertex():
        def __init__(self, i, xyz):
            self.id = i
            self.xyz = xyz
            self.edges = set()
        def __lt__(self, other):
            return self.id < other.id
        def addedge(self, e):
            self.edges.add(e)
        def removeedge(self, e):
            self.edges.remove(e)            
    class edge():
        def __init__(self, v0, v1):
            self.vertices = (v0,v1) if v0 < v1 else (v1, v0)
            #self.hash = hash(self.getKey())
            self.faces = set()
        def addface(self, f):
            self.faces.add(f)
        def removeface(self, f):
            self.faces.remove(f)
        def print_vid(self):
            v0, v1 = self.vertices
            print(v0.id, v1.id)
        def getKey(self):
            return self.vertices
        def __hash__(self):
            #return self.hash
            return hash(self.getKey())
        def __eq__(self, other):
            return self.vertices  == other.vertices
        def __lt__(self, other):
            return self.vertices  < other.vertices        
    class face():
        def __init__(self, fid, e0, e1, e2):
            check = False
            if check :
                dmesh.getCommonVertex(e0,e1)
                dmesh.getCommonVertex(e1,e2)
                dmesh.getCommonVertex(e2,e0)
            self.edges = (e0, e1, e2)
            self.id   = fid
            #self.hash = hash(self.getKey())
        def __hash__(self):
            #return self.hash
            return hash(self.getKey())
        def __eq__(self, other):
            return self.edges == other.edges
        def __lt__(self, other):
            return self.edges() < other.edges
        def print_vid(self):
            v0, v1, v2 = self._vertices()
            print(v0.id, v1.id, v2.id)
        def getKey(self):
            return self.edges       
        def getVertices(self):
            e0, e1, e2  = self.edges
            v0 = dmesh.getCommonVertex(e2, e0)
            v1 = dmesh.getCommonVertex(e0, e1)
            v2 = dmesh.getCommonVertex(e1, e2)
            return [v0, v1, v2]        
    def insert_vertex(self, xyz):
        vn = dmesh.vertex(self.nextvid, xyz)
        self._vertices.append(vn)
        self.nextvid += 1
        return vn        
    def insert_edge(self, v0, v1):
        etrial = dmesh.edge(v0,v1)
        key = etrial.getKey()
        e      = self._edges.get(key)
        if e is None :
            e = etrial
            self._edges.update({key : e})
            v0.addedge(e)
            v1.addedge(e)
        return e
    def insert_face(self, fid, e0, e1, e2):
        ftrial = dmesh.face(fid, e0, e1, e2)
        key = ftrial.getKey()
        f = self._faces.get(key)
        if  f is None:
            f = ftrial
            e0.addface(f)
            e1.addface(f)
            e2.addface(f)
            self._faces.update({key : f})
        return f    
    def _delete_face(self, f):
        for e in f.edges :  e.removeface(f)
        self._faces.pop(f.getKey())        
    def _delete_edge(self,e):
        if len(e.faces) > 0  : raise
        for v in e.vertices :  v.removeedge(e)
        self._edges.pop(e.getKey())        
    def __init__(self, xyz, tris):       
        self._vertices = [dmesh.vertex(i, xyz) for i, xyz in enumerate(xyz)]
        self._edges =   dict()
        self._faces    = dict()
        for fid, t in enumerate(tris) :
            v0, v1, v2 = self._vertices[t[0]], self._vertices[t[1]], self._vertices[t[2]]
            e0 = self.insert_edge(v0,v1)
            e1 = self.insert_edge(v1,v2)
            e2 = self.insert_edge(v2,v0)
            self.insert_face(fid, e0, e1, e2)
        self.nextvid = len(self._vertices)
        self.nextfid = len(self._faces)        
    def getCoordAndConnectivity(self):
        v2id = { v:i for i, v in enumerate (self._vertices)}
        xyz  = np.array([ v.xyz for v in self._vertices ])
        tris = np.array( [ [ v2id[v] for v in f.getVertices()] for f in self.getFaces()])
        return xyz, tris    
    def getVertices(self):
        return self._vertices    
    def getEdges(self):
        return self._edges.values()    
    def getFaces(self):
        return self._faces.values()    
    def edgeCutCallBackPass(e, e0, e1):
        pass
    def faceCutCallBackPass(f, f0, f1): 
        pass
    def split_edge(self, e, xyz, edgeCutCallBack = edgeCutCallBackPass , faceCutCallBack = faceCutCallBackPass):
        v0, v1 = e.vertices
        vn = self.insert_vertex(xyz)
        e0 = self.insert_edge(v0, vn)
        e1 = self.insert_edge(v1, vn)
        edgeCutCallBack(e, e0, e1)
        ftodel = [f for f in e.faces]
        for f in e.faces:
             fie = 0 if f.edges[0]==e else  1 if f.edges[1]==e else 2
             fe0, fe1, fe2 = f.edges[fie], f.edges[(fie+1)%3], f.edges[(fie+2)%3]
             fv0 = dmesh.getCommonVertex(fe2, fe0)
             vc =  dmesh.getCommonVertex(fe1, fe2)
             fen = self.insert_edge(vn,vc)
             if fv0 == v0:
                 f0e0, f0e1, f0e2 = e0, fen, fe2
                 f1e0, f1e1, f1e2 = e1, fe1, fen
             else :
                 f0e0, f0e1, f0e2 = e1, fen, fe2
                 f1e0, f1e1, f1e2 = e0, fe1, fen
             f0 = self.insert_face( self.nextfid, f0e0, f0e1, f0e2)
             self.nextfid +=1
             f1 = self.insert_face(self.nextfid, f1e0, f1e1, f1e2)
             self.nextfid +=1
             faceCutCallBack(f, f0, f1)
        for f in ftodel :self._delete_face(f)
        self._delete_edge(e)
        return vn

if __name__ =='__main__' :      
    import gmsh
    import gmsh_meshes_examples as gmshmesh
    import gmsh_post as gp
    import cProfile
    
    gmsh.initialize()             
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)
    
    dosmalltest = False
    dolargetest = True
    dopopup = True
    
    if dosmalltest :
        print('test on mesh with 2 elements:')
        xy  = np.array([[0.,0.],[1,0.],[0.,1],[1,1]])
        tris = np.array([[0,1,2],[2,1,3]])
        print('create dynamic mesh')
        m = dmesh(xy, tris)
        xy2, tris2 = m.getCoordAndConnectivity()
        print(xy2)
        print(tris2)
        gp.listPlotScalarElementField(xy2, tris2, np.ones(len(tris2)) )
        
    if dolargetest : 
        pr = cProfile.Profile()
        print('test on a gmsh mesh')
        modelname, physical_dict = gmshmesh.generate_L_in3parts(0.005)
        xy      = gmsh.model.mesh.getNodes()[1].reshape((-1,3))[:, :-1]
        tris    = gmsh.model.mesh.getElementsByType(2)[1].reshape((-1,3))-1
        print('number of elements:', len(tris))
        pr.enable()
        m = dmesh(xy, tris)
        xy2, tris2 = m.getCoordAndConnectivity()
        pr.disable()
        gp.listPlotScalarElementField(xy2, tris2, np.ones(len(tris2)) )
        pr.print_stats(sort='cumulative')
    if dopopup : gmsh.fltk.run()
    gmsh.finalize()
    
    
#    
#test on a gmsh mesh
#number of elements: 69760
#         7120335 function calls (6492495 primitive calls) in 2.803 seconds
#
#   Ordered by: cumulative time
#
#   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
#        1    0.125    0.125    2.599    2.599 oneLevelSimplexMesh.py:118(__init__)
#    69760    0.506    0.000    1.370    0.000 oneLevelSimplexMesh.py:100(insert_face)
#   209280    0.379    0.000    0.967    0.000 oneLevelSimplexMesh.py:90(insert_edge)
#   419360    0.114    0.000    0.687    0.000 {method 'add' of 'set' objects}
#  1256480    0.370    0.000    0.584    0.000 oneLevelSimplexMesh.py:50(__hash__)
#   209280    0.050    0.000    0.577    0.000 oneLevelSimplexMesh.py:41(addface)
#   209280    0.070    0.000    0.471    0.000 oneLevelSimplexMesh.py:67(__hash__)
#1465760/837920    0.207    0.000    0.439    0.000 {built-in method builtins.hash}
#   209280    0.237    0.000    0.282    0.000 oneLevelSimplexMesh.py:37(__init__)
#        1    0.002    0.002    0.230    0.230 oneLevelSimplexMesh.py:130(getCoordAndConnectivity)
#   210080    0.057    0.000    0.217    0.000 oneLevelSimplexMesh.py:32(addedge)
#   279040    0.089    0.000    0.194    0.000 {method 'get' of 'dict' objects}
#        1    0.121    0.121    0.138    0.138 oneLevelSimplexMesh.py:119(<listcomp>)
#    69760    0.040    0.000    0.123    0.000 oneLevelSimplexMesh.py:79(getVertices)
#  1465760    0.123    0.000    0.123    0.000 oneLevelSimplexMesh.py:48(getKey)
#   209280    0.083    0.000    0.083    0.000 oneLevelSimplexMesh.py:11(getCommonVertex)
#   209280    0.045    0.000    0.045    0.000 oneLevelSimplexMesh.py:30(__lt__)
#   174800    0.041    0.000    0.041    0.000 {method 'update' of 'dict' objects}
#        2    0.032    0.016    0.032    0.016 {built-in method numpy.array}
#    69760    0.031    0.000    0.031    0.000 oneLevelSimplexMesh.py:133(<listcomp>)
#   279040    0.026    0.000    0.026    0.000 oneLevelSimplexMesh.py:77(getKey)
#    69760    0.022    0.000    0.022    0.000 oneLevelSimplexMesh.py:58(__init__)
#    35281    0.017    0.000    0.017    0.000 oneLevelSimplexMesh.py:26(__init__)
#        1    0.011    0.011    0.011    0.011 oneLevelSimplexMesh.py:131(<dictcomp>)
#        1    0.003    0.003    0.003    0.003 weakref.py:561(__call__)
#        1    0.002    0.002    0.002    0.002 oneLevelSimplexMesh.py:132(<listcomp>)
#        1    0.000    0.000    0.000    0.000 oneLevelSimplexMesh.py:139(getFaces)
#        2    0.000    0.000    0.000    0.000 {built-in method builtins.len}
#        1    0.000    0.000    0.000    0.000 {method 'values' of 'dict' objects}
#        1    0.000    0.000    0.000    0.000 {method 'pop' of 'dict' objects}
#        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
    
