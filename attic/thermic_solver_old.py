#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 15:10:47 2023

@author: nchevaug
"""
import numpy as np
import scipy
import gmsh
import xfem_field as xf
import mapping

class ThermicSolver():
#getEntitiesForPhysicalGroup
    def __init__(self, gmshmodelname, conductivity, physical_dict, dirichlet = dict()):

        old_gmshmodelname = gmsh.model.getCurrent()
        
        gmsh.model.setCurrent(old_gmshmodelname)
        self.xy = gmsh.model.mesh.getNodes()[1].reshape((-1,3))[:,:-1]
        self.tris = gmsh.model.mesh.getElementsByType(2)[1].reshape((-1,3))-1
        self.Tfield = xf.FEMSpaceScalarP1("Temperature", self.xy, self.tris)
        self.tris_xy  = self.Tfield.tris_xy
        integrationRule0 = xf.T3_gausspoints(0)
        field_dic = self.Tfield.evalGradOp([integrationRule0]*len(self.tris))
        nv = len(self.xy)
        nt = len(self.tris)
        
        Bval = np.array(sum([list(dNdx.flatten()) for dNdx in field_dic['dNdx']], []))
        Bi   = np.arange(nt*2).repeat(3)
        Bj   =  np.array( sum([ list(np.tile(dofs, 2)) for dofs in field_dic['dofs']], []))
        print(Bval.shape, Bi.shape, Bj.shape)
        self.B = scipy.sparse.csr_array((Bval, (Bi, Bj)), (2*nt, nv))
        
        Dval =  np.array( sum([  list(np.eye(2).flatten()*conductivity*integrationRule0.w[0]*mapping.t3map_old(xy_appro).detJ()) for xy_appro in  self.tris_xy], []))
        Dj = np.array(sum([ [2*e, 2*e+1]*2 for e in  np.arange(nt)],[]))
        Di = np.arange(2*nt).repeat(2)
        self.D = scipy.sparse.csr_array((Dval, (Di, Dj)), (2*nt, 2*nt))
        
        self.dirichlet_to_dofs = {}
        for physname, val in dirichlet.items() :
            dim, geoentity = physical_dict[physname]
            nodes_tag, xyz = gmsh.model.mesh.getNodesForPhysicalGroup(dim, geoentity)
            nodes_tag -= 1
            dofsid = [self.Tfield.vid2dofid[vid] for vid in nodes_tag]
            self.dirichlet_to_dofs[physname] = dofsid
        
        freedofs = set(self.Tfield.dofid)
        for diri in self.dirichlet_to_dofs.values() : 
            freedofs = freedofs - set(diri)
        self.freedof = list(freedofs)
        
        self.Bfree = self.B[:, self.freedof]
        self.Kfree = self.Bfree.T.dot(self.D).dot(self.Bfree)
        
        print(dirichlet)
        print(dirichlet.values())
        
        self.Fdiri = np.zeros(len(self.freedof))
        U = np.zeros(nv)
        for dofs, dval in zip(self.dirichlet_to_dofs.values(), dirichlet.values()):
            self.Fdiri -= self.Bfree.T.dot(self.D.dot(self.B[:, dofs].dot(np.ones(len(dofs))*dval) ))
            U[dofs] = dval
        U[self.freedof] = scipy.sparse.linalg.spsolve(self.Kfree, self.Fdiri)
        self.U = U       
        gmsh.model.setCurrent(old_gmshmodelname)
        
if __name__== '__main__': 
    from exec_tools import profile      
    import gmsh_meshes_examples as gmshmesh
    import gmsh_post as gp
    
    elementSize             = 0.02
    #elementSize = 0.02
    popUpGmsh               = True
    profile.doprofile       = True

    gmsh.initialize()      
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)       
    modelname, physical_dict = gmshmesh.generate_L_in3parts(elementSize)
    nv =  len(gmsh.model.mesh.getNodes()[0])
    nf =  len(gmsh.model.mesh.getElementsByType(2)[0])
    print('nb elements =', nf)
    print('nb vertices =', nv)
    physname_to_dirichlet_value = {'dOmega_left':0.33, 'dOmega_right' :1.27}
    profile.enable()
    pb = ThermicSolver(modelname,1., physical_dict, physname_to_dirichlet_value )
    profile.disable()
    gp.listPlotScalarNodalField(pb.xy, pb.tris, pb.U, viewname = "T",  NbIso=10)
    
    if popUpGmsh : gmsh.fltk.run()
    gmsh.finalize()
    profile.print_stats()
    
    
#    # Profile for elmentSize = 0.02
#         2153269 function calls (2153260 primitive calls) in 9.646 seconds
#
#   Ordered by: cumulative time
#
#   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
#        1    0.005    0.005    9.646    9.646 thermic_solver_old.py:16(__init__)
#        4    7.302    1.825    7.302    1.825 {built-in method builtins.sum}
#        1    0.060    0.060    1.125    1.125 xfem_field.py:95(evalGradOp)
#    34960    0.423    0.000    1.072    0.000 mapping.py:20(__init__)
#   104988    0.079    0.000    1.036    0.000 {built-in method numpy.core._multiarray_umath.implement_array_function}
#    17480    0.040    0.000    0.984    0.000 xfem_field.py:31(dNdx)
#        1    0.162    0.162    0.975    0.975 thermic_solver_old.py:36(<listcomp>)
#    34960    0.021    0.000    0.495    0.000 <__array_function__ internals>:177(inv)
#    34960    0.231    0.000    0.445    0.000 linalg.py:483(inv)
#    17480    0.019    0.000    0.215    0.000 xfem_field.py:27(dNdu)
#    17480    0.032    0.000    0.199    0.000 mapping.py:49(detJ)
#    17480    0.010    0.000    0.194    0.000 <__array_function__ internals>:177(broadcast_to)
#    17480    0.008    0.000    0.171    0.000 stride_tricks.py:367(broadcast_to)
#    17480    0.010    0.000    0.167    0.000 <__array_function__ internals>:177(det)
#    17480    0.118    0.000    0.162    0.000 stride_tricks.py:340(_broadcast_to)
#    17480    0.072    0.000    0.146    0.000 linalg.py:2100(det)
#        1    0.030    0.030    0.140    0.140 thermic_solver_old.py:32(<listcomp>)
#    17480    0.009    0.000    0.134    0.000 <__array_function__ internals>:177(einsum)
#   104930    0.115    0.000    0.115    0.000 {built-in method numpy.array}
#    17480    0.007    0.000    0.110    0.000 <__array_function__ internals>:177(tile)
#    52440    0.065    0.000    0.110    0.000 linalg.py:136(_commonType)
#   139840    0.108    0.000    0.108    0.000 {method 'dot' of 'numpy.ndarray' objects}
#    17480    0.055    0.000    0.095    0.000 shape_base.py:1191(tile)
#    17480    0.008    0.000    0.085    0.000 einsumfunc.py:1009(einsum)
#    17480    0.078    0.000    0.078    0.000 {built-in method numpy.core._multiarray_umath.c_einsum}
#    17480    0.054    0.000    0.071    0.000 twodim_base.py:162(eye)
#    17480    0.055    0.000    0.055    0.000 xfem_field.py:104(<listcomp>)
#    52440    0.049    0.000    0.049    0.000 linalg.py:194(_assert_stacked_2d)
#    34962    0.040    0.000    0.040    0.000 {method 'flatten' of 'numpy.ndarray' objects}
#    17480    0.008    0.000    0.040    0.000 mapping.py:46(invF)
#        1    0.023    0.023    0.036    0.036 thermic_solver_old.py:30(<listcomp>)
#    34960    0.021    0.000    0.033    0.000 linalg.py:112(_makearray)
#        1    0.000    0.000    0.031    0.031 linsolve.py:103(spsolve)
#        1    0.000    0.000    0.030    0.030 umfpack.py:743(linsolve)
#   104880    0.022    0.000    0.030    0.000 linalg.py:117(isComplexType)
#        1    0.000    0.000    0.028    0.028 umfpack.py:547(numeric)
#    17502    0.014    0.000    0.023    0.000 {built-in method builtins.any}
#    17480    0.023    0.000    0.023    0.000 {method 'astype' of 'numpy.generic' objects}
#    52440    0.016    0.000    0.023    0.000 linalg.py:130(_realType)
#        1    0.000    0.000    0.021    0.021 _umfpack.py:589(umfpack_dl_numeric)
#        1    0.021    0.021    0.021    0.021 {built-in method scikits.umfpack.__umfpack.umfpack_dl_numeric}
#    52440    0.020    0.000    0.020    0.000 linalg.py:200(_assert_stacked_square)
#    34972    0.015    0.000    0.015    0.000 {method 'astype' of 'numpy.ndarray' objects}
#   157329    0.015    0.000    0.015    0.000 {built-in method builtins.issubclass}
#    34960    0.014    0.000    0.014    0.000 linalg.py:107(get_linalg_error_extobj)
#    17482    0.013    0.000    0.013    0.000 {method 'repeat' of 'numpy.ndarray' objects}
#    17494    0.013    0.000    0.013    0.000 {built-in method numpy.zeros}
#    87400    0.012    0.000    0.012    0.000 einsumfunc.py:1001(_einsum_dispatcher)
#    34962    0.011    0.000    0.011    0.000 {method 'reshape' of 'numpy.ndarray' objects}
#    17480    0.007    0.000    0.010    0.000 function_base.py:346(iterable)
#    69920    0.009    0.000    0.009    0.000 stride_tricks.py:345(<genexpr>)
#    70223    0.009    0.000    0.009    0.000 {built-in method builtins.len}
#    52622    0.008    0.000    0.008    0.000 {built-in method numpy.asarray}
#    52440    0.008    0.000    0.008    0.000 linalg.py:479(_unary_dispatcher)
#    34960    0.008    0.000    0.008    0.000 {method '__array_prepare__' of 'numpy.ndarray' objects}
#    34984    0.007    0.000    0.007    0.000 {built-in method builtins.getattr}
#    52456    0.007    0.000    0.007    0.000 {method 'get' of 'dict' objects}
#        1    0.000    0.000    0.006    0.006 umfpack.py:507(symbolic)
#        1    0.000    0.000    0.006    0.006 _umfpack.py:573(umfpack_dl_symbolic)
#        1    0.006    0.006    0.006    0.006 {built-in method scikits.umfpack.__umfpack.umfpack_dl_symbolic}
#    17480    0.006    0.000    0.006    0.000 stride_tricks.py:25(_maybe_view_as_subclass)
#        1    0.005    0.005    0.005    0.005 thermic_solver_old.py:37(<listcomp>)
#    17480    0.003    0.000    0.005    0.000 {built-in method builtins.all}
#        8    0.000    0.000    0.005    0.001 _base.py:400(dot)
#        8    0.000    0.000    0.005    0.001 _base.py:626(__matmul__)
#        8    0.000    0.000    0.005    0.001 _base.py:510(_mul_dispatch)
#        1    0.001    0.001    0.004    0.004 xfem_field.py:35(__init__)
#        2    0.000    0.000    0.004    0.002 _compressed.py:507(_mul_sparse_matrix)
#    34960    0.004    0.000    0.004    0.000 shape_base.py:1273(<genexpr>)
#    18/12    0.000    0.000    0.004    0.000 _compressed.py:26(__init__)
#    35007    0.004    0.000    0.004    0.000 {built-in method _operator.index}
#    34960    0.004    0.000    0.004    0.000 shape_base.py:1263(<genexpr>)
#    34961    0.003    0.000    0.003    0.000 {method 'append' of 'list' objects}
#    17489    0.003    0.000    0.003    0.000 {built-in method builtins.iter}
#    