#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 16:17:21 2023

@author: nchevaug
"""  #231
import numpy as np
import oneLevelSimplexMesh as onelevel
dmesh  = onelevel.dmesh


def compute_face_side(tris_ls):
    ''' given tris_ls an 2d array such as each line i give the value of the levelset at the nodes of an element 
        return an array containing the side of each element (-1, 0, 1, respectivily inside, crossing o out of the iso zero of the ls)'''
    return np.where(np.all( tris_ls >=0.,1), 1, np.where(np.all(tris_ls<=0., 1), -1, 0))

def faces2cut(tris_ls) :
    ''' given tris_ls an 2d array such as each line i give the value of the levelset at the nodes of an element 
        return the indexes of the elements that needs to be cut '''
    return np.where(np.all(np.stack((np.any(tris_ls>0 , 1), np.any(tris_ls<0 , 1)),1),1))[0]
    
def set_levelset(vertices, lsAtVertices, setls): 
    for v, ls in zip(vertices, lsAtVertices) : setls(v, ls)
            
def fit_to_vertices_old(vertices, getls, setls, absfittol, relfittol) :
    for v in vertices :
            lsv = getls(v)
            if np.abs(lsv) < absfittol : setls(v, 0.)
    for v in vertices :
            ls0 = getls(v)
            if ls0 ==0. : continue
            for e in v.edges:
                ls1 = getls(e.v1) if e.v0 == v else getls(e.v0) 
                if ls1*ls0 <= 0. :
                    s = -ls0/(ls1-ls0)
                    if s < relfittol : 
                        setls(v, 0.)
                        break
                    
def fit_to_vertices_old_v2(vls, v2v_index, v2v, absfittol, relfittol) :
    vls[ np.abs(vls) < absfittol] = 0.
    ls1 = vls[v2v]
    nv = len(v2v_index)-1  
    v0_indexes = np.repeat(np.arange(nv), v2v_index[1:]-v2v_index[:-1])
    ls0 = vls[v0_indexes]
    s_index = np.where(np.sign(ls0)*np.sign(ls1) == -1)[0]
    ils0 = ls0[s_index]
    ils1 = ls1[s_index]
    vls[ v0_indexes[s_index[ -ils0/(ils1-ils0) < relfittol]] ] = 0.
    
def fit_to_vertices(vls, trisv, absfittol, relfittol):
    vls[ np.abs(vls) < absfittol] = 0.
    ev = trisv[:,[2,0,0,1,1,2]].reshape((-1,2))
    evls = vls[ev]
    ev0ls = evls [:,0]
    ev1ls = evls [:,1]
    s_index = np.where(np.sign(ev0ls)*np.sign(ev1ls) == -1)[0]
    
    iev0ls = ev0ls[s_index]
    iev1ls = ev1ls[s_index]
    vls[ev[s_index[ -iev0ls/(iev1ls-iev0ls) < relfittol],0 ]] =   0.
            
def docut(e, getls):
    return getls(e.v0)*getls(e.v1) < 0

def cutpos(e, getls) :
    v0, v1 = e.v0, e.v1
    ls0, ls1 = getls(v0), getls(v1)
    s = -ls0/(ls1-ls0)
    xy = v0.xy*(1-s) +  v1.xy*s
    return s, xy

def build_interpolatecallback_default(setls):
    def interpolatecallback_default(s, vn, e) :
        setls(vn,0.)
    return interpolatecallback_default

def facecallback_propagate_parent(f, f0, f1):
    parent = f.id if not hasattr(f, "parent") else f.parent
    e = dmesh.getCommonEdge(f0,f1)
    f0.parent, f1.parent, e.parent  = [parent]*3

def lscut(m, getls, setls, fit = True, absfittol = 1.e-6, relfittol = 1.e-2, interpolatecallback = None, edgecallback = dmesh.edgeCutCallBackPass, facecallback = dmesh.faceCutCallBackPass):
    if interpolatecallback is None : interpolatecallback = build_interpolatecallback_default(setls)
    if fit : fit_to_vertices_old(m.getVertices(), getls, setls, absfittol, relfittol)
    edges2cut =[(e, cutpos(e, getls)) for e in m.getEdges() if docut(e, getls) ] 
    for (e, (s,xy)) in  edges2cut : 
        vn = m.split_edge(e, xy, edgecallback, facecallback)
        interpolatecallback(s, vn, e)
                 
def levelSetIsoCut(xy, tris, lsAtVertices, absfittol = 1.e-12, relfittol = 1.e-6, returnparent = False):
    def getls(v) : return v.ls
    def setls(v, ls) : v.ls = ls
    m = dmesh(xy, tris)
    set_levelset(m.getVertices(), lsAtVertices, setls)
    if returnparent :
        lscut(m, getls, setls, absfittol, relfittol, facecallback= facecallback_propagate_parent)
    else :
        lscut(m, getls, setls, absfittol, relfittol)
    xy_cut, tri_cut = m.getCoordAndConnectivity()
    ls_cut = np.array([v.ls    for v in m.getVertices()])
    side_cut = compute_face_side(ls_cut[tri_cut])
    if returnparent :
       parent_id = np.array([f.id if not hasattr(f, "parent") else f.parent for f in m.getFaces()]) 
       return xy_cut, ls_cut, tri_cut, side_cut, parent_id
    return xy_cut, ls_cut, tri_cut, side_cut

def levelSetIsoCut2(xy, tris2v, lsAtVertices, absfittol = 1.e-12, relfittol = 1.e-6, returnparent = False):
    ''' improved version of levelSetIsoCut : only the part of the mesh that really need to be cut are loaded in a dynamic mesh. Much faster '''
    vls =  lsAtVertices
    fit_to_vertices(vls, tris2v, absfittol, relfittol)
    tris_ls  = vls[tris2v]                     # tris_ls[i,j] contains the values of the level set at node j of element i 
    tris2cut = faces2cut(tris_ls)              # list of elements to cut
    tris2cutv = tris2v[tris2cut]               # trisZcutv[i,j] : vertices j of triangle to cut i 
    v2cut  = np.unique(tris2cutv)              # list of vertices connected to triangles to cut. also used to remap from new to old numbering
    neworder = np.zeros(int(np.max(v2cut))+1).astype(v2cut.dtype)
    neworder[v2cut] = np.arange(len(v2cut))
    def getls(v) : return v.ls
    def setls(v, ls) : v.ls = ls
    m = dmesh(xy[v2cut], neworder[tris2cutv])
    set_levelset(m.getVertices(), vls[v2cut], setls)
    if returnparent :
        lscut(m, getls, setls, fit = False, facecallback = facecallback_propagate_parent)
    else :
        lscut(m, getls, setls,fit = False)
    xy_cut_, tris2v_cut_ = m.getCoordAndConnectivity()
    xy_cut   = np.vstack((xy,   xy_cut_[len(v2cut):]))
    vls_cut_ = np.array([v.ls    for v in m.getVertices()])
    vls_cut  = np.hstack((vls, vls_cut_[len(v2cut):]))
    v2cut_added = np.hstack((v2cut, np.arange(len(xy), len(xy) + len(xy_cut_) - len(v2cut) ).astype(v2cut.dtype) ))
    tris2v_cut = np.vstack((np.delete(tris2v, tris2cut, axis=0), v2cut_added[tris2v_cut_]))
    side_cut = compute_face_side(vls_cut[tris2v_cut])
    if returnparent :
       parent_id_neworder = np.array([f.id if not hasattr(f, "parent") else f.parent for f in m.getFaces()]) 
       parent_id_all = np.hstack( (np.full(len(tris2v)-len(tris2cut), -1), tris2cut[parent_id_neworder]))
       return xy_cut, vls_cut, tris2v_cut, side_cut, parent_id_all
    return xy_cut, vls_cut, tris2v_cut, side_cut
    
def multiLevelSetIsoCut(xy, tris, listoflsAtVertices, absfittol = 1.e-12, relfittol = 1.e-6):
    def getls(i): 
        def getlsi(v) : return v.ls[i]
        return getlsi
    def setls(i): 
        def setlsi(v,ls) : v.ls[i] = ls
        return setlsi    
    m = dmesh(xy, tris)
    nls = len(listoflsAtVertices)
    for v in m.getVertices() : v.ls  = np.zeros(nls)
    for f in m.getFaces() :    f.side = np.zeros(nls)
    for i in range(nls) :      set_levelset(m.getVertices(), listoflsAtVertices[i], setls(i))
    for i in range(nls) :
        def interpolatecallback(s, vn, e):
            vn.ls = np.zeros(nls)
            vn.ls[i] = 0.
            for k in set(range(nls)) - {i}:
                vn.ls[k] =e.v0.ls[k]*(1-s) + e.v1.ls[k]*s
        lscut(m, getls(i), setls(i), absfittol, relfittol, interpolatecallback = interpolatecallback)     
    xy_cut, tri_cut = m.getCoordAndConnectivity()
    ls_cut   = np.array([v.ls   for v in m.getVertices() ])
    side_cut = np.hstack ( [ compute_face_side(ls_cut[:,i][tri_cut]).reshape((-1,1)) for i in range(nls) ]  )
    return xy_cut, ls_cut, tri_cut, side_cut
    
def crackIsoCut(xy, tris, lsn, lst, r = 0.5, fit2vertices = True, absfittol = 1.e-12, relfittol = 1.e-6):
    def setLsn(v, lsn) : v.lsn = lsn
    def getLsn(v) : return v.lsn
    def setLst(v, lst) : v.lst = lst
    def getLst(v) : return v.lst
    m = dmesh(xy, tris)
    set_levelset(m.getVertices(), lsn, setLsn)
    set_levelset(m.getVertices(), lst, setLst)
    def interpolatecallback_lsncut(s, vn, e):
        vn.lsn = 0.
        vn.lst = e.v0.lst*(1-s) + e.v1.lst*s
    def interpolatecallback_lstcut(s, vn, e):
        vn.lst = 0.
        vn.lsn = e.v0.lsn*(1-s) + e.v1.lsn*s
        parent = e if not hasattr(e, "parent") else e.parent 
        vn.parent = parent
    def edgecallback(e, e0, e1):
        parent = e if not hasattr(e, "parent") else e.parent 
        vn = dmesh.getCommonVertex(e0, e1)
        vn.parent, e0.parent, e1.parent  = [parent]*3
    def facecallback(f, f0, f1):
        parent = f.id if not hasattr(f, "parent") else f.parent
        e = dmesh.getCommonEdge(f0,f1)
        f0.parent, f1.parent, e.parent  = [parent]*3
    lscut(m, getLsn, setLsn, absfittol, relfittol, interpolatecallback_lsncut, edgecallback, facecallback)     
    lscut(m, getLst, setLst, absfittol, relfittol, interpolatecallback_lstcut, edgecallback, facecallback)     
    xy_cut, tri_cut = m.getCoordAndConnectivity()
    lsn_cut = np.array([v.lsn for v in m.getVertices() ])
    lst_cut = np.array([v.lst for v in m.getVertices() ])
    siden_cut = compute_face_side(lsn_cut[tri_cut])
    sidet_cut = compute_face_side(lst_cut[tri_cut])
    parent_id = np.array([-1 if not hasattr(f, "parent") else f.parent for f in m.getFaces()])
    return xy_cut, lsn_cut, lst_cut, tri_cut, parent_id, siden_cut, sidet_cut

if __name__== '__main__': 
    class profile(): 
        import cProfile
        doprofile = True
        pr = cProfile.Profile()
        def enable():  
            if profile.doprofile : profile.pr.enable()
        def disable(): 
            if profile.doprofile :  profile.pr.disable()
        def print_stats(): 
            if profile.doprofile : profile.pr.print_stats(sort='cumulative')
            
    import gmsh
    import gmsh_meshes_examples as gmshmesh
    import gmsh_post
    

    testFitToVertex     = False
    testLevelSetIsoCut =  True
    testMultiLevelSetIsoCut = False
    testCrackIsoCut = False
    element_size = 0.0025
    #element_size = 0.02
    
    popupgmsh   = True
    profile.doprofile   = True
    
    
            
    
    gmsh.initialize()             
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)
    
    def phi1(xy):
        x = np.atleast_2d(xy)[:,0]
        y = np.atleast_2d(xy)[:,1]
        return np.sqrt(x**2+y**2 )- 0.5
    
    def phi2(xy):
        x = np.atleast_2d(xy)[:,0]
        y = np.atleast_2d(xy)[:,1]
        return np.sqrt(x**2+y**2 )- 0.7
    
    def phi3(xy):
        x = np.atleast_2d(xy)[:,0]
        return x- 0.3  
    
    def lsn(xy) : 
        y = np.atleast_2d(xy)[:,1]
        return y-0.5
    
    xl = -0.5
    xr =  0.5
    def lst(xy) : 
        x = np.atleast_2d(xy)[:,0]
        return np.where(x<(xl+xr)/2., xl-x, x-xr )
       
    
    
    modelname = gmshmesh.generate_L_in3parts(element_size)
    xy_appro = gmsh.model.mesh.getNodes()[1].reshape((-1,3))[:,:2]
    tris_appro = gmsh.model.mesh.getElementsByType(2)[1].reshape((-1,3))-1
    print('nb elements =', len(tris_appro))
    print('nb vertices =', len(xy_appro))
    
    
    if testFitToVertex :
        absfittol = 1.e-6
        relfittol = 0.3
        # very small test to check implem. result should be ('fit', [1.,2.,0.,0.])
        tris = np.array([[0,1,2],[2,1,3]])
        vls  = np.array([-0.01,2., 0.01,-0.01])
        f2v = tris
        e2v, __ =  onelevel._create_edges_from_tris(f2v)
        v2e_index, v2e = onelevel._upwardAdjacencies(e2v)
        v2v_index, v2v = onelevel._vertices2vertices(e2v, v2e_index, v2e)
        fit_to_vertices_old_v2(vls, v2v_index, v2v, absfittol, relfittol)
        print('v2v_index', v2v_index)
        print('v2v', v2v)
        print('fit', vls)
        
        print('current version')
        fit_to_vertices(vls, tris, absfittol, relfittol)
        print('fit', vls)
    #if 0:     
        
        def getls(v) : return v.ls
        def setls(v, ls) : v.ls = ls
       
        vls =  phi1(xy_appro)
        gmsh_post.listPlotScalarNodalField(xy_appro, tris_appro, vls, viewname = "ls_before_fit", IntervalsType = 3, NbIso=2, Range = [-1.,1.])
 
        m = dmesh(xy_appro, tris_appro)       
        set_levelset(m.getVertices(), vls, setls)
        fit_to_vertices_old(m.getVertices(), getls, setls, absfittol, relfittol)
        xy_cut, tris_cut = m.getCoordAndConnectivity()
        ls_cut = np.array([getls(v) for v in m.getVertices()])
        gmsh_post.listPlotScalarNodalField(xy_cut, tris_cut, ls_cut, viewname = "ls_after_fit_old", IntervalsType = 3, NbIso=2, Range = [-1.,1.])
        
        vls =  phi1(xy_appro)
        f2v = tris_appro
        e2v, __ =  onelevel._create_edges_from_tris(f2v)
        v2e_index, v2e = onelevel._upwardAdjacencies(e2v)
        v2v_index, v2v = onelevel._vertices2vertices(e2v, v2e_index, v2e)
        vls =  phi1(xy_appro)
        fit_to_vertices_old_v2(vls, v2v_index, v2v, absfittol, relfittol)
        gmsh_post.listPlotScalarNodalField(xy_appro, tris_appro, vls, viewname = "ls_after_fit_oldv2", IntervalsType = 3, NbIso=2, Range = [-1.,1.])
        profile.disable()
        
        vls =  phi1(xy_appro)
        f2v = tris_appro
        profile.enable()
        fit_to_vertices(vls, f2v, absfittol, relfittol)
        profile.disable()
        gmsh_post.listPlotScalarNodalField(xy_appro, tris_appro, vls, viewname = "ls_after_fit", IntervalsType = 3, NbIso=2, Range = [-1.,1.])
        
    if testLevelSetIsoCut :
        profile.enable() 
        xy, phi, tris, side, parent_id = levelSetIsoCut2(xy_appro, tris_appro, phi1(xy_appro), absfittol = 0., relfittol = 0.002, returnparent = True)
        profile.disable()
        gmsh_post.listPlotScalarNodalField(xy, tris, phi, viewname = "isocut_ls", IntervalsType = 3, NbIso=10)
        gmsh_post.listPlotScalarElementField(xy, tris, side, viewname = "isocut_side", IntervalsType = 3, NbIso=10)
        gmsh_post.listPlotScalarElementField(xy_appro, tris_appro, np.arange(0,len(tris_appro)), viewname = "face_id", IntervalsType = 3, NbIso=10)
        gmsh_post.listPlotScalarElementField(xy, tris, parent_id, viewname = "parentface_id", IntervalsType = 3, NbIso=10)
         
#        profile.enable() 
#        xy, phi, tris, side = levelSetIsoCut(xy_appro, tris_appro, phi1(xy_appro), absfittol = 0., relfittol = 0.002)
#        profile.disable()
#        gmsh_post.listPlotScalarNodalField(xy, tris, phi, viewname = "isocut_ls", IntervalsType = 3, NbIso=10)
#        gmsh_post.listPlotScalarElementField(xy, tris, side, viewname = "isocut_side", IntervalsType = 3, NbIso=10)
#    
    if testMultiLevelSetIsoCut :
        lss_xy = [phi1(xy_appro), phi2(xy_appro), phi3(xy_appro)]
        profile.enable() 
        xy, mphi, tris, mside = multiLevelSetIsoCut(xy_appro, tris_appro, lss_xy, absfittol = 0., relfittol = 0.02)
        profile.disable()
        for i in range(len(lss_xy)) :
            gmsh_post.listPlotScalarNodalField(xy, tris, mphi[:,i], viewname = "multi_isocut_ls_{}".format(i), IntervalsType = 3, NbIso=10)
            gmsh_post.listPlotScalarElementField(xy, tris, mside[:,i], viewname = "multi_isocut_ls_{}".format(i), IntervalsType = 3, NbIso=10)
    
    if testCrackIsoCut :
        profile.enable()
        xy_cut, lsn_cut, lst_cut, tri_cut, parent_id, siden_cut, sidet_cut =  crackIsoCut(xy_appro, tris_appro, lsn(xy_appro), lst(xy_appro), r = 0.5, absfittol = 1.e-12, relfittol = 1.e-6)
        profile.disable()
        gmsh_post.listPlotScalarNodalField(xy_cut, tri_cut, lsn_cut, viewname = "crackisocut_lsn", IntervalsType = 3, NbIso=10)
        gmsh_post.listPlotScalarElementField(xy_cut, tri_cut, siden_cut, viewname = "crackisocut_lsn_side", IntervalsType = 3, NbIso=10)
        gmsh_post.listPlotScalarNodalField(xy_cut, tri_cut, lst_cut, viewname = "crackisocut_lst", IntervalsType = 3, NbIso=10)
        gmsh_post.listPlotScalarElementField(xy_cut, tri_cut, sidet_cut, viewname = "crackisocut_lst_side", IntervalsType = 3, NbIso=10)
        gmsh_post.listPlotScalarElementField(xy_appro, tris_appro, np.arange(0,len(tris_appro)), viewname = "face_id", IntervalsType = 3, NbIso=10)
        gmsh_post.listPlotScalarElementField(xy_cut, tri_cut, parent_id, viewname = "parentface_id", IntervalsType = 3, NbIso=10)
        
        
        
    if popupgmsh : gmsh.fltk.run()
    gmsh.finalize()

    profile.print_stats()
    