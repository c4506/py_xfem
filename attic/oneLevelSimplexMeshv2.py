#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20 16:26:58 2023

@author: nchevaug
"""

import time
import numpy as np

class dmesh():
    class vertex():
        def __init__(self, i, xyz):
            self.id = i
            self.xyz = xyz
            self.edges = set()
        def __lt__(self, other):
            return self.id < other.id
        def addedge(self, e):
            self.edges.add(e)
        def removeedge(self, e):
            self.edges.remove(e)
            
    class edge():
        def __init__(self, v0, v1):
            self.vertices = (v0,v1) if v0 < v1 else (v1, v0)
            #self.hash = hash(self.getKey())
            self.faces = set()
        def addface(self, f):
            self.faces.add(f)
        def removeface(self, f):
            self.faces.remove(f)
        def print_vid(self):
            v0, v1 = self.vertices
            print(v0.id, v1.id)
        def getKey(self):
            return self.vertices
        def __hash__(self):
            #return self.hash
            return hash(self.getKey())
        def __eq__(self, other):
            return self.vertices  == other.vertices
        def __lt__(self, other):
            return self.vertices  < other.vertices
        
    class face():
        def __init__(self, fid, e0, e1, e2):
            check = False
            if check :
                dmesh.face.getCommonVertex(e0,e1)
                dmesh.face.getCommonVertex(e1,e2)
                dmesh.face.getCommonVertex(e2,e0)
            self.edges = (e0, e1, e2)
            self.id   = fid
            #self.hash = hash(self.getKey())
        def __hash__(self):
            #return self.hash
            return hash(self.getKey())
        def __eq__(self, other):
            return self.edges == other.edges
        def __lt__(self, other):
            return self.edges() < other.edges
        def print_vid(self):
            v0, v1, v2 = self.vertices()
            print(v0.id, v1.id, v2.id)
        def getKey(self):
            return self.edges
        def getCommonVertex(e0, e1):
            v0e0, v1e0  = e0.vertices
            v0e1, v1e1  = e1.vertices
            if v0e0 == v0e1 : return v0e0
            if v0e0 == v1e1 : return v0e0
            if v1e0 == v0e1 : return v1e0
            if v1e0 == v1e1 : return v1e0
            raise
            
        def vertices(self):
            e0, e1, e2  = self.edges
            v0 = dmesh.face.getCommonVertex(e2, e0)
            v1 = dmesh.face.getCommonVertex(e0, e1)
            v2 = dmesh.face.getCommonVertex(e1, e2)
            return [v0, v1, v2]
        
    def insert_vertex(self, xyz):
        vn = dmesh.vertex(self.nextvid, xyz)
        self.vertices.append(vn)
        self.nextvid += 1
        return vn
        
    def insert_edge(self, v0, v1):
        etrial = dmesh.edge(v0,v1)
        key = etrial.getKey()
        e      = self.edges.get(key)
        if e is None :
            e = etrial
            self.edges.update({key : e})
            v0.addedge(e)
            v1.addedge(e)
        return e
    def insert_face(self, fid, e0, e1, e2):
        ftrial = dmesh.face(fid, e0, e1, e2)
        key = ftrial.getKey()
        f = self.faces.get(key)
        if  f is None:
            f = ftrial
            e0.addface(f)
            e1.addface(f)
            e2.addface(f)
            self.faces.update({key : f})
        return f
    
    def _delete_face(self, f):
        for e in f.edges :  e.removeface(f)
        self.faces.pop(f.getKey())
        
    def _delete_edge(self,e):
        if len(e.faces) > 0  : raise
        for v in e.vertices :  v.removeedge(e)
        self.edges.pop(e.getKey())
        
    def __init__(self, xyz, tris):       
        self.vertices = [dmesh.vertex(i, xyz) for i, xyz in enumerate(xyz)]
        self.edges =   dict()
        self.faces    = dict()
        for fid, t in enumerate(tris) :
            v0, v1, v2 = self.vertices[t[0]], self.vertices[t[1]], self.vertices[t[2]]
            e0 = self.insert_edge(v0,v1)
            e1 = self.insert_edge(v1,v2)
            e2 = self.insert_edge(v2,v0)
            self.insert_face(fid, e0, e1, e2)
        self.nextvid = len(self.vertices)
        self.nextfid = len(self.faces)
        
    def getCoordAndConnectivity(self):
        v2id = { v:i for i, v in enumerate (self.vertices)}
        xyz  = np.array([ v.xyz for v in self.vertices ])
        tris = np.array( [ [ v2id[v] for v in f.vertices()] for f in self.getFaces()])
        return xyz, tris
    
    def getEdges(self):
        return self.edges.values()
    
    def getFaces(self):
        return self.faces.values()
    
    def split_edge(self, e, xyz, callback = None):
        v0, v1 = e.vertices
        vn = self.insert_vertex(xyz)
        e0 = self.insert_edge(v0, vn)
        e1 = self.insert_edge(v1, vn)
        ftodel = [f for f in e.faces]
        for f in e.faces:
             fie = 0 if f.edges[0]==e else  1 if f.edges[1]==e else 2
             fe0, fe1, fe2 = f.edges[fie], f.edges[(fie+1)%3], f.edges[(fie+2)%3]
             fv0 = dmesh.face.getCommonVertex(fe2, fe0)
             vc = dmesh.face.getCommonVertex(fe1, fe2)
             fen = self.insert_edge(vn,vc)
             if fv0 == v0:
                 f0e0, f0e1, f0e2 = e0, fen, fe2
                 f1e0, f1e1, f1e2 = e1, fe1, fen
             else :
                 f0e0, f0e1, f0e2 = e1, fen, fe2
                 f1e0, f1e1, f1e2 = e0, fe1, fen
             f0 = self.insert_face( self.nextfid, f0e0, f0e1, f0e2)
             self.nextfid +=1
             f1 = self.insert_face(self.nextfid, f1e0, f1e1, f1e2)
             self.nextfid +=1
        for f in ftodel :self._delete_face(f)
        self._delete_edge(e)
        return vn
   