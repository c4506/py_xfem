#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 18 10:00:10 2023

@author: nchevaug
"""
import numpy as np

def nodes2triangles(triangles2vid):
    nv = np.amax(triangles2vid)+1
    v2tri = [None]*int(nv)
    for (tid, t) in enumerate(triangles2vid) :
        for i in [0,1,2] :
            a = v2tri[t[i]]
            v2tri[t[i]] = {tid} if a is None else  a.union({tid})
    return v2tri

if __name__ == '__main__':
    
    tri2vid = np.array([[0,1,2],[2,1,3]])
    print ('t2v \n',tri2vid)
    print('v2t \n', nodes2triangles(tri2vid))