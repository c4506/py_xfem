#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 13:54:46 2024

@author: nchevaug
"""

    
class xRidgeSpaceP1():
    def __init__(self, name, mesh_cut, ls, ls_cut, rmap = None, version = 6, dosvd = False, dodebugplot = False):
        self.name    = name
        self.rmap    = meshCutDataToRemapHelper(mesh_cut) if rmap is None else rmap
        self.enrich  = ridge(name+'_enrich', self.rmap, ls[mesh_cut.tris], ls_cut[mesh_cut.new_tris])
        self.base    = FEMSpaceScalarP1(name+'_base', mesh_cut.xy, mesh_cut.tris, tris_map = self.rmap.parent_mapping)
        cut_elem     = self.rmap.approNEQeval
        cut_vertices = np.unique(mesh_cut.tris[cut_elem])
        self.frmap = mapping.remapHelperFiltered(self.rmap.index_parent2sons, self.rmap.sons, self.rmap.parent_mapping, self.rmap.son_mapping, cut_elem)
        self.dosvd = dosvd
        if dodebugplot : gp.listPlotScalarField(mesh_cut.xy[mesh_cut.tris[cut_elem]], np.ones(3*len(cut_elem)), viewname ='RidgeSpace_support', IntervalsType = 3, NbIso = 10)
        if version == 0 :
            cut_vertices = np.unique(mesh_cut.tris[cut_elem])
            enriched_vertices = cut_vertices
            enriched_vertices2v = sp.sparse.csc_array( ( np.ones(len(enriched_vertices)), enriched_vertices, np.arange(len(enriched_vertices)+1)), shape = (len(mesh_cut.xy), len(enriched_vertices)) )
            self.enr2v =  enriched_vertices2v 
            if dodebugplot : gp.listPlotPointScalar(mesh_cut.xy[enriched_vertices], np.arange(len(enriched_vertices)), viewname= 'Ridgespace_V0_enrichedvertices' )
        if version == 1 :
            # one mode only
            cut_vertices = np.unique(mesh_cut.tris[cut_elem])
            enriched_vertices2v = sp.sparse.csc_array( ( np.ones(len(cut_vertices)), cut_vertices, np.array([0, len(cut_vertices)])), shape = (len(mesh_cut.xy), 1) )
            self.enr2v =  enriched_vertices2v 
            if dodebugplot : gp.listPlotPointScalar(mesh_cut.xy[enriched_vertices], np.ones(len(enriched_vertices)), viewname= 'Ridgespace_V1_enrichedvertices' )
        if version == 2 :
            from oneLevelSimplexMesh import _upwardAdjacencies
            cut_edges2v = mesh_cut.cut_edge_vertices
            vertices2cutedge_start, vertices2cutedge = _upwardAdjacencies(cut_edges2v)
            vertices2nbcutedge = vertices2cutedge_start[1:]-vertices2cutedge_start[:-1]
            isolated_vertices = np.where(vertices2nbcutedge == 1)[0]
            ev = mesh_cut.cut_edge_vertices
            edge_not_cut = vertices2cutedge[vertices2cutedge_start[isolated_vertices]] 
            enriched_edges = np.setdiff1d(np.arange(len(ev)), edge_not_cut)
            enriched_edges2v = ev[enriched_edges]
            __, enriched_edges_index = np.unique(enriched_edges2v[:,0], return_index = True)
            enriched_edges = enriched_edges[enriched_edges_index]
            enriched_vertices2v = sp.sparse.csc_array( ( np.ones(len(isolated_vertices)), isolated_vertices, np.arange(len(isolated_vertices)+1)), shape = (len(mesh_cut.xy), len(isolated_vertices)) )
            enriched_edges2v    = sp.sparse.csc_array( ( np.ones(2*len(enriched_edges)), ev[enriched_edges].flatten(), np.arange(0,2*len(enriched_edges)+1,2)), shape = (len(mesh_cut.xy), len(enriched_edges)) )
            self.enr2v = sp.sparse.hstack((enriched_vertices2v, enriched_edges2v))
            if dodebugplot :
                 gp.listPlotLineScalar(mesh_cut.xy[ev], np.ones(len(ev)*2).reshape((-1,1,2)), viewname= 'Ridgespace_V1_cut_edges' )
                 gp.listPlotPointScalar(mesh_cut.xy[isolated_vertices], np.ones(len(isolated_vertices)), viewname= 'Ridgespace_V1_enriched_vertices' )
                 gp.listPlotLineScalar(mesh_cut.xy[ev[enriched_edges] ], np.ones(len(enriched_edges)*2).reshape((-1,1,2)), viewname= 'Ridgespace_V2_enriched_edge' )
        if version == 3 :
            from oneLevelSimplexMesh import _upwardAdjacencies
            cut_edges2v  = mesh_cut.cut_edge_vertices
            cut_vertices = np.unique(cut_edges2v.flatten())
            cut_vertices_in  = cut_vertices[ls[cut_vertices] < 0]
            #cut_vertices_out = cut_vertices[ls[cut_vertices] > 0]
            vertices2cutedge_start, vertices2cutedge = _upwardAdjacencies(cut_edges2v)
            #vertices2nbcutedges  = vertices2cutedge_start[cut_vertices_out]-vertices2cutedge_start[cut_vertices_out+1]
            #enriched_edges   = vertices2cutedge[vertices2cutedge_start[cut_vertices_out]]
            enriched_edges   = vertices2cutedge[vertices2cutedge_start[cut_vertices_in]]
            enriched_vertices = np.setdiff1d(cut_vertices, np.unique(cut_edges2v[enriched_edges].flatten() ))
            enriched_vertices2v = sp.sparse.csc_array( ( np.ones(len(enriched_vertices)), enriched_vertices, np.arange(len(enriched_vertices)+1)), shape = (len(mesh_cut.xy), len(enriched_vertices)) )
            enriched_edges2v    = sp.sparse.csc_array( ( np.ones(2*len(enriched_edges)), cut_edges2v[enriched_edges].flatten(), np.arange(0,2*len(enriched_edges)+1,2)), shape = (len(mesh_cut.xy), len(enriched_edges)) )
            self.enr2v = sp.sparse.hstack((enriched_vertices2v, enriched_edges2v))
            if dodebugplot :
                gp.listPlotLineScalar(mesh_cut.xy[cut_edges2v], np.ones(len(cut_edges2v)*2).reshape((-1,1,2)), viewname= 'Ridgespace_V2_cut_edges' )
                gp.listPlotPointScalar(mesh_cut.xy[enriched_vertices], np.ones(len(enriched_vertices)), viewname= 'Ridgespace_V2_enriched_vertices' )
                gp.listPlotLineScalar(mesh_cut.xy[cut_edges2v[enriched_edges] ], np.ones(len(enriched_edges)*2).reshape((-1,1,2)), viewname= 'Ridgespace_V3_enriched_edge' )     
        #print('RidgeSpace size ', self.enr2v.shape[1])
        if version == 4 :
            from oneLevelSimplexMesh import _upwardAdjacencies
            cut_edges2v  = mesh_cut.cut_edge_vertices
            #print(cut_edges2v)
            cut_vertices = np.unique(cut_edges2v.flatten())
            #print(cut_vertices)
            vertices2cutedge_start, vertices2cutedge = _upwardAdjacencies(cut_edges2v)
            #print(vertices2cutedge_start)
            vertices2nbedges = vertices2cutedge_start[1:] - vertices2cutedge_start[:-1]
            #print(vertices2nbedges)
            enriched_vertices             = np.where(vertices2nbedges > 1)[0]
            print(len(enriched_vertices))
            print(len(cut_vertices))
            self.enr2v= sp.sparse.csc_array( ( np.ones(len(enriched_vertices)), enriched_vertices, np.arange(len(enriched_vertices)+1)), shape = (len(mesh_cut.xy), len(enriched_vertices)) )
            if dodebugplot :
                gp.listPlotLineScalar(mesh_cut.xy[cut_edges2v], np.ones(len(cut_edges2v)*2).reshape((-1,1,2)), viewname= 'Ridgespace_V4_cut_edges' )
                gp.listPlotPointScalar(mesh_cut.xy[enriched_vertices], np.ones(len(enriched_vertices)), viewname= 'Ridgespace_V4_enriched_vertices' )
                #print('RidgeSpace size ', self.enr2v.shape[1])
        if version == 5 :
            from oneLevelSimplexMesh import _upwardAdjacencies
            cut_edges2v  = mesh_cut.cut_edge_vertices
            cut_vertices = np.unique(cut_edges2v.flatten())
            vertices2cutedge_start, vertices2cutedge = _upwardAdjacencies(cut_edges2v)
            vertices2nbedges = vertices2cutedge_start[1:] - vertices2cutedge_start[:-1]
            dof2vertices = { i : [i] for i in cut_vertices}
            for v in  np.where(vertices2nbedges> 2)[0] :
                v2edges = vertices2cutedge[vertices2cutedge_start[v] : vertices2cutedge_start[v+1]]
                v2vs = np.setdiff1d( np.unique(cut_edges2v[v2edges].flatten()), [v])
                vdiscards = v2vs[np.where( vertices2nbedges[v2vs] == 1)]
                vkeep     = v2vs[np.where( vertices2nbedges[v2vs] != 1)]
                #print('k,d', vkeep, vdiscards)
                for vd in vdiscards : dof2vertices.pop(vd, None)
                for vk in vkeep     : dof2vertices[vk] += list(vdiscards)
            nbenr = len(dof2vertices)
            #print(dof2vertices)
            lines      = np.array(sum([vs for vs in dof2vertices.values()], []))
            column_ptr = np.cumsum(np.array(sum([ [len(vs)] for vs in dof2vertices.values()], [0])))
            self.enr2v= sp.sparse.csc_array( ( np.ones(len(lines)), lines, column_ptr), shape = (len(mesh_cut.xy), nbenr) )
#            if dodebugplot :
#                gp.listPlotLineScalar(mesh_cut.xy[cut_edges2v], np.ones(len(cut_edges2v)*2).reshape((-1,1,2)), viewname= 'Ridgespace_V4_cut_edges' )
#                gp.listPlotPointScalar(mesh_cut.xy[enriched_vertices], np.ones(len(enriched_vertices)), viewname= 'Ridgespace_V4_enriched_vertices' )
#                #print('RidgeSpace size ', self.enr2v.shape[1])
            print('size space', len(dof2vertices), len(cut_vertices))
           
        if version == 6 :
            from oneLevelSimplexMesh import _upwardAdjacencies
            cut_edges2v  = mesh_cut.cut_edge_vertices
            cut_vertices = np.unique(cut_edges2v.flatten())
            vertices2cutedge_start, vertices2cutedge = _upwardAdjacencies(cut_edges2v)
            vertices2nbedges = vertices2cutedge_start[1:] - vertices2cutedge_start[:-1]
            enriched_vertices = np.where(vertices2nbedges==1)[0]
            enriched_edges    = np.arange(len(cut_edges2v))[np.in1d(cut_edges2v[:,0], enriched_vertices, invert = True) * np.in1d(cut_edges2v[:,1], enriched_vertices, invert = True)]
            data   = np.ones(len(enriched_vertices)+2*len(enriched_edges))
            column_ptr = np.hstack((np.arange(len(enriched_vertices)), np.arange(len(enriched_edges)+1)*2 + len(enriched_vertices) ))
            lines   = np.hstack((enriched_vertices, cut_edges2v[enriched_edges].flatten()))
            #print('nb enr v', enriched_vertices)
            #print('nb enr edges',  cut_edges2v[enriched_edges])
            
            self.enr2v= sp.sparse.csc_array( ( data, lines, column_ptr), shape = (len(mesh_cut.xy),  len(column_ptr)-1 ))
            print('nedgecut ', len(cut_edges2v), 'nenr', len(column_ptr) -1, 'nvenr', len(enriched_vertices), 'neenr', len(enriched_edges))
            if dodebugplot :
                gp.listPlotLineScalar(mesh_cut.xy[cut_edges2v[enriched_edges]], np.ones(len(enriched_edges)*2).reshape((-1,1,2)), viewname= 'Ridgespace_V6_cut_edges' )
                gp.listPlotPointScalar(mesh_cut.xy[enriched_vertices], np.ones(len(enriched_vertices)), viewname= 'Ridgespace_V6_enriched_vertices' )
                
        if version == 7 :
            from oneLevelSimplexMesh import _upwardAdjacencies
            cut_edges2v = mesh_cut.cut_edge_vertices
            data        = np.ones(2*len(cut_edges2v))
            column_ptr  = np.arange(len(cut_edges2v)+1)*2
            lines       = cut_edges2v.flatten()
            
            self.enr2v= sp.sparse.csc_array( ( data, lines, column_ptr), shape = (len(mesh_cut.xy),  len(column_ptr)-1 ))
            print('nedgecut ', len(cut_edges2v), 'nenr', len(column_ptr) -1)
            if dodebugplot :
                gp.listPlotLineScalar(mesh_cut.xy[cut_edges2v], np.ones(len(cut_edges2v)*2).reshape((-1,1,2)), viewname= 'Ridgespace_V7_enriched_edges' )
         
        if version == 8 :
            from oneLevelSimplexMesh import _upwardAdjacencies
            cut_edges2v = mesh_cut.cut_edge_vertices
            vertices2cutedge_start, vertices2cutedge = _upwardAdjacencies(cut_edges2v)
            #vertices2nbedges = vertices2cutedge_start[1:] - vertices2cutedge_start[:-1]
            itype = cut_edges2v.dtype
            lines = np.zeros(0, dtype = itype)
            data  = np.zeros(0)
            #print(cut_edges2v.dtype)
            column_ptr   = np.zeros(len(cut_edges2v) +1, dtype = cut_edges2v.dtype)
            for i, (v0, v1) in enumerate(cut_edges2v) :
               
                
               # print(vertices2cutedge[vertices2cutedge_start[v0]:vertices2cutedge_start[int(v0+int(1))]])
                v0neib = cut_edges2v[vertices2cutedge[vertices2cutedge_start[v0]:vertices2cutedge_start[int(v0+1)]]].flatten()
                v1neib = cut_edges2v[vertices2cutedge[vertices2cutedge_start[v1]:vertices2cutedge_start[int(v1+1)]]].flatten()
                #print(v0neib)
                #print(v1neib)
                
                eneibv =  np.unique(np.hstack( (v0neib, v1neib ) ))
                edata  = -np.ones(len(eneibv))
                #print(v0, v1, eneibv)
                edata[eneibv == v0] = 1.
                edata[eneibv == v1] = 1. 
                data  = np.hstack((data, edata))
                lines = np.hstack((lines, eneibv))
                column_ptr[i+1] = column_ptr[i] + len(eneibv)
            
            #print(data)
            self.enr2v = sp.sparse.csc_array( ( data, lines, column_ptr), shape = (len(mesh_cut.xy),  len(column_ptr)-1 ))
            print('nedgecut ', len(cut_edges2v), 'nenr', len(column_ptr) -1)
            if dodebugplot :
                gp.listPlotLineScalar(mesh_cut.xy[cut_edges2v], np.ones(len(cut_edges2v)*2).reshape((-1,1,2)), viewname= 'Ridgespace_V8_enriched_edges' )
  
        
    def evalOp(self, evaltrisuv, return_evaltrisxy = False, raw_data = False):
        nodes_uv  = quad.T3_nodes().uv
        BaseAtNodes = self.base.evalOp(nodes_uv, self.rmap, self.frmap, return_evaltrisxy, raw_data = True)
        EnrAtNodes  = (self.enrich.eval(nodes_uv, frmap=self.frmap, raw_data = True)) #.reshape((-1,1))
        NAtNodes    = EnrAtNodes[:,:, np.newaxis]*BaseAtNodes
        nshape = FEMSpaceScalarP1.lenN
        if np.all(nodes_uv == evaltrisuv) : N = NAtNodes
        else :
            nbevaltri = NAtNodes.shape[0]
            N = NAtNodes.reshape((-1, len(nodes_uv))).reshape((len(nodes_uv),-1), order = 'F')
            N = FEMSpaceScalarP1.N(evaltrisuv).dot(N)
            N = N.reshape(( nbevaltri*len(evaltrisuv), -1), order = 'F')
        if raw_data : return N
        
        N_sparse = op2Sparse_rmap(N, self.base.size(), self.base.a2dofs, nshape, len(evaltrisuv), 1, self.rmap, self.frmap, sort = False)     
        N_sparse = N_sparse.dot(self.enr2v).tocsr()
        N_sparse.sort_indices()
        if self.dosvd :
            npts   = self.frmap.nbevals *len(evaltrisuv)
            shape =  (npts*1, self.base.size())
            col      = np.repeat(self.base.a2dofs[self.frmap.only_on_parents], self.frmap.appro2nbevals*len(evaltrisuv)*1, axis=0).flatten()  
            row_ptr =np.arange(0, (1*npts +1)*3, 3)
            N_svd   = sp.sparse.csr_array((N.flatten(), col, row_ptr), shape)
            N_svd_dense = N_svd.dot(self.enr2v).todense()
            svd = np.linalg.svd(N_svd_dense)
            #print('shape : ', N_svd_dense.shape, 'singularvarlues ridge N:', svd[0] , '\n', svd[1],  '\n', svd[2])
            print('shape : ', N_svd_dense.shape, 'singularvarlues ridge N:',  svd[1])
            
            #The following is musch slower than previous because we have all the empty rows !! 
            #print('shape : ', N_sparse.shape, 'singularvarlues ridge N:', np.linalg.svd(N_sparse.todense())[1])
        
        if return_evaltrisxy : return N_sparse, self.rmap.getEvalTris()
        return N_sparse
        
    def evalGradOp(self, evaltrisuv, return_evaltrisxy = False):
        dim = 2
        npt       = len(evaltrisuv)
        nodes_uv  = quad.T3_nodes().uv
        nshape = 3
        NRidge = self.evalOp(nodes_uv, raw_data = True)
        dNdx  = np.zeros( (self.frmap.nbevals, npt, dim, nshape) )  
        dNdx[self.frmap.noson2eval_index] = FEMSpaceScalarP1.gradN(evaltrisuv, self.frmap.parent_mapping.invF[self.frmap.approEQeval])
        dNdx[self.frmap.son2eval_index]   = FEMSpaceScalarP1.gradN(evaltrisuv, self.frmap.son_mapping.invF[self.frmap.sons])
        dNRidgedx = np.einsum( 'ijkl, ilm ->ijkm', dNdx, NRidge)
        dNRidgedx_sparse = op2Sparse_rmap(dNRidgedx, self.base.size(), self.base.a2dofs, nshape, npt, dim, self.rmap, self.frmap)
        dNRidgedx_sparse = dNRidgedx_sparse.dot(self.enr2v).tocsr()
        dNRidgedx_sparse.sort_indices()
        if False:
            print('singularvarlues ridge dNdx:', np.linalg.svd(dNRidgedx_sparse.todense())[1])
        if return_evaltrisxy : return dNRidgedx_sparse, self.rmap.getEvalTris()
        return dNRidgedx_sparse