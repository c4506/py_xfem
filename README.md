## PY_XFEM

PY_XFEM is a finite element library that I develop for educational and research purpose.

It is in heavy development stage.
For now, it can solve linear thermics, linear elasticity, and non-linear hyperelasticity problems on 2d domains.
It uses gmsh for the mesh and the post-processing.

It has some support for XFEM enrichment and mesh modification.

dependencies :
numpy, scipy, gmsh, scikit

Author : nicolas.chevaugeon@ec-nantes.fr