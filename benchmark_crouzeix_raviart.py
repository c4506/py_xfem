#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 14:58:41 2024

@author: nchevaug
"""
import sys
import getopt
import numpy as np
import scipy as sp
import gmsh
import gmsh_mesh_generator as mg
import gmsh_post_pro_interface as gp
import gmsh_mesh_model_interface as meshInterface
import quadrature as quad
import thermic_solver as ts
import mechanics_solver as ms
import exact_solutions as ex
import function_spaces as fs
import oneLevelSimplexMesh as sm
from execTools import Profile
from plot_tools import figax, savefig, show


def patch_test_elasticity(outdir: str):
    """ # patch test on a squared load by Dirchlet and then Neumann """
    lenght = 1.
    young = 12.2
    nu = 0.3
    ux = 0.1
    lamb, mu = ms.YoungPoisson2LambdaNu(young, nu)
    C = ms.planeStrainC2222CSR(lamb, mu)  # pylint: disable=C0103
    phys2dirichlet = {'left': {'x': 0.},
                      'bottom': {'y': 0}, 'right': {'x': ux}}

    e11 = ux/lenght
    s11 = 4*mu*(lamb+mu)/(lamb+2*mu)*e11
    stress_exact = np.array([[s11, 0.], [0., 0.]])

    model_name, __ = mg.square(length=lenght, relh=1.)
    mesh = sm.gmshModel2sMesh(model_name, phys2dirichlet.keys())
    trisxy = mesh.getTris2VerticesCoord()
    gmsh.fltk.initialize()
    for name, space_c, stab in zip(["P1", "P1NC", "P1NC"],
                                   [fs.FEMSpaceP1, fs.FEMSpaceP1NC, fs.FEMSpaceP1NC], [0., 0., 1.]):
        pb = ms.ElasticitySolver(mesh, C, physName2Dirichlet=phys2dirichlet,
                                 spaceConstructor=space_c, stab=stab)  # type: ignore
        u_dofs = pb.solve()
        __, u_trisxy = pb.postProDisplacement(u_dofs)
        __, stress_trisxy = pb.postProStress(u_dofs)
        error = np.linalg.norm(
            stress_trisxy - stress_exact[np.newaxis, np.newaxis])
        print(f"Dirichlet patch test, error = {error:.4e}")
        dfac = ux/np.max(u_trisxy)
        view = gp.listPlotFieldTri(trisxy, u_trisxy, P0=False, viewname="displacement",
                                   VectorType="Displacement", DisplacementFactor=dfac)
        print("save to " + outdir+"patchtest_"+name+"_stab_"+str(stab)+".pdf")
        for tag in gmsh.view.get_tags():
            gmsh.view.setVisibilityPerWindow(tag, 0)
        gmsh.view.setVisibilityPerWindow(view, 1)
        gmsh.write(outdir+"patchtest_"+name+"_stab_"+str(stab)+".pdf")
    gmsh.fltk.finalize()
    # gp.listPlotFieldTri(trisxy, stress_trisxy, P0=True, viewname="stress")

    # phys2dirichlet = {'left': {'x':0.}, 'bottom': {'y': 0}}
    # phys2neumann = {'right': {'x':s11}}
    # pb = ms.elasticitySolver(mesh, C,
    #                         physName2Dirichlet=phys2dirichlet,
    #                         physName2Neumann=phys2neumann,
    #                         spaceConstructor=fs.FEMSpaceP1)
    # e22 = -lamb*e11/(lamb+2*mu)
    # strain_exact = np.array([[e11, 0.], [0., e22]])
    # U = pb.solve()
    # ___, strain = pb.postProStrain(U)
    # error = np.linalg.norm(strain - strain_exact[np.newaxis, np.newaxis] )
    # print(f"Dirichlet/Neumann patch test, error = {error:.4e}")


def convergence_thermic(
    fem_space_name="P1C",
    exact_sol_constructor=ex.laplacianCircularInclusionQuad,
    start_hratio=0.5,
    steps=np.arange(2, 6),
    quad_for_error=quad.T3_gauss(2),
    samp_points_max_val_err=quad.T3_gauss(0).uv,  # quad.T3_midedges().uv,
    samp_points_for_max_grad_err=quad.T3_gauss(0).uv,  # quad.T3_midedges().uv,
    verbose=True,
):
    """Convergence analysis fo static thermal analysis"""
    # Conductivity :
    k = 1.0
    # radius of the disk separting conductivities.
    ri = 0.4
    # linear solver
    solver = "cholmod"
    # list to store results for each elementsize. h is computed as sqrt(S/ntri),
    # where S is the surface of the domain, ntri the number of triangle in the mesh
    res_keys = ["h", "err_l2", "err_h1", "err_maxval", "err_maxgrad", "rcond"]
    res_dic = {k: np.zeros(steps.size) for k in res_keys}
    # definition of the exact solution
    exactsol = exact_sol_constructor(k, k, ri=ri, re=np.sqrt(2))

    def exafun(xy):
        return exactsol.exactSolution(xy)

    def exagrad(xy):
        return exactsol.exactGradient(xy)

    # if np.isscalar(exactsol.source):
    #     source = exactsol.source
    # elif callable(exactsol.source):
    #     fun = exx
    #     def calsource(xy):
    #         return exactsol.source(xy)
    #     source = calsource
    # else:
    #     raise ValueError("Can't interprete the source term")
    if verbose:
        print("Start convergence Analysis using space ", fem_space_name, ".")
    for i, p in enumerate(steps):
        element_size = start_hratio**p
        if verbose:
            print("  elementSize ", element_size, ".")
        gmsh.option.setNumber("Mesh.SurfaceEdges",
                              1 if element_size > 0.05 else 0)
        model_name, physical_dict = mg.square(relh=element_size)
        nv = meshInterface.nb_vertices(model_name)
        nf = meshInterface.nb_triangles(model_name)
        if verbose:
            print("    Model ", model_name, "constructed.")
            print("    Nb elem: ", nf, "Nb node:", nv)
        physname_to_dirichlet_value = {
            "left": exafun,
            "right": exafun,
            "bottom": exafun,
            "top": exafun,
        }
        pbfem = ts.ThermicSolver(
            model_name,
            physical_dict,
            k,
            physname_to_dirichlet_value,
            source=exactsol.source,
            space=fem_space_name,
        )
        if verbose:
            print("    Start Solve")
        dof_values, res_dic["rcond"][i] = pbfem.solve(
            solver=solver, compute_rcond=True)
        if verbose:
            print("    End Solve")
            print("    Start Post-Processing")
        trisxy, tris_val = pbfem.postproVal(dof_values)
        trisxy, tris_grad = pbfem.postproGrad(dof_values)
        gp.listPlotFieldTri(trisxy, exafun(trisxy),
                            P0=False, viewname="T_" + "exact")
        gp.listPlotFieldTri(trisxy, tris_val, P0=False,
                            viewname="T_" + fem_space_name)
        gp.listPlotFieldTri(
            trisxy,
            tris_grad,
            P0=True,
            viewname="GradT_" + fem_space_name,
            VectorType="Arrow",
        )
        res_dic["h"][i] = np.sqrt(4 / nf)
        res_dic["err_h1"][i] = pbfem.H1_error(
            dof_values, exagrad, quad_for_error)
        res_dic["err_l2"][i] = pbfem.L2_error(
            dof_values, exafun, quad_for_error)
        res_dic["err_maxval"][i] = pbfem.maxValError(
            dof_values, exafun, samp_points_max_val_err
        )
        res_dic["err_maxgrad"][i] = pbfem.maxGradError(
            dof_values, exagrad, samp_points_for_max_grad_err
        )
        if verbose:
            print("    End Post-Processing")
        gmsh.model.remove()
        model_name, physical_dict = mg.square(relh=0.55)
    return res_dic


def convergence_elasticity(min_refine_level=4, max_refine_level=7,
                           verbose=True, space=fs.FEMSpaceP1):
    """ Convergence Anlysis in 2d elasticity : plate with circular hole 

        note : gmsh must be initialized prior ro the call.
    """
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)
    gmsh.option.setNumber('General.Terminal', 0)

    length = 2.  # length of the square mesh
    r0 = 0.5
    young = 1.
    nu = 0.4
    lamb, mu = ms.YoungPoisson2LambdaNu(young, nu)
    elasticity_tensor = ms.planeStrainC2222CSR(lamb, mu)
    exact_sol = ex.LinearElasticity2dCircularHoleInfiniteSpace(
        young, nu, hyp="planestrain", r=r0)

    def exactu(xy):
        return exact_sol.displacement(xy)[..., 0]

    def exactv(xy):
        return exact_sol.displacement(xy)[..., 1]
    imposed = {'x': exactu, 'y': exactv}
    phys2dirichlet = {'bottom': imposed,
                      'right': imposed, 'top': imposed, 'left': imposed}
    steps = max_refine_level - min_refine_level + 1
    h = np.zeros(steps)
    error = np.zeros(steps)
    for i, refine_level in enumerate(range(min_refine_level, max_refine_level+1)):
        lc = 2**(-refine_level)
        model_name, groupe_name2phys_dim_tag = mg.hole_in_square(
            length=length, radius=r0, relh=lc)
        mesh = sm.gmshModel2sMesh(model_name, groupe_name2phys_dim_tag.keys())
        gmsh.option.setNumber("Mesh.SurfaceEdges",
                              1 if mesh.getNbVertices() < 1000 else 0)
        if verbose:
            nv, ne, nt = mesh.getNbVertices(), mesh.getNbEdges(), mesh.getNbTris()
            print(f"nv {nv:d}, ne {ne:d}, nf {nt:d}")
        pb = ms.ElasticitySolver(mesh, elasticity_tensor, physName2Dirichlet=phys2dirichlet,
                                 spaceConstructor=space, stab=2*mu*0.1)
        u_dofs = pb.solve()
        fem_stress = pb.postProStress(u_dofs)[1]
        trisxy = mesh.getTris2VerticesCoord()
        exact_stress = exact_sol.stress(trisxy)
        gp.listPlotFieldTri(trisxy, fem_stress, P0=True,
                            viewname='fem Stress', Range=[0., 3.])
        if refine_level == max_refine_level:
            gp.listPlotFieldTri(trisxy, exact_stress,
                                viewname='exact Stress', Range=[0., 3.])
        stress_err = pb.L2StressError(
            u_dofs, exact_sol.stress, quadrature=quad.T3_gauss(2))
        if verbose:
            print(lc, stress_err)
        h[i] = lc
        error[i] = stress_err

    def power_fun(x, a, alpha):
        return a*np.power(x, alpha)
    h1error_rate = sp.optimize.curve_fit(
        power_fun, h, error, p0=[1., 1.])[0][1]
    gmsh.model.remove()
    return h, error, h1error_rate


def convergence_analysis(outdir: str):
    """Crouzeiz Raviart benchmark : do convergence analysis for CRelemnt (P1NC)
    and compare it to classical element"""
    Profile.doprofile = False
    quad_for_err = quad.T3_gauss(3)  # quad.T3_nodes(), quad.T3_midedges()
    points_for_maxval_err = quad.T3_gauss(0).uv
    points_for_maxgrad_err = quad.T3_gauss(0).uv
    exact_sol_constr = (
        ex.laplacianCircularInclusionQuad
    )  # exact.laplacianCircularInclusionQuintic
    fem_spaces = ["P1C", "P1NC"]
    ax_keys = ["err_l2", "err_h1", "err_maxval", "err_maxgrad", "rcond"]
    # ax_dic = {k: plt.subplots(1, 1)[1] for k in ax_keys}
    fig_dic = {k: figax(xlabel='h', ylabel=k) for k in ax_keys}
    for fem_space in fem_spaces:
        print("### fem space: ", fem_space)
        res_dic = convergence_thermic(
            fem_space,
            exact_sol_constructor=exact_sol_constr,
            quad_for_error=quad_for_err,
            samp_points_max_val_err=points_for_maxval_err,
            samp_points_for_max_grad_err=points_for_maxgrad_err,
            verbose=False
        )
        print("|".join([f"{k:^12}" for k in res_dic.keys()]))
        print(
            " -----------------------------------------------------------------------------"
        )
        for i in range(res_dic["h"].size):
            l = [f" {v[i]:.4e} " for v in res_dic.values()]
            print("|".join(l))
        h = res_dic.pop("h")
        for (k, v), axfig in zip(res_dic.items(), fig_dic.values()):
            ax = axfig[1]
            ax.loglog(h, v, "o-", linewidth=2, label=fem_space)
    for name, (fig, ax) in fig_dic.items():
        ax.legend()
        savefig(fig, outdir+'conv_' + name+'.tikz')

    fig, ax_h1 = figax('h', 'H1 error')
    for space, name in zip([fs.FEMSpaceP1, fs.FEMSpaceP1NC], ['P1', 'P1NC']):
        print("Fem space "+name)
        h, h1error, h1error_rate = convergence_elasticity(
            min_refine_level=3, max_refine_level=7,
            verbose=False, space=space)  # type: ignore
        print(f"| {'h':^10} | {'error H1':^10} |")
        for hi, errori in zip(h, h1error):
            print(f"| {hi:.4e} | {errori:.4e} |")
        print(f"stress error convergence rate {h1error_rate:.4e}")
        ax_h1.loglog(h, h1error, 'o-', label=name)
    ax_h1.legend()
    savefig(fig, outdir+"conv_elasticity.tikz")
    Profile.print_stats()


def plot_shape_function(outdir: str):
    """ Plot P1 and P1NC (Crouzeix Raviart) shape function"""
    xy = np.array([[0., 0.], [1., 0.], [0., 1.]])
    tri = np.array([[0, 1, 2]])
    mesh = sm.sMesh(xy, tri)
    edges = np.array([[0, 1], [1, 2], [2, 0]])
    xy_edges = xy[edges]
    order = 1
    rule = quad.T3_midedges()
    npt = len(rule.uv)
    uv = rule.uv.reshape((npt, 2))
    w = rule.w.reshape((npt, 1))
    view = gp.listPlotFieldPoint(
        uv, w, viewname='Order = ' + str(order), PointType='Scaled sphere', PointSize=10)
    gp.listPlotFieldLine(xy_edges, np.array(
        [0.001]*3), P0=True, LineType='3D cylinder', LineWidth=2, gv=view)
    gmsh.view.option.setNumber(view, "ShowScale", 0)
    femspace_p1 = fs.FEMSpaceP1('P1', mesh)
    femspace_p1nc = fs.FEMSpaceP1NC('P1NC', mesh)
    gmsh.option.setNumber("General.ColorScheme", 0)
    gmsh.option.setNumber("General.RotationX", 281.6932579310364)
    gmsh.option.setNumber("General.RotationY", 1.368333600270393)
    gmsh.option.setNumber("General.RotationZ", 231.3459998733334)
    gmsh.option.setNumber("General.ScaleX", .97)
    gmsh.option.setNumber("General.ScaleY", .97)
    gmsh.option.setNumber("General.ScaleZ", .97)
    gmsh.option.setNumber("General.TrackballQuaternion0", 0.2818694390386327)
    gmsh.option.setNumber("General.TrackballQuaternion1", 0.565015471044394)
    gmsh.option.setNumber("General.TrackballQuaternion2", 0.7021227836512218)
    gmsh.option.setNumber("General.TrackballQuaternion3", 0.3291363448396249)
    gmsh.option.setNumber("General.TranslationX", -0.001155219491703426)
    gmsh.option.setNumber("General.TranslationY", -0.007021334053470181)
    gmsh.fltk.initialize()
    N: sp.sparse.csr_array = femspace_p1nc.operator_dof2val_tri(quad.T3_nodes().uv) # pylint: disable=C0103
    u_dofs_list = [np.array([1, 0, 0]), np.array(
        [0, 1, 0]), np.array([0, 0, 1])]
    shape_names = ['N1NC', 'N2NC', 'N3NC']
    for u_dofs, name in zip(u_dofs_list, shape_names):  # pylint: disable=C0103
        u = N.dot(u_dofs).reshape((-1, 3))
        view = gp.listPlotFieldTri(xy.reshape((1, 3, 2)), u, viewname=name)
        gmsh.view.option.setNumber(view, "RaiseZ", 0.5)
        gmsh.view.option.setNumber(view, "Axes", 1)
        for tag in gmsh.view.get_tags():
            gmsh.view.setVisibilityPerWindow(tag, 0)
        gmsh.view.setVisibilityPerWindow(view, 1)
        gmsh.write(outdir+name+'.pdf')
    rule = quad.T3_nodes()
    npt = len(rule.uv)
    uv = rule.uv.reshape((npt, 2))
    w = rule.w.reshape((npt, 1))
    view = gp.listPlotFieldPoint(
        uv, w, viewname='Order = ' + str(order), PointType='Scaled sphere', PointSize=10)
    gp.listPlotFieldLine(xy_edges, np.array(
        [0.001]*3), P0=True, LineType='3D cylinder', LineWidth=2, gv=view)
    gmsh.view.option.setNumber(view, "ShowScale", 0)

    N = femspace_p1.operator_dof2val_tri(quad.T3_nodes().uv)  # pylint: disable=C0103
    for u_dofs, name in zip([np.array([1, 0, 0]), np.array([0, 1, 0]), np.array([0, 0, 1])],
                            ['N1C', 'N2C', 'N3C']):  # pylint: disable=C0103
        u = N.dot(u_dofs).reshape((-1, 3))
        view = gp.listPlotFieldTri(xy.reshape((1, 3, 2)), u, viewname=name)
        gmsh.view.option.setNumber(view, "RaiseZ", 0.5)
        gmsh.view.option.setNumber(view, "Axes", 1)
        for tag in gmsh.view.get_tags():
            gmsh.view.setVisibilityPerWindow(tag, 0)
        gmsh.view.setVisibilityPerWindow(view, 1)
        gmsh.write(outdir+name+'.png')


def corner_pb(*, outdir: str, element_size: float = 0.1):
    """  elasticity on L shape domain """
    young = 1.
    nu = 0.3
    lamb, mu = ms.YoungPoisson2LambdaNu(young, nu)
    meshgenerator = mg.l_shape
    phys2dirichlet = {'dOmega_left': {'x': 0., 'y': 0.}}
    phys2neumann = {'dOmega_right': {'x': 0.01}}

    space_constructors = [fs.FEMSpaceP1, fs.FEMSpaceP1NC]
    model_name, ___ = meshgenerator(element_size)
    edge_group_names = list(phys2dirichlet.keys()) + \
        list(phys2neumann.keys()) + ['dOmega8']
    mesh = sm.gmshModel2sMesh(model_name, edge_group_names)
    nv, ne, nt = mesh.getNbVertices(), mesh.getNbEdges(), mesh.getNbTris()
    print(f'nv {nv:d}, ne {ne:d}, nf {nt:d}')
    C = ms.planeStrainC2222CSR(lamb, mu) # pylint: disable=C0103
    fig_boundtension, ax_boundtension = figax(
        r"$x$ along one of the neumann boundary",
        r"norm of the boundary tension $|\sigma \dot n| $")
    savefig(fig_boundtension, outdir+"neumannbctension.tikz")

    for spaceConstructor in space_constructors:
        space_name = spaceConstructor.__name__+'_'
        pb = ms.ElasticitySolver(mesh, C, physName2Dirichlet=phys2dirichlet,
                                 physName2Neumann=phys2neumann,
                                 spaceConstructor=spaceConstructor,
                                 stab=2*mu*1.)
        U = pb.solve() # pylint: disable=C0103
        gp.listPlotFieldTri(*pb.postProStress(U), P0=True,
                            viewname=space_name+'Stress')
        gp.listPlotFieldTri(*pb.postProStrain(U), P0=True,
                            viewname=space_name+'Strain')
        gp.listPlotFieldTri(*pb.postProDisplacement(U), P0=False,
                            viewname=space_name+'Displacement', VectorType="Displacement",
                            DisplacementFactor=1.)

        stress = pb.postProStress(U)[1]
        neumann_edges, neumann_edge_flip = mesh.getEdgeGroupe('dOmega8')

        if neumann_edges.size:
            e2t_s, e2t = mesh._e2t_() # pylint: disable=W0212
            stress_neumann = stress[e2t[e2t_s[neumann_edges]]]
            normals = mesh.getEdgesNormal(neumann_edges, neumann_edge_flip)
            traction_neumann = np.einsum(
                "i...jk,i...k->i...j", stress_neumann, normals)
            nx = mesh.getEdges2VerticesCoord()[neumann_edges][..., 0]
            nx = np.sort(nx, axis=1)
            order = np.argsort(nx[:, 0])
            nt = (traction_neumann.reshape(-1, 2))[order, :]
            ntx = np.repeat(nt[:, 1], 2)
            nty = np.repeat(nt[:, 1], 2)
            ax_boundtension.plot(nx.flatten(), np.sqrt(
                ntx**2 + nty**2), '-o', label=space_name)
            gp.listPlotFieldLine(mesh.getEdges2VerticesCoord()[neumann_edges], traction_neumann,
                                 P0=True, viewname=space_name+'bcTraction', VectorType="Arrow")
            print('Max neumann norm ', np.max(
                np.linalg.norm(traction_neumann.squeeze(), axis=1)))
    ax_boundtension.legend()

if __name__ == "__main__":
    OUTDIR = "post/"
    IPLOT = False
    argv = sys.argv
    def print_help():
        ''' Print the help string'''
        print('usage : ', argv[0], ' -n <nbelem> -p -l -f <a>')
        print('Compute and plot figures for the crouzeix raviart presentation')
        print('options :')
        print('-h [--help]: this help')
        print('-o <dir> [--output-dir=<dir>] : output directory for all the figure. \
              the directory must exist (default post/)')
        print('-i --interactiveplot]: do we open interactive windows for the plots \
              (launch show() and gmsh.fltk.run())')
    try:
        opts, args = getopt.getopt(argv[1:], "hio:",
                                   ["help", "interactiveplot", "output-dir"])
    except getopt.GetoptError:
        print('Input Error')
        print_help()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ("-i", "--interactiveplot"):
            IPLOT = True
        elif opt in ("-o", "--output-dir"):
            OUTDIR = arg
    print("Starting the crouzeix_raviart benchmark")
    gmsh.initialize()
    gmsh.option.setNumber("PostProcessing.AnimationCycle", 1)
    gmsh.option.setNumber("General.Terminal", 0)
    convergence_analysis(OUTDIR)
    patch_test_elasticity(OUTDIR)
    corner_pb(outdir=OUTDIR, element_size=0.1)
    if IPLOT:
        gmsh.fltk.run()
        show()
    gmsh.finalize()
    gmsh.initialize()
    gmsh.option.setNumber("PostProcessing.AnimationCycle", 1)
    gmsh.option.setNumber("General.Terminal", 0)
    plot_shape_function(OUTDIR)
    if IPLOT:
        gmsh.fltk.run()
        show()
    gmsh.finalize()
