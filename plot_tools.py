#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu April 4 12:42:30 2024

@author: nchevaug """
from __future__ import annotations
import pathlib
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.figure
import tikzplotlib as tikz

mpl.rcParams['lines.linewidth'] = 2
Fig = matplotlib.figure.Figure

def figax(xlabel: str, ylabel: str):
    ''' simple function to create fig with names axis '''
    fig, ax = plt.subplots(1, 1)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    return fig, ax

def savefig(fig : Fig, filepath: pathlib.Path|str) -> None:
    ''' Export the figure fig to file named filepath. if extension is .tikz or .tex, use tikzplotlib
        For the export
    '''
    if isinstance(filepath, str):
        filepath = pathlib.Path(filepath)
    if filepath.suffix in ['.tikz', '.tex']:
        tikz.save(filepath, figure=fig,
           axis_width = '\\axiswidth',
           axis_height = '\\axisheight')
    else:
        fig.savefig(str(filepath))

def show()->None:
    """ display interactive matplotlib figure """
    plt.show()
