#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file contains exact solutions to some classic pde problems

Use full to test convergence
Created on Wed Feb 28 09:49:32 2024
@author: nchevaug
"""
import unittest
import numpy as np
import gmsh

import gmsh_mesh_generator as mg
import gmsh_post_pro_interface as gp
import oneLevelSimplexMesh as sm


class LaplacianGeneral():
    '''Base class for the definition of Lapalcian type problem and its exact solution '''

    def __init__(self, lsfuncs=None, classifyer=None, filt=0, conductivity=None, source=None,
                 BNDDefinition=None, DirichletBCs=None, NeumannBCs=None,
                 exactSolution=None, exactGradient=None) -> None:
        super().__init__()
        self.source = source
        if lsfuncs is None: 
            self.lsfuncs = []
        else:
            self.lsfuncs = lsfuncs
        self.classifyer = classifyer
        self.BNDDefinition = {} if BNDDefinition is None else BNDDefinition
        self.exactSolution = exactSolution
        self.exactGradient = exactGradient
        self.DirichletBCs = {} if DirichletBCs is None else DirichletBCs
        self.NeumannBCs = {} if NeumannBCs is None else NeumannBCs
        self.conductivity = conductivity
        self.filter = filt

    def get_source_term(self):
        """ return the souce term associated to the problem """
        return self.source

    def get_ls(self):
        """ get The level-set function used to define the problem
        (when multiple conductiviies are present )"""
        return self.lsfuncs

    def get_exact_sol(self):
        """ return the exact solution of the problem """
        return self.exactSolution

    def get_exact_grad(self):
        """ return the gradient exact solution of the problem """
        return self.exactGradient

    def get_dirichlet_bc_val(self):
        """ return a dictionary of boundary name to dirichlet value """
        return self.DirichletBCs

    def get_neumann_bc_val(self):
        """ return a dictionary of boundary name to neumann value """
        return self.NeumannBCs

    def get_conductivity_val(self):
        """ return a dictionary of conductivity values """
        return self.conductivity


class laplacianCircularInclusionQuad(LaplacianGeneral):
    """ Circular inclusion, with quadratic solution

    u  equal zero at re, source term  's = 1.' is constant, 2 conductivity : k1 inside, k2 outside
    """

    def __init__(self, k1, k2, ri=0.4, re=np.sqrt(2)) -> None:
        super().__init__()
        self.ri = ri
        self.re = re
        self.lsfuncs = [lambda xyz: np.sqrt(
            xyz[..., 0]**2 + xyz[..., 1]**2) - ri]
        self.BNDDefinition = {'up': lambda x: x[1] == +1.0,
                              'down': lambda x: x[1] == -1.0,
                              'right': lambda x: x[0] == +1.0,
                              'left': lambda x: x[0] == -1.0, }
        self.NeumannBCs = {}
        self.conductivity = {'lsneg': k1, 'lspos': k2}

        def r(xy):
            return np.hypot(xy[..., 0], xy[..., 1])

        def exact_sol(xy):
            rr = r(xy)
            return np.where(rr <= ri,
                            (k1*(re**2 - ri**2) + k2*(-rr**2 + ri**2))/(4*k1*k2),
                            (-rr**2 + re**2)/(4*k2))

        def exact_grad(xy):
            return np.where((r(xy) < ri)[..., np.newaxis], -xy/(2*k1), -xy/(2*k2))
        self.source = 1.
        self.exactSolution = exact_sol
        self.exactGradient = exact_grad
        self.DirichletBCs = {'down': self.exactSolution, 'up': self.exactSolution,
                             'left': self.exactSolution, 'right': self.exactSolution}


class laplacianCircularInclusionQuintic(LaplacianGeneral):
    """ Circular inclusion with order 5 displacement source (r-re)**3 """

    def __init__(self, k1, k2, ri=0.4, re=np.sqrt(2)) -> None:
        super().__init__()
        self.ri = ri
        self.re = re
        lsfuncs = [lambda xyz: np.sqrt(xyz[0]**2 + xyz[1]**2) - ri]
        # self.lsfuncs = [lambda xyz: -xyz[0]-a, lambda xyz: xyz[0]-a]
        conductivity = {'lsneg': k1, 'lspos': k2}

        def r(xy):
            return np.hypot(xy[..., 0], xy[..., 1])

        def qroverr(rr):
            return -0.5*re**3 + re**2*rr - 3./4*re*rr**2 + 1./5*rr**3

        def primitiveqr(rr):
            return (-48*rr**5 + 225*rr**4*re-400*rr**3*re**2+300*rr**2*re**3)/1200

        def exact_sol(xy):
            rr = r(xy)
            uOut = -primitiveqr(rr)/k2 + primitiveqr(re)/k2
            uIn = -primitiveqr(ri)/k2 + primitiveqr(re)/k2 - \
                primitiveqr(rr)/k1 + primitiveqr(ri)/k1
            return np.where(rr <= ri, uIn, uOut)

        def exact_grad(xy):
            rr = r(xy)
            qrroverr = qroverr(rr)[..., np.newaxis]
            return np.where(rr[..., np.newaxis] < ri, xy*qrroverr/k1, xy*qrroverr/k2)

        def source(xy):
            return -(r(xy) - re)**3

        dirichlet_bc = {'down': exact_sol, 'up': exact_sol,
                        'left': exact_sol, 'right': exact_sol}
        super().__init__(lsfuncs=lsfuncs, filt=-1, conductivity=conductivity, source=source,
                         DirichletBCs=dirichlet_bc, NeumannBCs={},
                         exactSolution=exact_sol, exactGradient=exact_grad)


class LinearElasticity2dCircularHoleInfiniteSpace():
    """ linear Elasticity exact solution : hole in a plate """

    def __init__(self, young, nu, *, hyp='planestrain', r=0.5, p=1.):
        if hyp == 'planestrain':
            k2 = (1.-2.*nu) / (2.*(1.-nu))
        elif hyp == 'planestress':
            k2 = (1.-nu) / 2.
        else:
            raise ValueError("hyp must be either planestrain or planestress")
        self.young = young
        self.nu = nu
        self.mu = young/(2.*(1.+nu))
        self.k2 = k2
        self.hyp = hyp
        self.r = r
        self.p = p

    def f(self, z):
        """ complex potential f evaluation """
        assert z.dtype == np.complex128
        p, r = (self.p, self.r)
        return p*r/4.*(z/r + 2*r/z)

    def df(self, z):
        """ fist derivative of the complex potential f evaluation """
        assert z.dtype == np.complex128
        p, r = (self.p, self.r)
        return p*r/4.*(1/r - 2*r/z/z)

    def ddf(self, z):
        """ fist derivative of the complex potential f evaluation """
        assert z.dtype == np.complex128
        p, r = (self.p, self.r)
        return p*r*r/z/z/z

    def dg(self, z):
        """ fist derivative of the complex potential g evaluation """
        assert z.dtype == np.complex128
        p, r = (self.p, self.r)
        return -p*r/2.*(z/r + r/z - np.power(r, 3)*np.power(z, -3))

    def ddg(self, z):
        """ second derivative of the complex potential g evaluation """
        assert z.dtype == np.complex128
        p, r = (self.p, self.r)
        return -p*r/2.*(1/r - r/z/z + 3 * pow(r, 3)*pow(z, -4))

    def displacement(self, xy):
        """ return the displacement value at each position xy """
        if (xy.dtype != np.complex128) and xy.shape[-1] == 2:
            shape = xy.shape
            z = xy.view(dtype=np.complex128).squeeze()
        else:
            z = xy
            shape = xy.shape + (2,)
        k2 = self.k2
        mu = self.mu
        mu2upiv = (1+k2)/(1-k2) * self.f(z) - z * \
            np.conj(self.df(z)) - np.conj(self.dg(z))
        return mu2upiv.view(dtype=np.float64).reshape(shape)/2./mu

    def stress(self, xy, dim=2):
        """ return the stress field

        if dim = 2 return array shape is (..,2,2)
        if dim = 3 return array shape is (..,3,3)
        """
        if dim not in [2, 3]:
            raise ValueError("dim not in [2,3]")
        if (xy.dtype != np.complex128) and xy.shape[-1] == 2:
            shape = xy.shape
            z = xy.view(dtype=np.complex128).reshape(shape[:-1])
        else:
            z = xy
            shape = xy.shape + (2,)
        # sigmaxx + sigmayy
        sxxpsyy = 4*np.real(self.df(z))
        # sigmayy - sigmaxx + 2isigmaxy
        syymsxxp2isxy = 2.*(np.conj(z)*self.ddf(z) + self.ddg(z))
        sxx = (sxxpsyy - np.real(syymsxxp2isxy))*0.5
        syy = (sxxpsyy + np.real(syymsxxp2isxy))*0.5
        sxy = 0.5*np.imag(syymsxxp2isxy)
        stress_ = np.zeros(shape[:-1]+(dim, dim))
        stress_[..., 0, 0] = sxx
        stress_[..., 0, 1] = sxy
        stress_[..., 1, 0] = sxy
        stress_[..., 1, 1] = syy
        if (self.hyp == "planestrain" and dim == 3):
            stress_[..., 2, 2] = self.nu*(sxx+syy)
        return stress_

    def strain(self, xy, dim=2):
        """ return the displacement field

        if dim = 2 return array shape is (..,2,2)
        if dim = 3 return array shape is (..,3,3)
        """
        if dim not in [2, 3]:
            raise ValueError("dim not in [2,3]")
        if (xy.dtype != np.complex128) and xy.shape[-1] == 2:
            z = xy.view(dtype=np.complex128).squeeze()
        else:
            z = xy
        stress_ = self.stress(z, dim)
        E, nu = (self.young, self.nu)
        strain_ = np.zeros(stress_.shape)
        if self.hyp == "planestrain":
            strain_[..., 0, 0] = (1.+nu)/E * ((1-nu) *
                                              stress_[..., 0, 0] - nu*stress_[..., 1, 1])
            strain_[..., 1, 1] = (1.+nu)/E * ((1-nu) *
                                              stress_[..., 1, 1] - nu*stress_[..., 0, 0])
            strain_[..., 0, 1] = (1.+nu)/E * stress_[..., 0, 1]
        elif self.hyp == "planestress":
            strain_[..., 0, 0] = (
                1./E) * (stress_[..., 0, 0] - nu*stress_[..., 1, 1])
            strain_[..., 1, 1] = (
                1./E) * (stress_[..., 1, 1] - nu*stress_[..., 0, 0])
            strain_[..., 0, 1] = (1./E)*(1.-nu) * stress_[..., 0, 1]
            if dim == 3:
                strain_[..., 2, 2] = -nu / \
                    (1.-nu) * (strain_[..., 0, 0]+strain_[..., 1, 1])
        else:
            raise ValueError("dim not in [2,3]")
        strain_[..., 1, 0] = strain_[..., 0, 1]
        return strain_

class laplacianSquareHole(LaplacianGeneral):
    """ Manufactured solution  : square with a square hole

    +--------------+
    |              |
    |     +--+     |
    |     |  |     |
    |     +--+     |
    |              |
    +--------------+
    """

    def __init__(self, a=0.4) -> None:
        super().__init__()
        self.a = a
        lsfuncs = [lambda xyz: -xyz[0]-a, lambda xyz: xyz[0]-a,
                   lambda xyz: -xyz[1]-a, lambda xyz: xyz[1]-a]
        # self.lsfuncs = [lambda xyz: -xyz[0]-a, lambda xyz: xyz[0]-a]
        BNDDefinition = {'up': lambda x: x[1] == +1.0,
                         'down': lambda x: x[1] == -1.0,
                         'right': lambda x: x[0] == +1.0,
                         'left': lambda x: x[0] == -1.0, }
        NeumannBCs = {}
        conductivity = {'lsneg': 1., 'lspos': 1.}

        def exact_grad(x, y):
            gex = np.zeros((2, x.shape[0], x.shape[1]))
            gex[0] = -np.pi/self.a * \
                np.sin(np.pi * x / self.a) * np.cos(np.pi * y / self.a)
            gex[1] = -np.pi/self.a * \
                np.cos(np.pi * x / self.a) * np.sin(np.pi * y / self.a)
            return gex

        def exact_sol(x, y):
            return np.cos(np.pi * x / self.a) * np.cos(np.pi * y / self.a)

        def source(x, y): return 2.*np.pi**2/self.a**2 * \
            np.cos(np.pi*x/self.a)*np.cos(np.pi*y/self.a)
        DirichletBCs = {'down': exact_sol, 'up': exact_sol,
                        'left': exact_sol, 'right': exact_sol}
        super().__init__(lsfuncs=lsfuncs, filt=-1, conductivity=conductivity, source=source,
                         BNDDefinition=BNDDefinition, DirichletBCs=DirichletBCs,
                         NeumannBCs=NeumannBCs,
                         exactSolution=exact_sol, exactGradient=exact_grad)

def laplacianInclusion(exactsolbuilder, popUpGmsh=False):
    ''' plot exact solution for laplacien with 2 conductivities on a mesh '''
    gmsh.initialize()
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)
    gmsh.option.setNumber('General.Terminal', 0)
    # Conductivities :
    k1 = 1.
    k2 = 2.*k1
    # length of the square
    L = 2.
    # radius of the disk separting conductivities.
    ri = 0.4
    re = np.sqrt(2.)
    # mesh density
    lc = 0.05
    # generate gmshmesh
    modelName, groupeName2PhysDimTag = mg.disk_in_ref_square(
        length=L, radius=ri, relh=lc)
    # convert gmshmesh to smesh
    mesh = sm.gmshModel2sMesh(modelName, groupeName2PhysDimTag.keys())
    trisxy = mesh.getTris2VerticesCoord()
    triscogxy = mesh.getTris2VerticesCog()
    exact = exactsolbuilder(k1=k1, k2=k2, ri=ri, re=re)
    if callable(exact.source):
        source = exact.source(trisxy)
    else:
        source = exact.source*np.ones(trisxy.shape[:-1])
    temp = exact.exactSolution(trisxy)
    grad = exact.exactGradient(triscogxy)
    # plotting the fields in gmsh
    gp.listPlotFieldTri(trisxy, source, viewname="source")
    gp.listPlotFieldTri(trisxy, temp, viewname="temperature")
    gp.listPlotFieldTri(trisxy, grad, P0=True, viewname="gradT")
    # run gmsh ide
    if popUpGmsh:
        gmsh.fltk.run()
    gmsh.finalize()
    return temp


def elasticityHoleInPlate(popUpGmsh=False):
    """ test exactSolutions.linearElasticity2dCircularHoleInfiniteSpace and plot it"""
    gmsh.initialize()
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)
    gmsh.option.setNumber('General.Terminal', 0)
    L = 2.
    r0 = 0.2
    E = 1.
    nu = 0.3
    lc = 0.05
    P = 1.
    # generate gmshmesh
    modelName, groupeName2PhysDimTag = mg.hole_in_square(
        length=L, radius=r0, relh=lc)
    # convert gmshmesh to smesh
    mesh = sm.gmshModel2sMesh(modelName, groupeName2PhysDimTag.keys())
    trisxy = mesh.getTris2VerticesCoord()
    # getting all the boundary edges and their normal
    eids, eflips = (np.array([], dtype=np.int64), np.array([], dtype=np.int64))
    for name in mesh.getEdgeGroupeNames():
        eids_n, eflips_n = mesh.getEdgeGroupe(name)
        eids = np.append(eids, eids_n)
        eflips = np.append(eflips, eflips_n)
    normals = mesh.getEdgesNormal(eids, eflips)
    boundaryedgexy = mesh.getEdges2VerticesCoord(eids)
    # constructing the exact solution
    exact = LinearElasticity2dCircularHoleInfiniteSpace(
        E, nu, hyp='planestrain', r=r0, p=P)
    disp = exact.displacement(trisxy)
    stress = exact.stress(trisxy)
    strain = exact.strain(trisxy)
    boundarystress = exact.stress(boundaryedgexy)
    boundarytraction = np.einsum(
        "ijkl,ijl->ijk", boundarystress, normals[:, np.newaxis, :])
    # plotting the fields in gmsh
    gp.listPlotFieldTri(trisxy, disp, viewname="displacement",
                        VectorType="Displacement", DisplacementFactor=0.1)
    gp.listPlotFieldTri(trisxy, stress, viewname="stress")
    gp.listPlotFieldTri(trisxy, strain, viewname="strain")
    gp.listPlotFieldLine(boundaryedgexy, boundarytraction,
                         viewname='boundary tractions')

    # run gmsh ide
    if popUpGmsh:
        gmsh.fltk.run()
    gmsh.finalize()
    return stress

# OTHER EXACT SOLUTIONS COURTESIE OF G. LEGRAIN. Will updated ans corrected when needed
# class laplacianUniaxialDirichlet(laplacianGeneral):
#     def __init__(self, source=0) -> None:
#         super().__init__()
#         # self.diri = {'up': 1., 'down': 0.}
#         self.source = source
#         self.lsfuncs = [lambda xyz: xyz[1]]
#         self.classifyer = classifyers.classifyerAllIn
#         self.BNDDefinition = {  'up': lambda x: x[1] == 1.0,
#                                 'down': lambda x: x[1] == -1.0,}
#         self.DirichletBCs = {'up': 1., 'down': 0.}
#         self.NeumannBCs = {}
#         self.conductivity = {}
#         self.exactSolution = lambda x,y: 0.5*(y+1.)
#         self.exactGradient = lambda x,y: 0.5*np.array([x*0.,y*0.+1])

# class laplacianUniaxialGREG(laplacianGeneral):
#     def __init__(self, source=0) -> None:
#         super().__init__()
#         # self.diri = {'up': 1., 'down': 0.}
#         self.source = source
#         self.lsfuncs = [lambda xyz: xyz[1]]
#         self.classifyer = classifyers.classifyerAllIn
#         self.BNDDefinition = {  'up': lambda x: x[1] == 1.0,
#                                 'down': lambda x: x[1] == -1.0,}
#         self.DirichletBCs = {'down': 0.}
#         self.NeumannBCs = {'up': 1.}
#         self.conductivity = {}
#         self.exactSolution = lambda x,y: 0.5*(y+1.)
#         self.exactGradient = lambda x,y: 0.5*np.array([x*0.,y*0.+1])

# #        s             s
# # |-------------|--------------|
# # 0     k1            k2       1
# class laplacianUniaxialSourceMultimat(laplacianGeneral):
#     def __init__(self, source=1, k1=1., k2=1.) -> None:
#         super().__init__()
#         lsfuncs = [lambda xyz: xyz[1]]
#         classifyer = classifyers.classifyerAllIn
#         BNDDefinition = {  'up': lambda x: x[1] == 1.0,
#                                 'down': lambda x: x[1] == -1.0,}
#         DirichletBCs = {'down': 0., 'up': 1.}
#         NeumannBCs = {}
#         conductivity = {'lsneg': k1,  'lspos': k2}
#         s = source
#         def exactSol(x,y):
#             uex = np.zeros(x.shape)
#             uex[y < 0.] = -(y[y < 0.] + 1)*(-2*k1*k2 + k1*s*y[y < 0.] - 2*k1*s + k2*s*y[y < 0.])
#                            /(2*k1*(k1 + k2))
#             uex[y > 0.] = -(-2*k1*k2*y[y > 0.] + k1*s*y[y > 0.]**2 - k1*s*y[y > 0.] -
#                           2*k2**2 + k2*s*y[y > 0.]**2 + k2*s*y[y > 0.] - 2*k2*s)/(2*k2*(k1 + k2))
#             return uex
#         def exactGrad(x,y):
#             gex = np.zeros((2,x.shape[0], x.shape[1]))
#             gex[1][y<0.] = (k1*k2-k1*s*y[y<0.] + k1*s/2 - k2*s*y[y<0.] - k2*s/2)/(k1*(k1+k2))
#             gex[1][y>0.] = (k1*k2-k1*s*y[y>0.] + k1*s/2 - k2*s*y[y>0.] - k2*s/2)/(k2*(k1+k2))
#             return gex
#         super().__init__(lsfuncs=lsfuncs, classifyer=classifyer, filter=-1,
#                     conductivity=conductivity, source=source,
#                     BNDDefinition=BNDDefinition, DirichletBCs=DirichletBCs, NeumannBCs=NeumannBCs,
#                     exactSolution=exactSol, exactGradient=exactGrad)

# # Circular hole in cylindrical coordinates
# class laplacianCircularHoleDirichletRi(laplacianGeneral):
#     def __init__(self, ri=0.4, re=2, k=6) -> None:
#         super().__init__()
#         NOT_WORKING
#         self.ri = ri
#         self.re = re
#         self.k = k
#         lsfuncs = [lambda xyz: np.sqrt(xyz[0]**2 + xyz[1]**2) - ri]
#         # self.lsfuncs = [lambda xyz: -xyz[0]-a, lambda xyz: xyz[0]-a]
#         classifyer = classifyers.classifyerAllIn
#         BNDDefinition = {'all': lambda x:
#                                    np.logical_or(np.logical_or(x[1] ==  1.0, x[1] == -1.0),
#                                        np.logical_or(x[0] ==  1.0, x[0] == -1.0)),
#                          'hole': lambda x: np.abs(np.sqrt(x[0]**2 + x[1]**2) - ri) < 1.e-5}
#         NeumannBCs = {}
#         conductivity = {'lsneg': 1.,  'lspos': 1.}
#         def r(x,y):
#             return np.hypot(x,y)
#         def exactGrad(x,y):
#             gex = np.zeros((2,x.shape[0], x.shape[1]))
#             rr = r(x,y)
#             dr = self.re - self.ri
#             gex[0] = np.pi*self.k*x*np.cos(np.pi*self.k*(self.ri - rr)/(dr))/((dr)*rr)
#             gex[1] = np.pi*self.k*y*np.cos(np.pi*self.k*(self.ri - rr)/(dr))/((dr)*rr)
#             return gex
#         def exactSol(x,y):
#             return np.sin(np.pi*self.k*(r(x,y) - self.ri)/(self.re - self.ri))
#         source = lambda x,y: -np.pi*self.k*(-np.pi*self.k*r(x,y)*np.sin(np.pi*self.k
#                               *(r(x,y) - self.ri)/(self.re - self.ri))
#                               + (self.re - self.ri)*np.cos(np.pi*self.k*(r(x,y) - self.ri)
#                               /(self.re - self.ri)))/(r(x,y)*(self.re - self.ri)**2)
#         DirichletBCs = {'all': exactSol, 'hole': exactSol}
#         super().__init__(lsfuncs=lsfuncs, classifyer=classifyer,
#                     filter=-1, conductivity=conductivity, source=source,
#                     BNDDefinition=BNDDefinition, DirichletBCs=DirichletBCs, NeumannBCs=NeumannBCs,
#                     exactSolution=exactSol, exactGradient=exactGrad)
# class laplacianCircularHoleNeumannRi(laplacianGeneral):
#     def __init__(self, ri=0.4, re=4, k=3) -> None:
#         super().__init__()
#         self.ri = ri
#         self.re = re
#         self.k = k
#         lsfuncs = [lambda xyz: np.sqrt(xyz[0]**2 + xyz[1]**2) - ri]
#         classifyer = classifyers.classifyerAllIn
#         BNDDefinition = {'all': lambda x:
#                           np.logical_or(np.logical_or(x[1] ==  1.0, x[1] == -1.0),
#                            np.logical_or(x[0] ==  1.0, x[0] == -1.0)),}
#         NeumannBCs = {}
#         conductivity = {'lsneg': 1.,  'lspos': 1.}
#         def r(x,y):
#             return np.sqrt(x**2 + y**2)
#         def exactGrad(x,y):
#             gex = np.zeros((2,x.shape[0], x.shape[1]))
#             tmp = (ri - np.sqrt(x**2 + y**2))/(2*(re - ri)))/(2*(re - ri)*np.sqrt(x**2 + y**2))
#             gex[0] =  np.pi*x*(2*k + 1)*np.sin(np.pi*(2*k + 1)*tmp
#             gex[1] =  np.pi*y*(2*k + 1)*np.sin(np.pi*(2*k + 1)*tmp
#             return gex
#         def exactSol(x,y):
#             # return 0.*x
#             return np.cos(np.pi*(k + 1/2)*(r(x,y) - ri)/(re - ri))
#         source = lambda x,y: -np.pi*(2*k + 1)*(-np.pi*r(x,y)*(2*k + 1)
#                              *np.cos(np.pi*(2*k + 1)*(r(x,y) - ri)/(2*(re - ri)))
#                               + 2*(-re + ri)*np.sin(np.pi*(2*k + 1)*(r(x,y) - ri)
#                               /(2*(re - ri))))/(4*r(x,y)*(re - ri)**2)
#         DirichletBCs = {'all': exactSol}
#         super().__init__(lsfuncs=lsfuncs, classifyer=classifyer,
#                     filter=-1, conductivity=conductivity, source=source,
#                     BNDDefinition=BNDDefinition, DirichletBCs=DirichletBCs, NeumannBCs=NeumannBCs,
#                     exactSolution=exactSol, exactGradient=exactGrad)


# class laplacianCorner(laplacianGeneral):
#     def __init__(self) -> None:
#         lsfuncs = [lambda xyz: xyz[0], lambda xyz: xyz[1]]
#         BNDDefinition = {'all': lambda x:
#                       np.logical_or(np.logical_or(x[1] ==  1.0, x[1] == -1.0),
#                       np.logical_or(x[0] ==  1.0, x[0] == -1.0)),}
#         conductivity = {'lsneg': 1.,  'lspos': 1.}
#         # Coordinate change to define alpha angle: theta = 3pi/4 + alpha
#         # Used so that tha discontinuity in the arctan2 function is not inside the domain
#         P=np.array([[np.cos(5*np.pi/4), np.cos(-np.pi/4)],
#                     [np.sin(5*np.pi/4), np.sin(-np.pi/4)]]).T
#         def r(x,y):
#             return np.sqrt(x**2 + y**2)
#         def computeTheta(x,y):
#             initShape = x.shape
#             xy = np.vstack([x.flatten(), y.flatten()])
#             xyt = P@xy
#             xt = xyt[0].reshape(initShape)
#             yt = xyt[1].reshape(initShape)
#             alpha = np.arctan2(yt,xt)
#             theta = 3.*np.pi/4. + alpha
#             # aaaa
#             return theta
#         def exactSol(x,y):
#             uex = np.zeros(x.shape)
#             rr = r(x,y)
#             theta = computeTheta(x,y)
#             uex = 2./5. * rr**(2./3.)*np.cos(2./3. * theta)
#             return uex
#         def exactGrad(x,y):
#             gex = np.zeros((2,x.shape[0], x.shape[1]))
#             theta = computeTheta(x,y)
#             rr = r(x,y)
#             gex_r = 4./15. * rr**(-1./3.) * np.cos(2./3. * theta)
#             gex_t = -4./15. * rr**(-1./3.) * np.sin(2./3. * theta)
#             # Project back in cartesian coordinates
#             gex_x = gex_r * np.cos(theta + np.pi/2.) - gex_t * np.sin(theta + np.pi/2.)
#             gex_y = gex_r * np.sin(theta + np.pi/2.) + gex_t * np.cos(theta + np.pi/2.)
#             gex[0] = gex_x
#             gex[1] = gex_y
#             return gex
#         DirichletBCs = {'all': exactSol}
#         super().__init__(lsfuncs=lsfuncs, classifyer=classifyers.classifyerAnyIn,
#                     filter=1, conductivity=conductivity, source=0.,
#                     BNDDefinition=BNDDefinition, DirichletBCs=DirichletBCs, NeumannBCs={},
#                     exactSolution=exactSol, exactGradient=exactGrad)
#
# class laplacianStraightPatchTest(laplacianGeneral):
#     def __init__(self, type='lin') -> None:
#         # super().__init__()
#         assert(type == 'lin' or type == 'quad')
#         lsfuncs = [lambda xyz: xyz[1]]
#         self.type = type
#         classifyer = classifyers.classifyerAllIn
#         BNDDefinition = {  'down':  lambda x: x[1] == -1.0,
#                            'right': lambda x: x[0] ==  1.0,
#                            'left':  lambda x: x[0] == -1.0,}
#         NeumannBCs = {}
#         conductivity = {'lsneg': 1.,  'lspos': 1.}
#         def exactSol(x,y):
#             if self.type == 'lin':
#                 uex = 0.5 * (1-x)
#             else:
#                 uex = x**2 - 0.5 * (1+x)
#             return uex
#         def exactGrad(x,y):
#             gex = np.zeros((2,x.shape[0], x.shape[1]))
#             if self.type == 'lin':
#                 gex[0] -= 0.5
#             else:
#                 gex[0] = 2. * x - 0.5
#             return gex
#         if self.type == 'lin':
#             source = lambda x,y: 0.
#         else:
#             source = lambda x,y: -2.
#         DirichletBCs = {'left': exactSol, 'right': exactSol}
#         super().__init__(lsfuncs=lsfuncs,
#                     classifyer=classifyers.classifyerAnyIn, filter=1,
#                     conductivity=conductivity, source=source,
#                     BNDDefinition=BNDDefinition, DirichletBCs=DirichletBCs, NeumannBCs={},
#                     exactSolution=exactSol, exactGradient=exactGrad)



class testAnaly(unittest.TestCase):
    """ testing Analytical solutions """

    def testElasticityHoleInPlate(self, popUpGmsh=False):
        """ - Compute max exact stress on mesh nodes and compare to ref value """
        stress = elasticityHoleInPlate(popUpGmsh)
        max_stressxx = np.max(stress[:, :, 0, 0])
        self.assertTrue((max_stressxx - 3) < 1.e-9)

    def testLaplacianCircularInclusionQuad(self, popUpGmsh=False):
        """ - Compute max Temperature on mesh nodes and compare to ref value """
        temp = laplacianInclusion(
            laplacianCircularInclusionQuad, popUpGmsh=popUpGmsh)
        self.assertTrue(np.abs(np.max(temp) - 0.2698578163107752) < 1.e-6)

    def testLaplacianCircularInclusionQuintic(self, popUpGmsh=False):
        """ - Compute max Temperature on mesh nodes and compare to ref value """
        temp = laplacianInclusion(
            laplacianCircularInclusionQuintic, popUpGmsh=popUpGmsh)
        self.assertTrue(np.abs(np.max(temp) - 0.21952206273616834) < 1.e-6)


if __name__ == '__main__':
    laplacianInclusion(laplacianCircularInclusionQuad, popUpGmsh=True)
    laplacianInclusion(laplacianCircularInclusionQuintic, popUpGmsh=True)
    elasticityHoleInPlate(popUpGmsh=True)
