#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  4 12:07:38 2024

@author: nchevaug
"""
import unittest
import numpy as np
import scipy as sp
import gmsh
import gmsh_mesh_generator as mg
import gmsh_post_pro_interface as gp
import exact_solutions as ex
import mechanics_solver as ms
import oneLevelSimplexMesh as sm
import function_spaces as fs
import quadrature as quad
from plot_tools import figax, savefig, show

def convergence_analysis(min_refine_level=4, max_refine_level=7,
                         verbose=True, popup_gmsh=False, space=fs.FEMSpaceP1):
    """ Convergence Anlysis in 2d elasticity : plate with circular hole """
    #if !gmsh.is_initialized()
    gmsh.initialize()
    gmsh.option.setNumber('PostProcessing.AnimationCycle', 1)
    gmsh.option.setNumber('General.Terminal', 0)
    gmsh.option.setNumber('Mesh.SurfaceEdges', 0)
    length = 2. #length of the square mesh
    r0 = 0.5
    young = 1.
    nu = 0.4
    lamb, mu = ms.YoungPoisson2LambdaNu(young, nu)
    elasticity_tensor = ms.planeStrainC2222CSR(lamb, mu)
    exact_sol = ex.LinearElasticity2dCircularHoleInfiniteSpace(young, nu, hyp="planestrain", r=r0)
    def exactu(xy):
        return exact_sol.displacement(xy)[..., 0]
    def exactv(xy):
        return exact_sol.displacement(xy)[..., 1]
    imposed = {'x':exactu, 'y':exactv}
    phys2dirichlet = {'bottom': imposed, 'right': imposed, 'top': imposed, 'left': imposed}
    steps = max_refine_level- min_refine_level+ 1
    h = np.zeros(steps)
    error = np.zeros(steps)
    for i, refine_level in enumerate(range(min_refine_level, max_refine_level+1)):
        lc = 2**(-refine_level)
        model_name, groupe_name2phys_dim_tag = mg.hole_in_square(length=length, radius=r0, relh=lc)
        mesh = sm.gmshModel2sMesh(model_name, groupe_name2phys_dim_tag.keys())
        if verbose:
            nv, ne, nt = mesh.getNbVertices(), mesh.getNbEdges(), mesh.getNbTris()
            print(f"nv {nv:d}, ne {ne:d}, nf {nt:d}")
        pb = ms.ElasticitySolver(mesh, elasticity_tensor, physName2Dirichlet=phys2dirichlet,
                                 spaceConstructor=space, stab=2*mu*0.1)
        u_dofs = pb.solve()
        if popup_gmsh:
            fem_stress = pb.postProStress(u_dofs)[1]
            trisxy = mesh.getTris2VerticesCoord()
            exact_stress = exact_sol.stress(trisxy)
            gp.listPlotFieldTri(trisxy, fem_stress, P0=True, viewname='fem Stress', Range=[0., 3.])
            if refine_level == max_refine_level:
                gp.listPlotFieldTri(trisxy, exact_stress, viewname='exact Stress', Range=[0., 3.])
        stress_err = pb.L2StressError(u_dofs, exact_sol.stress, quadrature=quad.T3_gauss(2))
        if verbose:
            print(lc, stress_err)
        h[i] = lc
        error[i] = stress_err
    def power_fun(x, a, alpha):
        return a*np.power(x, alpha)
    h1error_rate = sp.optimize.curve_fit(power_fun, h, error, p0=[1., 1.])[0][1]
    if popup_gmsh:
        gmsh.fltk.run()
    gmsh.finalize()
    return h, error, h1error_rate

class TestElasticity(unittest.TestCase):
    """ Test case for unittest """
    def test_convergence(self):
        """ # Convergence Analysis: Compare H1 error rate to 1"""
        __, __, h1_error_rate = convergence_analysis(min_refine_level=3, max_refine_level=5,
                                        verbose=False, popup_gmsh=False)
        self.assertTrue(np.abs(h1_error_rate-1) < 0.1)

def main():
    """ Main program """
    print("Convergence Analysis Elasticity")

    fig, ax_h1 = figax(xlabel='h', ylabel='H1 error')
    for space, name in zip([fs.FEMSpaceP1, fs.FEMSpaceP1NC], ['P1', 'P1NC']):
        print("Fem space "+name)
        h, h1error, h1error_rate = convergence_analysis(min_refine_level=3, max_refine_level=7,
                                    verbose=False, popup_gmsh=False, space=space) # type: ignore
        print(f"| {'h':^10} | {'error H1':^10} |")
        for hi, errori in zip(h, h1error):
            print(f"| {hi:.4e} | {errori:.4e} |")
        print(f"stress error convergence rate {h1error_rate:.4e}")
        ax_h1.loglog(h, h1error, 'o-', label =name)
    ax_h1.legend()
    savefig(fig, "Docs/conv_elasticity.tikz")
    show()

if __name__ == '__main__':
    main()
